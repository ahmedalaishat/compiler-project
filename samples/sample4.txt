create type region(name string);
create type country(id number, name string, region region);
create type city(id number, name string, country country);

create table clubs (id number,name string, city city) type="json" path="data\\clubs.json";

create aggregation_function max("CommonAggregations.jar","CommonAggregations","max",number,[number]);

fn()
{
    var c = select max(id) from clubs where city.country.region.name in ("Asia","Africa");
    print(c);
}