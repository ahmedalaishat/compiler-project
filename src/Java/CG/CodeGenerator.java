package Java.CG;

import Java.AST.Function.Parse;
import Java.AST.Function.VarDeclaration;
import Java.AST.SqlStmt.SelectStmt;
import Java.SymbolTable.Scope;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.type.IMergedTable;
import Java.SymbolTable.type.MergedTable;
import Java.SymbolTable.type.TableType;
import Java.SymbolTable.type.Type;

import java.io.IOException;
import java.util.Map;

import static Java.Main.SAMPLE_NUM;
import static Java.Main.symbolTable;

public class CodeGenerator {
    static final String OUTPUT_DIR = "B:\\FACULTY\\4th year\\2nd semester\\compiler\\Project\\src\\";
    static final String BASE_PACKAGE = "generated" + SAMPLE_NUM;
    //    static String basePackage;
    static final String FILTERABLE_PACKAGE = BASE_PACKAGE + ".filterable";
    static final String UTIL_PACKAGE = BASE_PACKAGE + ".util";
    static final String TYPES_PACKAGE = BASE_PACKAGE + ".types";
    static final String DECLARED_TYPES_PACKAGE = TYPES_PACKAGE + ".declared";
    static final String VARS_TYPES_PACKAGE = TYPES_PACKAGE + ".vars";

    public static void generateAll(Parse p) throws IOException {
        FileManager.removeOld();
//        write merged classes (from a join b)
        for (VarDeclaration varDeclaration : p.getVarDeclarations()) {
            Object ob = varDeclaration.getVariableValue().getValue();
            if (ob instanceof SelectStmt) {
//                basePackage = BASE_PACKAGE + p.getFunName();
                SelectStmt stmt = (SelectStmt) ob;
                IMergedTable iMergedTable = stmt.getFrom().getIMergedTable();
                if (iMergedTable instanceof MergedTable)
                    for (MergedTable mergedTable : ((MergedTable) iMergedTable).getMergedTables())
                        ClassGenerator.write(mergedTable.getMergedType(), CodeGenerator.VARS_TYPES_PACKAGE, false);
            }
        }
//        write declared types and tables (CREATE table ,CREATE type)
        for (TableType tableType : symbolTable.getDeclaredTypes()) {
            ClassGenerator.write(tableType, DECLARED_TYPES_PACKAGE, true);
        }
//        write variables types (var x = select)
        for (Scope scope : symbolTable.getScopes()) {
            for (Map.Entry<String, Symbol> entry : scope.getSymbolMap().entrySet()) {
                Type type = entry.getValue().getType();
//                TODO may be add aggregation function
                if (type instanceof TableType) {
                    TableType t = (TableType) type;
                    ClassGenerator.write(t, VARS_TYPES_PACKAGE, true);
                }
            }
        }
//        write Main && Filterable && QueryEngine
        MainGenerator.write(p);
        StaticClassesGenerator.write();
    }
}
