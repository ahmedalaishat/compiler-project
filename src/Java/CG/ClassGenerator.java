package Java.CG;

import Java.SymbolTable.type.TableType;

import java.io.IOException;

import static Java.CG.StaticClassesGenerator.getPackageAndImports;

class ClassGenerator {

    static void write(TableType t, String packageName, boolean isTable) throws IOException {
        String generatedClass = ClassGenerator.generateClass(t, packageName, isTable);
        FileManager.writeFile(packageName, t.getTypeName(), generatedClass);
    }

    private static String generateClass(TableType tableType, String packageName, boolean isTable) {
        return getPackageAndImports(packageName) +
                "\npublic class " + tableType.getTypeName() + (isTable?" extends Table":"")+" {\n\t" +
                tableType.generateClassContent() +
                "\n}";
    }
}
