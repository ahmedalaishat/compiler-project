package Java.CG;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import static Java.CG.CodeGenerator.BASE_PACKAGE;
import static Java.CG.CodeGenerator.OUTPUT_DIR;

class FileManager {

    // write file with content
    static void writeFile(String packageName, String fileName, String fileContent) throws IOException {
        String path = getPackagePath(packageName);
        createPackage(new File(path));
        FileOutputStream fileOutputStream = new FileOutputStream(path + fileName + ".java");
        fileOutputStream.write(fileContent.getBytes());
        fileOutputStream.close();
    }

    static void removeOld() {
        deleteDirectory(new File(OUTPUT_DIR + BASE_PACKAGE));
    }

    private static void createPackage(File packageFile) throws IOException {
        if (!packageFile.getParentFile().exists())
            createPackage(packageFile.getParentFile());
        if (!packageFile.exists())
            Files.createDirectory(packageFile.toPath());
    }

    private static String getPackagePath(String packageName) {
        return OUTPUT_DIR + packageName.replace(".", "\\") + "\\";
    }

    private static void deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        directoryToBeDeleted.delete();
    }
}
