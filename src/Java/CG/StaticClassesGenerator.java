package Java.CG;

import java.io.IOException;

import static Java.CG.CodeGenerator.*;

public class StaticClassesGenerator {


    private static String generateFilterableClass() {
        return getPackageAndImports(FILTERABLE_PACKAGE) +
                "\n" +
                "\n" +
                "public class FilterableSequence<I> {\n" +
                "    private List<I> data = new ArrayList<>();\n" +
                "\n" +
                "    public FilterableSequence<I> filter(Predicate<I> p) {\n" +
                "        return new FilterableSequence<>(getData().stream().filter(p)\n" +
                "                .collect(Collectors.toList()));\n" +
                "    }\n" +
                "\n" +
                "    public FilterableSequence() {\n" +
                "    }\n" +
                "\n" +
                "    public FilterableSequence(List<I> data) {\n" +
                "        this.data = data;\n" +
                "    }\n" +
                "\n" +
                "    public List<I> getData() {\n" +
                "        return data;\n" +
                "    }\n" +
                "\n" +
                "    public void setData(List<I> data) {\n" +
                "        this.data = data;\n" +
                "    }\n" +
                "\n" +
                "    public <T> FilterableSequence<T> merge(Class<T> mergedClass, FilterableSequence right)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException {\n" +
                "        return QueryEngine.mergeTables(mergedClass, this, right);\n" +
                "    }\n" +
                "\n" +
                "    public <T> FilterableSequence<T> innerJoin(Class<T> mergedClass, FilterableSequence right, Predicate<T> filter)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException {\n" +
                "        return merge(mergedClass, right).filter(filter);\n" +
                "    }\n" +
                "\n" +
                "    public <T> FilterableSequence<T> map(Class<T> mergedClass)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException {\n" +
                "        return QueryEngine.mapTable(mergedClass, this);\n" +
                "    }\n" +
                "\n" +
                "    public <T> FilterableSequence<T> mapWithFilter(Class<T> mergedClass, Predicate<T> filter)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException {\n" +
                "        return map(mergedClass).filter(filter);\n" +
                "    }\n" +
                "    \n" +
                "    public <T> FilterableSequence<T> mapWithAgg(Class<T> mergedClass, ArrayList<AggFun> aggFuns)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, NoSuchFieldException, ClassNotFoundException {\n" +
                "        return QueryEngine.mapWithAgg(mergedClass,this,aggFuns);\n" +
                "    }\n" +
                "    " +
                "\n"+
                "\n" +
                "    public <T> FilterableSequence<T> groupBy(Class<T> mergedClass,String field, ArrayList<AggFun> aggFuns)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, NoSuchFieldException, ClassNotFoundException {\n" +
                "        return QueryEngine.groupBy(mergedClass,this,field,aggFuns);\n" +
                "    }"+
                "\n" +
                "\n" +
                "    public <T> FilterableSequence<T> groupByWithHaving(Class<T> mergedClass,String field, ArrayList<AggFun> aggFuns,HavingAgg havingAgg)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, NoSuchFieldException, ClassNotFoundException {\n" +
                "        return QueryEngine.groupByWithHaving(mergedClass,this,field,aggFuns,havingAgg);\n" +
                "    }"+
                "\n" +
                "\n"+
                "    public <T> FilterableSequence<T> groupByWithFilter(Class<T> mergedClass, String field, ArrayList<AggFun> aggFuns, Predicate<I> p)\n" +
                "            throws InvocationTargetException, InstantiationException, IllegalAccessException, IOException, NoSuchMethodException, NoSuchFieldException, ClassNotFoundException {\n" +
                "        return QueryEngine.groupBy(mergedClass, this.filter(p), field, aggFuns);\n" +
                "    }\n"+
                "\n"+
                "\n" +
                "    @Override\n" +
                "    public String toString() {\n" +
                "        StringBuilder builder = new StringBuilder();\n" +
                "        builder.append(getClass().getSimpleName())\n" +
                "                .append(\"= [\\n\");\n" +
                "        for (I i : data)\n" +
                "            builder.append(i).append(\",\\n\");\n" +
                "        builder.append(\"];\");\n" +
                "        return builder.toString();\n" +
                "    }\n" +
                "}";
    }

    private static String generateQueryEngine() {
        return getPackageAndImports(UTIL_PACKAGE) +
                "\n" +
                "\n" +
                "\n" +
                "public class QueryEngine {\n" +
                "\n" +
                "    public static <I> FilterableSequence<I> mapWithAgg(Class<I> mappedItemClass, FilterableSequence basic, ArrayList<AggFun> aggFuns)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException, MalformedURLException, NoSuchFieldException, NoSuchMethodException {\n" +
                "        " +
                "if (basic.getData().size()==0){\n" +
                "            return new FilterableSequence<>(new ArrayList<>());\n" +
                "        }"+
                "Constructor[] MergedConstructors = mappedItemClass.getDeclaredConstructors();\n" +
                "        Constructor mergedConstr = null;\n" +
                "        for (Constructor constructor : MergedConstructors) {\n" +
                "            if (constructor.getParameterCount() == 0)\n" +
                "                mergedConstr = constructor;\n" +
                "        }\n" +
                "        mergedConstr.setAccessible(true);\n" +
                "        I mapped = (I) mergedConstr.newInstance();\n" +
                "        Field[] mappedFields = mappedItemClass.getDeclaredFields();\n" +
                "        FilterableSequence<I> mappedSequence = new FilterableSequence<>();\n" +
                "        for (AggFun aggFun : aggFuns) {\n" +
                "            for (Field field : mappedFields) {\n" +
                "                field.setAccessible(true);\n" +
                "\n" +
                "                try\n" +
                "                {\n" +
                "                    if (aggFun.functionName.equals(field.getName())) {\n" +
                "                        field.set(mapped, aggFun.invoke(basic.getData()));\n" +
                "                    } else if (basic.getData().get(0).getClass().getDeclaredField(field.getName()) != null) {\n" +
                "                        field.set(mapped, getFieldObject(basic.getData().get(0), field.getName()));\n" +
                "                    }\n" +
                "                }catch (NoSuchFieldException e){}" +
                "            }\n" +
                "        }\n" +
                "        ArrayList<I> list = new ArrayList<>();\n" +
                "        list.add(mapped);\n" +
                "        FilterableSequence<I> result = new FilterableSequence<>();\n" +
                "        result.setData(list);\n" +
                "        return result;\n" +
                "    }\n" +
                "\n" +
                "    public static <I> FilterableSequence<I> mapTable(Class<I> mappedItemClass, FilterableSequence basic)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException {\n" +
                "        FilterableSequence<I> mappedSequence = new FilterableSequence<>();\n" +
                "        List<I> list = new ArrayList<>();\n" +
                "        for (int i = 0; i < basic.getData().size(); i++) {\n" +
                "            I t = QueryEngine.flatObject(mappedItemClass, basic.getData().get(i));\n" +
                "            list.add(t);\n" +
                "        }\n" +
                "        mappedSequence.setData(list);\n" +
                "        return mappedSequence;\n" +
                "    }\n" +
                "\n" +
                "    public static <\n" +
                "            I> FilterableSequence<I> mergeTables(Class<I> mergedItemClass, FilterableSequence left, FilterableSequence right)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException {\n" +
                "        FilterableSequence<I> mergedSequence = new FilterableSequence<>();\n" +
                "        List<I> list = new ArrayList<>();\n" +
                "        for (int i = 0; i < left.getData().size(); i++) {\n" +
                "            for (int j = 0; j < right.getData().size(); j++) {\n" +
                "                I t = QueryEngine.mergeObjects(mergedItemClass, left.getData().get(i), right.getData().get(j));\n" +
                "                list.add(t);\n" +
                "            }\n" +
                "        }\n" +
                "        mergedSequence.setData(list);\n" +
                "        return mergedSequence;\n" +
                "    }\n" +
                "\n" +
                "    private static <T> T mergeObjects(Class<T> mergedClass, Object leftOb, Object rightOb)\n" +
                "            throws IllegalAccessException, InvocationTargetException, InstantiationException {\n" +
                "        ArrayList<Table> tables = getTables(leftOb);\n" +
                "        tables.addAll(getTables(rightOb));\n" +
                "        Constructor[] MergedConstructors = mergedClass.getDeclaredConstructors();\n" +
                "        Constructor mergedConstr = null;\n" +
                "        for (Constructor constructor : MergedConstructors) {\n" +
                "            if (constructor.getParameterCount() == 0)\n" +
                "                mergedConstr = constructor;\n" +
                "        }\n" +
                "        mergedConstr.setAccessible(true);\n" +
                "        T merged = (T) mergedConstr.newInstance();\n" +
                "        for (Field field : mergedClass.getDeclaredFields()) {\n" +
                "            field.setAccessible(true);\n" +
                "            for (Table table : tables) {\n" +
                "//                TODO change to var name (table alias)\n" +
                "                if (table.getClass().getSimpleName().equals(field.getType().getSimpleName()))\n" +
                "                    field.set(merged, table);\n" +
                "            }\n" +
                "        }\n" +
                "        return merged;\n" +
                "    }\n" +
                "\n" +
                "    private static <T> T flatObject(Class<T> mappedClass, Object basic)\n" +
                "            throws IllegalAccessException, InvocationTargetException, InstantiationException {\n" +
                "        Map<String, Object> objectMap;\n" +
                "        objectMap = getFlatTable(basic);\n" +
                "        Constructor[] mappedClasConsts = mappedClass.getDeclaredConstructors();\n" +
                "        Constructor mappedClssCons = null;\n" +
                "        for (Constructor constructor : mappedClasConsts) {\n" +
                "            if (constructor.getParameterCount() == 0)\n" +
                "                mappedClssCons = constructor;\n" +
                "        }\n" +
                "        mappedClssCons.setAccessible(true);\n" +
                "        T merged = (T) mappedClssCons.newInstance();\n" +
                "        for (Field field : mappedClass.getFields()) {\n" +
                "            field.setAccessible(true);\n" +
                "            Object fieldValue = objectMap.get(field.getName());\n" +
                "            if (fieldValue != null)\n" +
                "                field.set(merged, fieldValue);\n" +
                "        }\n" +
                "        return merged;\n" +
                "    }\n" +
                "\n" +
                "    private static Map<String, Object> getFlatTable(Object root) throws IllegalAccessException {\n" +
                "        Map<String, Object> tables = new LinkedHashMap<>();\n" +
                "        Field[] fields = root.getClass().getDeclaredFields();\n" +
                "        for (Field field : fields) {\n" +
                "            field.setAccessible(true);\n" +
                "            Object child = field.get(root);\n" +
                "            if (Modifier.isStatic(field.getModifiers()))\n" +
                "                continue;\n" +
                "            if (field.getType().isPrimitive() || field.getType().equals(String.class))\n" +
                "                tables.put(field.getName(), child);\n" +
                "            else if (child instanceof Table)\n" +
                "                tables.putAll(getFlatTable(child, field.getName()));\n" +
                "            else\n" +
                "                tables.putAll(getFlatTable(child));\n" +
                "        }\n" +
                "        return tables;\n" +
                "    }\n" +
                "\n" +
                "    private static Map<String, Object> getFlatTable(Object root, String rootName) throws IllegalAccessException {\n" +
                "        Map<String, Object> tables = new LinkedHashMap<>();\n" +
                "        Field[] fields = root.getClass().getDeclaredFields();\n" +
                "        for (Field field : fields) {\n" +
                "            String name = rootName + \"$\" + field.getName();\n" +
                "            field.setAccessible(true);\n" +
                "            Object child = field.get(root);\n" +
                "            if (Modifier.isStatic(field.getModifiers()))\n" +
                "                continue;\n" +
                "            if (field.getType().isPrimitive() || field.getType().equals(String.class))\n" +
                "                tables.put(name, child);\n" +
                "            else\n" +
                "                tables.putAll(getFlatTable(child, name));\n" +
                "        }\n" +
                "        return tables;\n" +
                "    }\n" +
                "\n" +
                "    private static ArrayList<Table> getTables(Object root) throws IllegalAccessException {\n" +
                "        ArrayList<Table> tables = new ArrayList<>();\n" +
                "        if (root instanceof Table) {\n" +
                "            tables.add((Table) root);\n" +
                "            return tables;\n" +
                "        }\n" +
                "        Field[] fields = root.getClass().getDeclaredFields();\n" +
                "        for (Field field : fields) {\n" +
                "            field.setAccessible(true);\n" +
                "            Object child = field.get(root);\n" +
                "            if (child instanceof Table)\n" +
                "                tables.add((Table) child);\n" +
                "            else\n" +
                "                tables.addAll(getTables(child));\n" +
                "        }\n" +
                "        return tables;\n" +
                "    }\n" +
                "\n" +
                "    public static <T> FilterableSequence<T> groupBy(Class<T> mergedClass, FilterableSequence basic,\n" +
                "                                                    String groupByField, ArrayList<AggFun> aggFuns)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException,\n" +
                "            MalformedURLException, NoSuchFieldException, NoSuchMethodException {\n" +
                "        HashMap<Object, ArrayList<T>> map = (HashMap<Object, ArrayList<T>>) basic.getData().stream()\n" +
                "                .collect(Collectors.groupingBy(p -> {\n" +
                "                    try {\n" +
                "                        return getFieldObject(p, groupByField);\n" +
                "                    } catch (IllegalAccessException e) {\n" +
                "                        e.printStackTrace();\n" +
                "                    }\n" +
                "                    return false;\n" +
                "                }));\n" +
                "        ArrayList<T> list = new ArrayList<>();\n" +
                "        for (Map.Entry<Object, ArrayList<T>> entry : map.entrySet()) {\n" +
                "            list.add(mapWithAgg(mergedClass, new FilterableSequence(entry.getValue()), aggFuns).getData().get(0));\n" +
                "        }\n" +
                "        FilterableSequence<T> result = new FilterableSequence<>();\n" +
                "        result.setData(list);\n" +
                "        return result;\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    public static Double getAggResult(FilterableSequence basic, AggFun aggFun)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException, MalformedURLException, NoSuchFieldException, NoSuchMethodException {\n" +
                "        return (Double) aggFun.invoke(basic.getData());\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    public static <T> FilterableSequence<T> groupByWithHaving(Class<T> mergedClass, FilterableSequence basic,\n" +
                "                                                              String groupByField, ArrayList<AggFun> aggFuns, HavingAgg havingAgg)\n" +
                "            throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException,\n" +
                "            MalformedURLException, NoSuchFieldException, NoSuchMethodException {\n" +
                "        HashMap<Object, ArrayList<T>> map = (HashMap<Object, ArrayList<T>>) basic.getData().stream()\n" +
                "                .collect(Collectors.groupingBy(p -> {\n" +
                "                    try {\n" +
                "                        return getFieldObject(p, groupByField);\n" +
                "                    } catch (IllegalAccessException e) {\n" +
                "                        e.printStackTrace();\n" +
                "                    }\n" +
                "                    return false;\n" +
                "                }));\n" +
                "        Object o = map.entrySet().stream().filter(p -> {\n" +
                "            try {\n" +
                "                return havingAgg.exec(p.getValue());\n" +
                "            } catch (MalformedURLException e) {\n" +
                "                e.printStackTrace();\n" +
                "            } catch (ClassNotFoundException e) {\n" +
                "                e.printStackTrace();\n" +
                "            } catch (NoSuchMethodException e) {\n" +
                "                e.printStackTrace();\n" +
                "            } catch (InvocationTargetException e) {\n" +
                "                e.printStackTrace();\n" +
                "            } catch (IllegalAccessException e) {\n" +
                "                e.printStackTrace();\n" +
                "            } catch (NoSuchFieldException e) {\n" +
                "                e.printStackTrace();\n" +
                "            }\n" +
                "            return false;\n" +
                "        }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));\n" +
                "        map = (HashMap<Object, ArrayList<T>>) o;\n" +
                "        ArrayList<T> list = new ArrayList<>();\n" +
                "        for (Map.Entry<Object, ArrayList<T>> entry : map.entrySet()) {\n" +
                "            list.add(mapWithAgg(mergedClass, new FilterableSequence(entry.getValue()), aggFuns).getData().get(0));\n" +
                "        }\n" +
                "        FilterableSequence<T> result = new FilterableSequence<>();\n" +
                "        result.setData(list);\n" +
                "        return result;\n" +
                "    }\n" +
                "\n" +
                "\n" +
                "    private static Object getFieldObject(Object obj, String fieldName) throws IllegalAccessException {\n" +
                "        for (Field field1 : obj.getClass().getDeclaredFields()) {\n" +
                "            field1.setAccessible(true);\n" +
                "            if (field1.getName().equals(fieldName)) {\n" +
                "                return field1.get(obj);\n" +
                "            }\n" +
                "            if (!field1.getType().isPrimitive() && !Modifier.isStatic(field1.getModifiers())) {\n" +
                "                Object f = getFieldObject(field1.get(obj), fieldName);\n" +
                "                if (f != null)\n" +
                "                    return f;\n" +
                "            }\n" +
                "        }\n" +
                "        return null;\n" +
                "    }\n" +
                "}";
    }

    static String getPackageAndImports(String packageName) {
        return "package " + packageName + ";" +
                "\n" +
                "\nimport " + BASE_PACKAGE + ".*;" +
                "\nimport " + TYPES_PACKAGE + ".*;" +
                "\nimport " + DECLARED_TYPES_PACKAGE + ".*;" +
                "\nimport " + UTIL_PACKAGE + ".*;" +
                "\nimport " + VARS_TYPES_PACKAGE + ".*;" +
                "\nimport " + FILTERABLE_PACKAGE + ".*;" +
                "\n" +
                "\nimport com.google.gson.Gson;" +
                "\nimport com.google.gson.JsonElement;" +
                "\nimport com.google.gson.JsonObject;" +
                "\nimport com.google.gson.reflect.TypeToken;" +
                "\nimport com.google.gson.stream.JsonReader;" +
                "\n" +
                "\nimport java.io.File;" +
                "\nimport java.io.FileInputStream;" +
                "\nimport java.io.InputStreamReader;" +
                "\nimport java.io.IOException;" +
                "\n" +
                "import java.lang.reflect.Constructor;\n" +
                "import java.lang.reflect.Field;\n" +
                "import java.lang.reflect.InvocationTargetException;\n" +
                "import java.lang.reflect.Modifier;\n" +
                "import java.util.*;\n" +
                "import java.util.HashMap;\n" +
                "import java.util.LinkedHashMap;" +
                "import java.lang.reflect.Method;\n" +
                "import java.net.MalformedURLException;\n" +
                "import java.net.URL;\n" +
                "import java.net.URLClassLoader;\n" +
                "\nimport java.util.ArrayList;" +
                "\nimport java.util.List;" +
                "\nimport java.util.Scanner;" +
                "\nimport java.util.function.Predicate;" +
                "\nimport java.util.stream.Collectors;";
    }

    private static String generateTable() {
        return getPackageAndImports(DECLARED_TYPES_PACKAGE) +
                "\n" +
                "\n" +
                "public abstract class Table {\n" +
                "}\n";
    }

    static void write() throws IOException {
        FileManager.writeFile(DECLARED_TYPES_PACKAGE, "Table", generateTable());
        FileManager.writeFile(UTIL_PACKAGE, "AggFun", generateAggFun());
        FileManager.writeFile(UTIL_PACKAGE, "HavingAgg", generateHavingAgg());
        FileManager.writeFile(UTIL_PACKAGE, "QueryEngine", generateQueryEngine());
        FileManager.writeFile(FILTERABLE_PACKAGE, "FilterableSequence", generateFilterableClass());
    }

    private static String generateAggFun() {
        return getPackageAndImports(UTIL_PACKAGE) +
                "\n" +
                "\n" +
                "public class AggFun<RT, IT> {\n" +
                "    public String functionName;\n" +
                "    public String jarPath;\n" +
                "    public String className;\n" +
                "    public String methodName;\n" +
                "    public String aggField;\n" +
                "\n" +
                "    public AggFun(String functionName, String jarPath, String className, String methodName, String aggField) {\n" +
                "        this.functionName = functionName;\n" +
                "        this.jarPath = jarPath;\n" +
                "        this.className = className;\n" +
                "        this.methodName = methodName;\n" +
                "        this.aggField = aggField;\n" +
                "    }\n" +
                "\n" +
                "    public RT invoke(List data) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException,\n" +
                "            IllegalAccessException, NoSuchFieldException {\n" +
                "        ArrayList<IT> list = new ArrayList<>();\n" +
                "        for (Object i : data) {\n" +
                "            Field field;\n" +
                "            if (aggField.equals(\"*\")) {\n" +
                "                field = i.getClass().getDeclaredFields()[0];\n" +
                "                field.setAccessible(true);\n" +
                "                list.add((IT) field.get(i));\n" +
                "            } else {\n" +
                "                Object o = getFieldObject(i);\n" +
                "                list.add((IT) o);\n" +
                "            }\n" +
                "        }\n" +
                "        String singletonMethod = \"getCommonAggregations\";\n" +
                "        URLClassLoader myClassLoader = new URLClassLoader(\n" +
                "                new URL[]{new File(jarPath).toURI().toURL()},\n" +
                "                QueryEngine.class.getClassLoader()\n" +
                "        );\n" +
                "        Class<?> myClass = Class.forName(className, true, myClassLoader);\n" +
                "        Method mySingletonGetterMethod = myClass.getMethod(singletonMethod);\n" +
                "        Object myObject = mySingletonGetterMethod.invoke(null);\n" +
                "        return (RT) myObject.getClass().getMethod(methodName, List.class)\n" +
                "                .invoke(myObject, list);\n" +
                "    }\n" +
                "\n" +
                "    private Object getFieldObject(Object obj) throws IllegalAccessException {\n" +
                "        for (Field field1 : obj.getClass().getDeclaredFields()) {\n" +
                "            field1.setAccessible(true);\n" +
                "            if (field1.getName().equals(aggField)) {\n" +
                "                return field1.get(obj);\n" +
                "            }\n" +
                "            if (!field1.getType().isPrimitive()) {\n" +
                "                Object f = getFieldObject(field1.get(obj));\n" +
                "                if (f != null)\n" +
                "                    return f;\n" +
                "            }\n" +
                "        }\n" +
                "        return null;\n" +
                "    }\n" +
                "}";
    }

    private static String generateHavingAgg() {
        return getPackageAndImports(UTIL_PACKAGE) +
                "\n" +
                "\n" +
                "public class HavingAgg {\n" +
                "    AggFun aggFun;\n" +
                "    public double aggVal;\n" +
                "    public Predicate<HavingAgg> predicate;\n" +
                "\n" +
                "    public HavingAgg(AggFun aggFun,Predicate<HavingAgg> predicate) {\n" +
                "        this.aggFun = aggFun;\n" +
                "        this.predicate = predicate;\n" +
                "    }\n" +
                "\n" +
                "    public boolean exec(ArrayList list) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException, MalformedURLException, ClassNotFoundException {\n" +
                "        aggVal = (double) aggFun.invoke(list);\n" +
                "        return predicate.test(this);\n" +
                "    }\n" +
                "}";
    }
}
