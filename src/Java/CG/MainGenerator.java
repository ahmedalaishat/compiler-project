package Java.CG;

import Java.AST.Function.JavaStmt.PrintStmt;
import Java.AST.Function.Parse;
import Java.AST.Function.VarAssign;
import Java.AST.Function.VarDeclaration;
import Java.AST.SqlStmt.*;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.type.IMergedTable;
import Java.SymbolTable.type.MergedTable;
import Java.SymbolTable.type.TableType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static Java.CG.CodeGenerator.BASE_PACKAGE;
import static Java.CG.StaticClassesGenerator.getPackageAndImports;

class MainGenerator {

    //        generate main code
    private static String generateContent(Parse p) {
        boolean agg = false;
        StringBuilder mainContent = new StringBuilder();
//        1. generate varDeclaration (var x = SELECT)
        for (VarDeclaration varDeclaration : p.getVarDeclarations()) {
            Object ob = varDeclaration.getVariableValue().getValue();
            if (ob instanceof SelectStmt) {
                String typeName = ((TableType) varDeclaration.getSymbol().getType()).getTypeName();
                SelectStmt stmt = (SelectStmt) ob;
                List<ResultColumn> resultColumns = ((SelectStmt) ob).getSelecting().getFunctionExprs();
                mainContent.append("\n\t\taggFuns=new ArrayList<>();");
                for (ResultColumn resultColumn : resultColumns) {
                    FunctionExpr expr = (FunctionExpr) resultColumn.getExpr();
                    agg = true;
                    for (AggregationFunction aggregationFunction : stmt.getSelecting().getAgg()) {
                        if (aggregationFunction.getAggregationFunctionName().equals(resultColumn.getExpr().toString()))
                            if (expr.getExprs().size() == 0) {
                                mainContent.append("\n\t\taggFuns.add(").append(newAggFun(aggregationFunction
                                        , null)).append(");");
                            } else {
                                mainContent.append("\n\t\taggFuns.add(").append(newAggFun(aggregationFunction
                                        , ((ColumnExpr) expr.getExprs().get(0)).getColumnName())).append(");");
                            }
                    }
                }
                IMergedTable mergedTable = stmt.getFrom().getIMergedTable();
                mainContent.append("\n\t\tFilterableSequence<")
                        .append(typeName)
                        .append("> ")
                        .append(varDeclaration.getIdentifierName())
                        .append("= ");
                mainContent.append(execQuery(typeName, mergedTable,
                        stmt.getWhere() != null ? stmt.getWhere().getExpr() : null, agg, stmt));
            }
        }
//        2. generate varAssign (x = SELECT)
//        TODO check var assign
        for (VarAssign varAssign : p.getVarAssigns()) {
            Object ob = varAssign.getVariableValue().getValue();
            if (ob instanceof SelectStmt) {
                String typeName = ((TableType) varAssign.getSymbolType()).getTypeName();
                SelectStmt stmt = (SelectStmt) ob;
                IMergedTable mergedTable = stmt.getFrom().getIMergedTable();
                mainContent.append("\n\t\t")
                        .append(varAssign.getIdentifierName())
                        .append("= ");
                mainContent.append(execQuery(typeName, mergedTable,
                        stmt.getWhere() != null ? stmt.getWhere().getExpr() : null, agg, stmt));
            }
        }
//        3. generate print stmt
        for (PrintStmt printStmt : p.getPrintStmts()) {
            mainContent.append("\n\t\tSystem.out.println(")
                    .append(printStmt.getVal().getValue().toString())
                    .append(");\n");
        }
        return mainContent.toString();
    }

    private static String newAggFun(AggregationFunction agg, String columnName) {
        StringBuilder builder = new StringBuilder();
        builder.append("new AggFun(\"")
                .append(agg.getAlias() == null ? agg.getAggregationFunctionName() :
                        agg.getAlias()).append("\",\n\"")
                .append(agg.getJarPath()).append("\",\n\"")
                .append(agg.getClassName()).append("\",\n\"")
                .append(agg.getMethodName()).append("\",\n\"");
        if (columnName == null)
            builder.append("*").append("\"").append(")");
        else
            builder.append(columnName).append("\"").append(")");
        return builder.toString();
    }

    private static String execQuery(String typeName, IMergedTable mergedTable, Expr whereExpr,
                                    boolean agg, SelectStmt stmt) {
        StringBuilder builder = new StringBuilder();
        if (mergedTable instanceof TableType) {//only one table in from  clause
            if (mergedTable.getAlias() != null)
                builder.append((mergedTable).getAlias());
            else
                builder.append(((TableType) mergedTable).getTypeName())
                        .append(".getData()");
        } else if (mergedTable instanceof MergedTable) {
            ArrayList<MergedTable> mergedTables = ((MergedTable) mergedTable).getMergedTables();
            IMergedTable iTable = mergedTables.get(0).getLeft();
            if (iTable.getAlias() != null)
                builder.append(iTable.getAlias());
            else
                builder.append(iTable.getName()).append(".getData()");
            if (((MergedTable) mergedTable).getOperator().equals(JoinClause.JoinOperator.CROSS))
                for (MergedTable table : mergedTables)
                    builder.append(merge(table));
            else if (((MergedTable) mergedTable).getOperator().equals(JoinClause.JoinOperator.INNER_JOIN))
                for (MergedTable table : mergedTables)
                    builder.append(innerJoin(table));
        }
        //        where filtering
        String s = mergedTable instanceof TableType ? ((TableType) mergedTable).getTypeName() : "";
        String predicate;
        if (whereExpr != null && (predicate = whereExpr.getPredicate()) != null)
            builder.append(".filter(p->").
                    append(predicate.replace("$", ".")).
                    append(")");
        if (stmt.getGroupBy() != null) {
            if (stmt.getGroupBy().getHaving() != null) {
                String ss = null;
                if (stmt.getGroupBy().getHaving().getFunctionExpr().getExprs().size() != 0) {
                    ss = ((ColumnExpr) stmt.getGroupBy().getHaving().getFunctionExpr().getExprs().get(0)).getColumnName();
                }
                builder.append(".groupByWithHaving(").append(typeName).append(".class,\"")
                        .append(stmt.getGroupBy().getExprs().get(0)).append("\", aggFuns, ")
                        .append("new HavingAgg(").append(newAggFun(stmt.getGroupBy().getHaving()
                        .getFunctionExpr().getAgg().get(0), ss)).append(", p-> ")
                        .append(stmt.getGroupBy().getHaving().getExpr().getPredicate()).append("))");
//                        append(stmt.getGroupBy().getHaving().getExpr().getPredicate().replace("$", ".")).
//                        append(") ");
//                builder.append(".groupByWithFilter(").append(typeName).append(".class,\"")
//                        .append(stmt.getGroupBy().getExprs().get(0)).append("\", aggFuns, ").append("p->")
//                        .append(stmt.getGroupBy().getHaving().getExpr().getPredicate().replace("$", "."))
//                        .append(")");
            } else {
                builder.append(".groupBy(").append(typeName).append(".class,\"")
                        .append(stmt.getGroupBy().getExprs().get(0)).append("\", aggFuns)");
            }
        } else {
            if (agg)
                builder.append("\n\t\t.mapWithAgg(").append(typeName).append(".class,aggFuns)");
            else
                builder.append("\n\t\t.map(").append(typeName).append(".class)");
        }
        builder.append(";");
        return builder.toString();
    }

    private static String innerJoin(MergedTable mergedTable) {
        StringBuilder builder = new StringBuilder();
        builder.append("\n\t\t.innerJoin(").append(mergedTable.getName()).append(".class, ");
        if (mergedTable.getRight().getAlias() != null) {
            builder.append(mergedTable.getRight().getAlias())
                    .append(", p->");
        } else {
            builder.append(mergedTable.getRight().getName())
                    .append(".getData(), p->");
        }
        String predicate;
        if ((predicate = mergedTable.getPredicate()) != null)
            builder.append(predicate.replace("$", ".")).append(")");
        return builder.toString();
    }

    private static String merge(MergedTable mergedTable) {
        if (mergedTable.getAlias() != null)
            return "\n\t\t.merge(" + mergedTable.getName() + ".class, " + mergedTable.getRight().getAlias() + ")";
        else
            return "\n\t\t.merge(" + mergedTable.getName() + ".class, " + mergedTable.getRight().getName() + ".getData())";
    }

    private static String generate(Parse p) {
        return getPackageAndImports(BASE_PACKAGE) +
                "\n" +
                "\n" +
                "public class Main {\n" +
                "    public static void main(String[] args)\n" +
                "            throws IOException, IllegalAccessException, InvocationTargetException, InstantiationException " +
                ", NoSuchMethodException, NoSuchFieldException, ClassNotFoundException {\n" +
                "       ArrayList<AggFun> aggFuns = new ArrayList<>();" +
                generateContent(p) +
                "\n    }\n" +
                "}";
    }

    static void write(Parse p) throws IOException {
        FileManager.writeFile(BASE_PACKAGE, "Main", generate(p));
    }
}