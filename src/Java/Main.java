package Java;

import Java.AST.Function.Parse;
import Java.AST.Visitor.ASTBaseVisitor;
import Java.Base.BaseVisitor;
import Java.CG.CodeGenerator;
import Java.SymbolTable.Scope;
import Java.SymbolTable.SymbolTable;
import gen.sqlLexer;
import gen.sqlParser;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.util.Stack;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {

    public static SymbolTable symbolTable = new SymbolTable();
    public static int id = 0;
    public static Stack<Scope> scopesStack = new Stack<>();
    public static final String SAMPLE_NUM = "7";


    public static void addScope(Scope scope) {
        try {
            scope.setParent(scopesStack.peek());
        } catch (Exception ignored) {
        }
        scopesStack.push(scope);
        symbolTable.addScope(scope);

    }

    //    TODO
//     optimize having and merge
//     add agg to where and having claus
//
    public static void main(String[] args) {
        try {
            CharStream cs = fromFileName("samples\\sample" + SAMPLE_NUM + ".txt");
            sqlLexer lexer = new sqlLexer(cs);
            CommonTokenStream token = new CommonTokenStream(lexer);
            sqlParser parser = new sqlParser(token);
            ParseTree tree = parser.parse();
            Parse p = (Parse) new BaseVisitor().visit(tree);
            p.accept(new ASTBaseVisitor());
            CodeGenerator.generateAll(p);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
