package Java.AST.Visitor;

public interface AcceptVisit {
    void accept(ASTVisitor visitor);
}
