package Java.AST.Visitor;

import Java.AST.Function.*;
import Java.AST.Function.JavaStmt.*;
import Java.AST.SqlStmt.*;
import Java.AST.SqlStmt.table.Table;

public interface ASTVisitor {
    void visit(Parse parse);

    void visit(Function function);

    void visit(CallFunctionIns callFunctionIns);

    void visit(BooleanValue booleanValue);
    void visit(NumberValue numberValue);
    void visit(StringValue stringValue);

    void visit(VariableValue variableValue);

    void visit(Statement statement);

    void visit(SelectStmt selectStmt);

    void visit(FunctionBody functionBody);

    void visit(FunctionHeader functionHeader);

    void visit(ReturnIns returnIns);

    void visit(BreakIns breakIns);

    void visit(Argument argument);

    void visit(Array array);

    void visit(VarDeclaration varDeclaration);

    void visit(VarAssign varAssign);

    void visit(EnhancedIfStmt enhancedIfStmt);

    void visit(DoWhileStmt doWhileStmt);

    void visit(ForEachStmt forEachStmt);

    void visit(ForStmt forStmt);

    void visit(IfStmt ifStmt);

    void visit(PrintStmt printStmt);

    void visit(SwitchStmt switchStmt);

    void visit(WhileStmt whileStmt);

    void visit(ElseStmt elseStmt);

    void visit(Casee casee);

    void visit(CaseBody caseBody);

    void visit(ForUpdate forUpdate);

    void visit(ForInitialization forInitialization);

    void visit(TypeName typeName);

    void visit(UnaryOperationExpr unaryOperationExpr);

    void visit(TwoExpr twoExpr);

    void visit(Table table);

    void visit(SubQuery subQuery);

    void visit(SubExpr subExpr);

    void visit(ResultColumn resultColumn);

    void visit(OrderingTerm orderingTerm);

    void visit(LiteralValue literalValue);

    void visit(JoinClause joinClause);

    void visit(InExpr inExpr);

    void visit(IndexedColumn indexedColumn);

    void visit(FunctionExpr functionExpr);

    void visit(Expr expr);

    void visit(ExistExpr existExpr);

    void visit(ColumnDef columnDef);

    void visit(ColumnExpr columnExpr);

    void visit(Param param);

    void visit(From from);

    void visit(GroupBy groupBy);

    void visit(Values values);

    void visit(Where where);

    void visit(Selecting selecting);

    void visit(OrderingBy orderingBy);

    void visit(Limit limit);

    void visit(Increment increment);

    void visit(Decrement decrement);

    void visit(CreateTypeStmt createTypeStmt);

    void visit(CreateTableStmt createTableStmt);

    void visit(CreateAggFunStmt createAggFunStmt);

    void visit(Having having);

    void visit(Operation operation);

    void visit(ExprOperation exprOperation);
}
