package Java.AST.Visitor;

import Java.AST.Function.*;
import Java.AST.Function.JavaStmt.*;
import Java.AST.SqlStmt.*;
import Java.AST.SqlStmt.table.BasicTable;
import Java.AST.SqlStmt.table.Table;
import Java.Main;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.type.TableType;
import Java.Util;

import java.util.ArrayList;

import static Java.Main.*;

public class ASTBaseVisitor implements ASTVisitor {

    @Override
    public void visit(Parse parse) {
        addScope(parse.getScope());
        //System.out.println(parse);
    }

    @Override
    public void visit(Function function) {
        String funName = function.getFunctionName();
        if (symbolTable.getScope(funName) != null) {
            System.err.println("Error in: (" + function.getLine() + "," + function.getCol() + ")" + funName + " already declared.");
        }
        addScope(function.getScope());
        //System.out.println(function);
    }

    @Override
    public void visit(ExprOperation exprOperation) {
    }

    @Override
    public void visit(Operation operation) {
    }

    @Override
    public void visit(Having having) {
    }

    @Override
    public void visit(CallFunctionIns callFunctionIns) {
        if (symbolTable.getScope(callFunctionIns.getFunctionName()) == null) {
            System.err.println("Error in: (" + callFunctionIns.getLine() + "," + callFunctionIns.getCol() + ") calling undeclared function " + callFunctionIns.getFunctionName());
        }
        //System.out.println(callFunctionIns);
    }

    @Override
    public void visit(NumberValue numberValue) {
        if (numberValue.getType().equals(NumberValue.Type.IDENTIFIER)) {
            if (Main.scopesStack.peek().isDeclared(numberValue.getValue()) == null)
                System.err.println("Error in: (" + numberValue.getLine() + "," + numberValue.getCol() + ") using undeclared variable " + numberValue.getValue());
        }
    }

    @Override
    public void visit(StringValue stringValue) {
        if (stringValue.getType().equals(StringValue.Type.IDENTIFIER)) {
            if (Main.scopesStack.peek().isDeclared(stringValue.getValue()) == null)
                System.err.println("Error in: (" + stringValue.getLine() + "," + stringValue.getCol() + ") using undeclared variable " + stringValue.getValue());
        }
    }

    @Override
    public void visit(BooleanValue booleanValue) {
        if (booleanValue.getType().equals(BooleanValue.Type.IDENTIFIER)) {
            if (Main.scopesStack.peek().isDeclared(booleanValue.getCon().toString()) == null)
                System.err.println("Error in: (" + booleanValue.getLine() + "," + booleanValue.getCol() + ") using undeclared variable " + booleanValue.getCon().toString());
        }
        //System.out.println(booleanValue);
    }

    @Override
    public void visit(VariableValue variableValue) {
        if (variableValue.getType().equals(VariableValue.VariableType.IDENTIFIER)) {
            if (Main.scopesStack.peek().isDeclared(variableValue.getValue().toString()) == null)
                System.err.println("Error in: (" + variableValue.getLine() + "," + variableValue.getCol() + ") using undeclared variable " + variableValue.getValue().toString());
        }
        //System.out.println(variableValue);
    }

    @Override
    public void visit(Statement statement) {
        //System.out.println(statement);
    }

    @Override
    public void visit(FunctionBody functionBody) {
        //System.out.println(functionBody);
    }

    @Override
    public void visit(FunctionHeader functionHeader) {
        //System.out.println(functionHeader);
    }

    @Override
    public void visit(ReturnIns returnIns) {
        //System.out.println(returnIns);
    }

    @Override
    public void visit(BreakIns breakIns) {
        //System.out.println(breakIns);
    }

    @Override
    public void visit(Argument argument) {
        //System.out.println(argument);
    }

    @Override
    public void visit(Array array) {
        //System.out.println(array);
    }

    @Override
    public void visit(VarDeclaration varDeclaration) {
        if (Main.scopesStack.peek().isDeclared(varDeclaration.getIdentifierName()) != null)
            System.err.println("Error in: (" + varDeclaration.getLine() + "," + varDeclaration.getCol() + ") " + varDeclaration.getIdentifierName() + " variable already exist.");
        else {
            scopesStack.peek().addSymbol(varDeclaration.getIdentifierName(), varDeclaration.getSymbol());
            Symbol symbol = varDeclaration.getSymbol();
            if (symbol != null && symbol.getType() != null && symbol.getType() instanceof TableType)
                ((TableType) symbol.getType()).setTypeName("Var" + Util.capitalize(varDeclaration.getIdentifierName()) + Util.capitalize(((TableType) symbol.getType()).getTypeName()));
            if (varDeclaration.getVariableValue() == null)
                System.err.println("Warring in: (" + varDeclaration.getLine() + "," + varDeclaration.getCol() + ") using unassigned variable " + varDeclaration.getIdentifierName());
        }
        //System.out.println(varDeclaration);
    }

    @Override
    public void visit(VarAssign varAssign) {
        Symbol symbol = scopesStack.peek().isDeclared(varAssign.getIdentifierName());
        if (symbol == null)
            System.err.println("Error in: (" + varAssign.getLine() + "," + varAssign.getCol() + ") using undeclared variable " + varAssign.getIdentifierName());
        else {
            if (varAssign.getVariableValue().getValue() instanceof SelectStmt) {
                if (!symbol.getType().equals(((SelectStmt) varAssign.getVariableValue().getValue()).getFlattedType()))
                    System.err.println("Error in: (" + varAssign.getLine() + "," + varAssign.getCol() + ") " + varAssign.getIdentifierName() + " assigning another type for declared variable.");
            } else if (symbol.getType() != null && !symbol.getType().equals(varAssign.getSymbolType())) {
                System.err.println("Error in: (" + varAssign.getLine() + "," + varAssign.getCol() + ") " + varAssign.getIdentifierName() + " assigning another type for declared variable.");
            }
        }
        //System.out.println(varAssign);
    }

    @Override
    public void visit(EnhancedIfStmt enhancedIfStmt) {
        //System.out.println(enhancedIfStmt);
    }

    @Override
    public void visit(DoWhileStmt doWhileStmt) {
        addScope(doWhileStmt.getScope());
        //System.out.println(doWhileStmt);
    }

    @Override
    public void visit(ForEachStmt forEachStmt) {
        addScope(forEachStmt.getScope());
        if (Main.scopesStack.peek().isDeclared(forEachStmt.getVarName()) == null) {
            System.err.println("Error in: (" + forEachStmt.getLine() + "," + forEachStmt.getCol() + ") using undeclared variable" + forEachStmt.getVarName());
        }
        //System.out.println(forEachStmt);
    }

    @Override
    public void visit(ForStmt forStmt) {
        addScope(forStmt.getScope());
        //System.out.println(forStmt);
    }

    @Override
    public void visit(IfStmt ifStmt) {
        addScope(ifStmt.getScope());
        //System.out.println(ifStmt);
    }

    @Override
    public void visit(PrintStmt printStmt) {
        //System.out.println(printStmt);
    }

    @Override
    public void visit(SwitchStmt switchStmt) {
        addScope(switchStmt.getScope());
        if (Main.scopesStack.peek().isDeclared(switchStmt.getIdentifierName()) == null) {
            System.err.println("Error in: (" + switchStmt.getLine() + "," + switchStmt.getCol() + ") using undeclared variable " + switchStmt.getIdentifierName());
        }
        //System.out.println(switchStmt);
    }

    @Override
    public void visit(WhileStmt whileStmt) {
        addScope(whileStmt.getScope());
        //System.out.println(whileStmt);
    }

    @Override
    public void visit(ElseStmt elseStmt) {
        addScope(elseStmt.getScope());
        //System.out.println(elseStmt);
    }

    @Override
    public void visit(Casee casee) {
        //System.out.println(casee);
    }

    @Override
    public void visit(CaseBody caseBody) {
        //System.out.println(caseBody);
    }

    @Override
    public void visit(ForUpdate forUpdate) {
        //System.out.println(forUpdate);
    }

    @Override
    public void visit(ForInitialization forInitialization) {
        //System.out.println(forInitialization);
    }

    @Override
    public void visit(TypeName typeName) {
        //System.out.println(typeName);
    }

    @Override
    public void visit(UnaryOperationExpr unaryOperationExpr) {
        //System.out.println(unaryOperationExpr);
    }

    @Override
    public void visit(TwoExpr twoExpr) {
        //System.out.println(twoExpr);
    }

    @Override
    public void visit(Table table) {
        if (table instanceof BasicTable)
            if (symbolTable.getDeclaredType(((BasicTable) table).getTableName()) == null)
                System.err.println("Error in: (" + table.getLine() + "," + table.getCol() + ") using of undeclared type " + ((BasicTable) table).getTableName());
        //System.out.println(table);
    }

    @Override
    public void visit(SubQuery subQuery) {
        //System.out.println(subQuery);
    }

    @Override
    public void visit(SubExpr subExpr) {
        //System.out.println(subExpr);
    }

    @Override
    public void visit(ResultColumn resultColumn) {
        //System.out.println(resultColumn);
    }

    @Override
    public void visit(OrderingTerm orderingTerm) {
        //System.out.println(orderingTerm);
    }

    @Override
    public void visit(LiteralValue literalValue) {
        //System.out.println(literalValue);
    }

    @Override
    public void visit(JoinClause joinClause) {
        //System.out.println(joinClause);
    }

    @Override
    public void visit(InExpr inExpr) {
        //System.out.println(inExpr);
    }

    @Override
    public void visit(IndexedColumn indexedColumn) {
        //System.out.println(indexedColumn);
    }

    @Override
    public void visit(FunctionExpr functionExpr) {
        //System.out.println(functionExpr);
    }

    @Override
    public void visit(Expr expr) {
//        if (expr.getiExpr() instanceof InExpr && ((InExpr) expr.getiExpr()).getSelectStmt()!=null){
//
//        }
        //System.out.println(expr);
    }

    @Override
    public void visit(ExistExpr existExpr) {
        //System.out.println(existExpr);
    }

    @Override
    public void visit(ColumnDef columnDef) {
        //System.out.println(columnDef);
    }

    @Override
    public void visit(ColumnExpr columnExpr) {
        //System.out.println(columnExpr);
    }

    @Override
    public void visit(Param param) {
        scopesStack.peek().addSymbol(param.getIdentifier(), param.getSymbol());
        //System.out.println(param);
    }

    @Override
    public void visit(From from) {
        //System.out.println(from);
    }

    @Override
    public void visit(GroupBy groupBy) {
        ArrayList<String> aggFunctions = groupBy.getAggFunctions();
        if (aggFunctions.size() > 0) {
            System.err.println("Error in: (" + groupBy.getLine() + "," + groupBy.getCol() + ") Group by clause can’t contain aggregate function");
        }
        //System.out.println(groupBy);
    }

    @Override
    public void visit(Values values) {
        //System.out.println(values);
    }

    @Override
    public void visit(Where where) {
        //System.out.println(where);
    }

    @Override
    public void visit(SelectStmt selectStmt) {
//        for (String tableName :
//                selectStmt.get) {
//
//        }
//        if (symbolTable.getDeclaredType(table.getTableName()) == null)
//            System.err.println("Error: " + table.getTableName() + " table did not declared.");
//        for (ColumnExpr columnExpr :
//                selectStmt.getColumnsUsed()) {
//
//        }
        ArrayList<String> columnsNotExists = selectStmt.check();
        for (String columnName :
                columnsNotExists) {
            System.err.println("Error in: (" + selectStmt.getLine() + "," + selectStmt.getCol() + ") column " + columnName + " not exist in table " + selectStmt.getTableName());
        }
//        if (selectStmt.getWhere() != null) {
//            String column = selectStmt.getWhere().check(selectStmt.getTableName());
//            if (column != null)
//                System.err.println("Error in: " + selectStmt.getWhere().getExpr().getLine() + "," + selectStmt.getWhere().getExpr().getCol() + " " + column + " can not be in result of select stmt.");
//        }
        //        selectStmt.getFlatType();
//        ITableOrSub iTableOrSub = selectStmt.getFrom().getTableOrSubQueries().get(0).getiTableOrSub();
//        if (iTableOrSub instanceof Table) {
//            Type type = symbolTable.getDeclaredType(((Table) iTableOrSub).getTableName());
//            if (type != null) {
//                Selecting selecting = selectStmt.getSelecting();
//                if (!selecting.isAll()) {
//                    ArrayList<ResultColumn> resultColumns = selecting.getResultColumns();
//                    for (ResultColumn resultColumn : resultColumns) {
//                        if (!resultColumn.isStar()&&resultColumn.getExpr().getiExpr() instanceof ColumnExpr){
//                            if(!type.isColumnTypeExist(((ColumnExpr) resultColumn.getExpr().getiExpr()).getColumnName())){
//                                System.err.println("Error: " + ((ColumnExpr) resultColumn.getExpr().getiExpr()).getColumnName() + " not exists.");
//                            }
//                        }
//                    }
//                }
//            }
//        }
        //System.out.println(selectStmt);
    }

    @Override
    public void visit(Selecting selecting) {
        //System.out.println(selecting);
    }

    @Override
    public void visit(OrderingBy orderingBy) {
        //System.out.println(orderingBy);
    }

    @Override
    public void visit(Limit limit) {
        //System.out.println(limit);
    }

    @Override
    public void visit(Increment increment) {
        if (Main.scopesStack.peek().isDeclared(increment.getVariable()) == null) {
            System.err.println("Error in: (" + increment.getLine() + "," + increment.getCol() + ") using undeclared variable" + increment.getVariable());
        }
        //System.out.println(increment);
    }

    @Override
    public void visit(Decrement decrement) {
        if (Main.scopesStack.peek().isDeclared(decrement.getVariable()) == null) {
            System.err.println("Error in: (" + decrement.getLine() + "," + decrement.getCol() + ") using undeclared variable " + decrement.getVariable());
        }
        //System.out.println(decrement);
    }

    @Override
    public void visit(CreateTypeStmt createTypeStmt) {
        symbolTable.getDeclaredTypes().add(createTypeStmt.getType());
        for (ColumnDef columnDef : createTypeStmt.getColumnDefs()) {
            if (createTypeStmt.getCount(columnDef.getColumnName()) > 1)
                System.err.println("Error in: (" + createTypeStmt.getLine() + "," + createTypeStmt.getCol() + ") " + columnDef.getColumnName() + " column duplicated.");
        }
        //System.out.println(createTypeStmt);
    }

    @Override
    public void visit(CreateTableStmt createTableStmt) {
//        Type tableType = new Type();
//        tableType.setName(createTableStmt.getTableName());
//        symbolTable.addType(tableType);
        try {
            symbolTable.getDeclaredTypes().add(createTableStmt.getType());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error in: (" + createTableStmt.getLine() + "," + createTableStmt.getCol() + ")");
            System.err.println(e.getMessage());
        }
        for (ColumnDef columnDef : createTableStmt.getColumnDefs()) {
            if (createTableStmt.getCount(columnDef.getColumnName()) > 1)
                System.err.println("Error in: (" + createTableStmt.getLine() + "," + createTableStmt.getCol() + ") " + columnDef.getColumnName() + " column duplicated.");
        }
        //System.out.println(createTableStmt);
    }

    @Override
    public void visit(CreateAggFunStmt createAggFunStmt) {
        symbolTable.getDeclaredAggregationFunction().add(createAggFunStmt.getAggregationFunction());
        //System.out.println(createAggFunStmt);
    }
}
