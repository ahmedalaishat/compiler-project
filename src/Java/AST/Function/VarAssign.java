package Java.AST.Function;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.type.Type;

public class VarAssign extends Instruction {
    private String identifierName;
    private VariableValue variableValue;
    private Symbol symbol;

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public void setVariableValue(VariableValue variableValue) {
        this.variableValue = variableValue;
    }

    public String getIdentifierName() {
        return identifierName;
    }

    public VariableValue getVariableValue() {
        return variableValue;
    }

    public Type getSymbolType() {
        return variableValue.getSymbolType();
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        variableValue.accept(visitor);
    }

    @Override
    public String toString() {
        return "VarDeclaration{" +
                ", identifierName='" + identifierName + '\'' +
                "} " + super.toString();
    }
}
