package Java.AST.Function;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class FunctionHeader extends Node {
    private ArrayList<Param> params = new ArrayList<>();

    public void setParams(ArrayList<Param> params) {
        this.params = params;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Param param : params) {
            param.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "FunctionHeader{} " + super.toString();
    }

    public ArrayList<Param> getParams() {
        return params;
    }
}
