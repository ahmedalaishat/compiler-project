package Java.AST.Function;

import Java.AST.Node.IArgument;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class ReturnIns extends Instruction {
    IArgument returnsObject;

    public IArgument getReturnsObject() {
        return returnsObject;
    }

    public void setReturnsObject(IArgument returnsObject) {
        this.returnsObject = returnsObject;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (returnsObject != null && returnsObject instanceof Node)
            ((Node) returnsObject).accept(visitor);
    }

    @Override
    public String toString() {
        return "ReturnIns{} " + super.toString();
    }
}
