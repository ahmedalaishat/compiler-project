package Java.AST.Function;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Symbol;

public class Param extends Node {
    private String identifier;
    private VariableValue variableValue;
    private Symbol symbol;


    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setVariableValue(VariableValue variableValue) {
        this.variableValue = variableValue;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (variableValue != null)
            variableValue.accept(visitor);
    }

    @Override
    public String toString() {
        return "Param{" +
                ", identifier='" + identifier + '\'' +
                "} " + super.toString();
    }

    public String getIdentifier() {
        return identifier;
    }

    public Symbol getSymbol() {
        if (symbol == null)
            return new Symbol(identifier, true);
        return symbol;
    }
}
