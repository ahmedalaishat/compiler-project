package Java.AST.Function;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class FunctionBody extends Node {
    private ArrayList<Instruction> instructions = new ArrayList<>();

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Instruction instruction : instructions) {
            instruction.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "FunctionBody{} " + super.toString();
    }

    public ArrayList<Instruction> getInstructions() {
        return instructions;
    }
}
