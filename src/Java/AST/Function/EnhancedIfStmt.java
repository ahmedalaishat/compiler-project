package Java.AST.Function;

import Java.AST.Node.IEnhancedIfStmt;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class EnhancedIfStmt extends Node implements IEnhancedIfStmt {
    private IEnhancedIfStmt con;
    private IEnhancedIfStmt t;
    private IEnhancedIfStmt f;

    public void setCon(IEnhancedIfStmt con) {
        this.con = con;
    }

    public void setT(IEnhancedIfStmt t) {
        this.t = t;
    }

    public void setF(IEnhancedIfStmt f) {
        this.f = f;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        con.accept(visitor);
        if (t != null)
            t.accept(visitor);
        if (f != null)
            f.accept(visitor);
    }

    @Override
    public String toString() {
        return "EnhancedIfStmt{} " + super.toString();
    }
}
