package Java.AST.Function;

import Java.AST.Node.IArgument;
import Java.AST.Node.IEnhancedIfStmt;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class BooleanValue extends Node implements IArgument, IEnhancedIfStmt {
    private Type type;
//if we using ! or true or false or identifier other case it will be null
    private Object con;
//    can be string or boolean or number or identifier
    private Object left;
    private Object right;
    private String operation;

    public void setType(Type type) {
        this.type = type;
    }

    public void setCon(Object con) {
        this.con = con;
    }

    public void setLeft(Object left) {
        this.left = left;
    }

    public void setRight(Object right) {
        this.right = right;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Object getCon() {
        return con;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (con != null && con instanceof Node) {
            ((Node) con).accept(visitor);
        }
        if (left != null && left instanceof Node) {
            ((Node) left).accept(visitor);
        }
        if (right != null && right instanceof Node) {
            ((Node) right).accept(visitor);
        }
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return "BooleanValue{" +
                "type=" + type +
                ", operation='" + operation + '\'' +
                "} " + super.toString();
    }

    public enum Type {TRUE, FALSE, IDENTIFIER, NOT, COMPARE_NUM, COMPARE_STR, COMPARE_BOOL}
}
