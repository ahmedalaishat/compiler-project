package Java.AST.Function;

import Java.AST.Visitor.ASTVisitor;

public class BreakIns extends Instruction {
    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "BreakIns{} " + super.toString();
    }
}
