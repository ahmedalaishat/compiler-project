package Java.AST.Function;

import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class CallFunctionIns extends Instruction {
    private String functionName;
    private ArrayList<VariableValue> arguments = new ArrayList<>();

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public ArrayList<VariableValue> getArguments() {
        return arguments;
    }

    public void setArguments(ArrayList<VariableValue> arguments) {
        this.arguments = arguments;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (int i = 0; i < arguments.size(); i++) {
            arguments.get(i).accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "CallFunctionIns{" +
                "functionName='" + functionName + '\'' +
                "} " + super.toString();
    }
}
