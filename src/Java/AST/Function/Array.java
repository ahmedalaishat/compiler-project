package Java.AST.Function;

import Java.AST.Node.IArgument;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class Array extends Node implements IArgument {
    private ArrayList<VariableValue> variableValues;

    public ArrayList<VariableValue> getVariableValues() {
        return variableValues;
    }

    public void setVariableValues(ArrayList<VariableValue> variableValues) {
        this.variableValues = variableValues;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (variableValues != null)
            for (VariableValue variableValue : variableValues) {
                variableValue.accept(visitor);
            }
    }

    @Override
    public String toString() {
        return "Array{} " + super.toString();
    }
}
