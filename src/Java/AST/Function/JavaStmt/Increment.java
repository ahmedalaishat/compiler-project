package Java.AST.Function.JavaStmt;

import Java.AST.Visitor.ASTVisitor;

public class Increment extends JavaStmt {
    enum Type{before,after}
    private String variable;
    private Decrement.Type type;

    public Decrement.Type getType() {
        return type;
    }

    public void setType(Decrement.Type type) {
        this.type = type;
    }


    @Override
    public String toString() {
        return "Increment{" +
                "variable='" + variable + '\'' +
                '}';
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }
}
