package Java.AST.Function.JavaStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class ForUpdate extends Node {
    private ArrayList<String> identifiers = new ArrayList<>();
    private String val;
    private Increment increment;
    private Decrement decrement;

    public String getVal() {
        return val;
    }

    public ArrayList<String> getIdentifiers() {
        return identifiers;
    }

    public Increment getIncrement() {
        return increment;
    }

    public void setIncrement(Increment increment) {
        this.increment = increment;
    }

    public Decrement getDecrement() {
        return decrement;
    }

    public void setDecrement(Decrement decrement) {
        this.decrement = decrement;
    }

    public void setIdentifiers(ArrayList<String> identifiers) {
        this.identifiers = identifiers;
    }

    public void setVal(String val) {
        this.val = val;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (increment != null)
            increment.accept(visitor);
        if (decrement != null)
            decrement.accept(visitor);
    }

    @Override
    public String toString() {
        return "ForUpdate{" +
                "val='" + val + '\'' +
                "} " + super.toString();
    }
}
