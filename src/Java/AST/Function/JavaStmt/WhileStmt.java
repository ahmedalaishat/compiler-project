package Java.AST.Function.JavaStmt;

import Java.AST.Function.BooleanValue;
import Java.AST.Function.Instruction;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.id;
import static Java.Main.scopesStack;

public class WhileStmt extends JavaStmt {
    private BooleanValue booleanValue;
    private ArrayList<Instruction> instructions;
    private Scope scope;

    public Scope getScope() {
        if (scope==null)
            scope=new Scope("while_" + id++);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void setBooleanValue(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        booleanValue.accept(visitor);
        for (Instruction instruction : instructions
        ) {
            instruction.accept(visitor);
        }
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "WhileStmt{} " + super.toString();
    }
}
