package Java.AST.Function.JavaStmt;

import Java.AST.Function.BooleanValue;
import Java.AST.Function.Instruction;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.id;
import static Java.Main.scopesStack;

public class IfStmt extends JavaStmt {
    Scope scope;

    public Scope getScope() {
        if (scope==null)
            scope= new Scope("if_" + id++);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    private BooleanValue booleanValue;
    private ArrayList<Instruction> instructions;
    private ElseStmt elseStmt;


    public void setBooleanValue(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    public void setElseStmt(ElseStmt elseStmt) {
        this.elseStmt = elseStmt;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        booleanValue.accept(visitor);
        for (Instruction instruction : instructions) {
            instruction.accept(visitor);
        }
        if (elseStmt != null)
            elseStmt.accept(visitor);
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "IfStmt{} " + super.toString();
    }
}
