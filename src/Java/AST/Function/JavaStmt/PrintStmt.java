package Java.AST.Function.JavaStmt;

import Java.AST.Function.VariableValue;
import Java.AST.Visitor.ASTVisitor;

public class PrintStmt extends JavaStmt {
    private VariableValue val;

    public void setVal(VariableValue val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "PrintStmt{" +
                "val='" + val + '\'' +
                "} " + super.toString();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (val!=null)
            val.accept(visitor);
    }

    public VariableValue getVal() {
        return val;
    }
}
