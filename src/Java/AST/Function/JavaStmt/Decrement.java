package Java.AST.Function.JavaStmt;

import Java.AST.Visitor.ASTVisitor;

public class Decrement extends JavaStmt {
    public enum Type{before,after}
    private String variable;
    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Decrement{" +
                "variable='" + variable + '\'' +
                '}';
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
