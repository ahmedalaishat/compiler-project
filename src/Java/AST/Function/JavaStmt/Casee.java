package Java.AST.Function.JavaStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class Casee extends Node {
    private CaseBody caseBody;

    public void setCaseBody(CaseBody caseBody) {
        this.caseBody = caseBody;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        caseBody.accept(visitor);
    }

    @Override
    public String toString() {
        return "Casee{} " + super.toString();
    }
}
