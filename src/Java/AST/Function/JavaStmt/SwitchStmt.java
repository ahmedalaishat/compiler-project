package Java.AST.Function.JavaStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.id;
import static Java.Main.scopesStack;

public class SwitchStmt extends JavaStmt {
    private String identifierName;
    private ArrayList<Casee> casees;
    private Scope scope;

    public Scope getScope() {
        if (scope==null)
            scope=new Scope("switch_" + id++);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public void setCasees(ArrayList<Casee> casees) {
        this.casees = casees;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Casee casee : casees
        ) {
            casee.accept(visitor);
        }
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "SwitchStmt{" +
                "identifierName='" + identifierName + '\'' +
                "} " + super.toString();
    }

    public String getIdentifierName() {
        return identifierName;
    }
}
