package Java.AST.Function.JavaStmt;

import Java.AST.Function.BreakIns;
import Java.AST.Function.Instruction;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class CaseBody extends Node {
    ArrayList<Instruction> instructions;
    BreakIns breakIns;

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    public void setBreakIns(BreakIns breakIns) {
        this.breakIns = breakIns;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Instruction instruction : instructions
        ) {
            instruction.accept(visitor);
        }
        if (breakIns != null)
            breakIns.accept(visitor);
    }

    @Override
    public String toString() {
        return "CaseBody{} " + super.toString();
    }
}
