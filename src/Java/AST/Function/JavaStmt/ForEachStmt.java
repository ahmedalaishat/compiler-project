package Java.AST.Function.JavaStmt;

import Java.AST.Function.Instruction;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.id;
import static Java.Main.scopesStack;

public class ForEachStmt extends JavaStmt {
    private String varName;
    private String arrayName;
    private ArrayList<Instruction> instructions;
    private Scope scope;

    public Scope getScope() {
        if (scope == null)
            scope = new Scope("foreach_" + id++);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public void setArrayName(String arrayName) {
        this.arrayName = arrayName;
    }

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Instruction instruction : instructions
        ) {
            instruction.accept(visitor);
        }
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "ForEachStmt{" +
                "varName='" + varName + '\'' +
                ", arrayName='" + arrayName + '\'' +
                "} " + super.toString();
    }

    public String getVarName() {
        return varName;
    }
}
