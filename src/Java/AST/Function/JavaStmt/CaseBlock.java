package Java.AST.Function.JavaStmt;

public class CaseBlock extends Casee {
    private Object value;

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "CaseBlock{" +
                "value=" + value +
                "} " + super.toString();
    }
}
