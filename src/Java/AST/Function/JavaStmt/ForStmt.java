package Java.AST.Function.JavaStmt;

import Java.AST.Function.BooleanValue;
import Java.AST.Function.Instruction;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.id;
import static Java.Main.scopesStack;

public class ForStmt extends JavaStmt {
    private ArrayList<Instruction> instructions = new ArrayList<>();
    private ForInitialization forInitialization;
    private ForUpdate forUpdate;
    private BooleanValue booleanValue;
    private Scope scope;

    public Scope getScope() {
        if (scope==null)
            scope = new Scope("for_" + id++);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    public void setForInitialization(ForInitialization forInitialization) {
        this.forInitialization = forInitialization;
    }

    public void setForUpdate(ForUpdate forUpdate) {
        this.forUpdate = forUpdate;
    }

    public void setBooleanValue(BooleanValue booleanValue) {
        this.booleanValue = booleanValue;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        forInitialization.accept(visitor);
        booleanValue.accept(visitor);
        forUpdate.accept(visitor);
        for (Instruction instruction : instructions) {
            instruction.accept(visitor);
        }
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "ForStmt{} " + super.toString();
    }
}
