package Java.AST.Function.JavaStmt;

import Java.AST.Function.VarAssign;
import Java.AST.Function.VarDeclaration;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class ForInitialization extends Node {
    private VarDeclaration varDeclaration;
    private VarAssign varAssign;

    public VarDeclaration getVarDeclaration() {
        return varDeclaration;
    }

    public void setVarDeclaration(VarDeclaration varDeclaration) {
        this.varDeclaration = varDeclaration;
    }

    public VarAssign getVarAssign() {
        return varAssign;
    }

    public void setVarAssign(VarAssign varAssign) {
        this.varAssign = varAssign;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (varAssign!=null)
            varAssign.accept(visitor);
        if (varDeclaration!=null)
            varDeclaration.accept(visitor);
    }

    @Override
    public String toString() {
        return "ForInitialization{" +
                "varDeclaration=" + varDeclaration +
                ", varAssign=" + varAssign +
                "} " + super.toString();
    }
}
