package Java.AST.Function;

import Java.AST.Node.IArgument;
import Java.AST.Node.IEnhancedIfStmt;
import Java.AST.Node.Node;
import Java.AST.SqlStmt.SelectStmt;
import Java.AST.Visitor.ASTVisitor;
import Java.AST.Visitor.AcceptVisit;
import Java.SymbolTable.type.Type;

public class VariableValue extends Node implements IArgument, IEnhancedIfStmt {

    public enum VariableType {STRING, BOOLEAN, NUMBER, INCREMENT, DECREMENT, NULL, SELECT, CALL_FUN, ENHANCED_IF, IDENTIFIER}

    private VariableType type;
    private Object value;

    public Type getSymbolType() {
        if (type.equals(VariableType.SELECT)) return ((SelectStmt)value).getFlattedType();
        if (type.equals(VariableType.STRING)) return Java.SymbolTable.type.Type.createPrimitiveFrom(Type.STRING_CONST);
        if (type.equals(VariableType.BOOLEAN)) return Java.SymbolTable.type.Type.createPrimitiveFrom(Type.BOOLEAN_CONST);
        if (type.equals(VariableType.NUMBER)||type.equals(VariableType.INCREMENT)||type.equals(VariableType.DECREMENT))
            return Java.SymbolTable.type.Type.createPrimitiveFrom(Java.SymbolTable.type.Type.NUMBER_CONST);
        return null;
    }

    public VariableType getType() {
        return type;
    }

    public void setType(VariableType type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (value instanceof AcceptVisit)
            ((AcceptVisit) value).accept(visitor);
    }

    @Override
    public String toString() {
        return "VariableValue{" +
                "type=" + type +
                ", value=" + value +
                "} " + super.toString();
    }
}
