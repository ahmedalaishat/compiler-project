package Java.AST.Function;

import Java.AST.Node.IArgument;
import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class Argument extends Node {
    IArgument iArgument;

    public IArgument getiArgument() {
        return iArgument;
    }

    public void setiArgument(IArgument iArgument) {
        this.iArgument = iArgument;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (iArgument != null && iArgument instanceof Node)
            ((Node) iArgument).accept(visitor);
    }

    @Override
    public String toString() {
        return "Argument{} " + super.toString();
    }
}
