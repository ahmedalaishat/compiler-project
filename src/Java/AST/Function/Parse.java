package Java.AST.Function;

import Java.AST.Function.JavaStmt.PrintStmt;
import Java.AST.Node.Node;
import Java.AST.SqlStmt.GlobalStatement;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import java.util.ArrayList;

import static Java.Main.scopesStack;

public class Parse extends Node {
    Scope scope;
    private ArrayList<GlobalStatement> globalStatements = new ArrayList<>();

    public ArrayList<VarDeclaration> getVarDeclarations() {
        ArrayList<VarDeclaration> varDeclarations = new ArrayList<>();
        for (GlobalStatement globalStatement : globalStatements)
            if (globalStatement instanceof Function)
                for (Instruction instruction : ((Function) globalStatement).getFunctionBody().getInstructions())
                    if (instruction instanceof VarDeclaration)
                        varDeclarations.add((VarDeclaration) instruction);
        return varDeclarations;
    }

    public String getFunName() {
        ArrayList<VarDeclaration> varDeclarations = new ArrayList<>();
        for (GlobalStatement globalStatement : globalStatements)
            if (globalStatement instanceof Function)
                return ((Function) globalStatement).getFunctionName();
        return null;
    }

    public ArrayList<VarAssign> getVarAssigns() {
        ArrayList<VarAssign> varAssigns = new ArrayList<>();
        for (GlobalStatement globalStatement : globalStatements)
            if (globalStatement instanceof Function)
                for (Instruction instruction : ((Function) globalStatement).getFunctionBody().getInstructions())
                    if (instruction instanceof VarAssign)
                        varAssigns.add((VarAssign) instruction);
        return varAssigns;
    }

    public ArrayList<PrintStmt> getPrintStmts() {
        ArrayList<PrintStmt> printStmts = new ArrayList<>();
        for (GlobalStatement globalStatement : globalStatements)
            if (globalStatement instanceof Function)
                for (Instruction instruction : ((Function) globalStatement).getFunctionBody().getInstructions())
                    if (instruction instanceof PrintStmt)
                        printStmts.add((PrintStmt) instruction);
        return printStmts;
    }

    public void setGlobalStatements(ArrayList<GlobalStatement> globalStatements) {
        this.globalStatements = globalStatements;
    }

    public Scope getScope() {
        if (scope == null)
            scope = new Scope("global");
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    public ArrayList<GlobalStatement> getGlobalStatements() {
        return globalStatements;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (int i = 0; i < globalStatements.size(); i++) {
            globalStatements.get(i).accept(visitor);
        }
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "Parse{} " + super.toString();
    }
}
