package Java.AST.Function;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Symbol;
import Java.SymbolTable.type.Type;

public class VarDeclaration extends Instruction {
    private String identifierName;
    private VariableValue variableValue;
    private Symbol symbol;

    public String getIdentifierName() {
        return identifierName;
    }

    public void setIdentifierName(String identifierName) {
        this.identifierName = identifierName;
    }

    public void setVariableValue(VariableValue variableValue) {
        this.variableValue = variableValue;
    }

    public Symbol getSymbol() {
        if (symbol != null)
            return symbol;
        Type type;
        if (variableValue == null) {
            type = null;
        } else {
            type = variableValue.getSymbolType();
        }
        return this.symbol = new Symbol(identifierName, type);
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (variableValue != null)
            variableValue.accept(visitor);
    }

    @Override
    public String toString() {
        return "VarDeclaration{" +
                ", identifierName='" + identifierName + '\'' +
                "} " + super.toString();
    }

    public VariableValue getVariableValue() {
        return variableValue;
    }
}
