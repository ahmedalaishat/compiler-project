package Java.AST.Function;

import Java.AST.SqlStmt.GlobalStatement;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.Scope;

import static Java.Main.scopesStack;

public class Function extends GlobalStatement {
    Scope scope;

    public String getFunctionName() {
        return functionName;
    }

    public FunctionHeader getFunctionHeader() {
        return functionHeader;
    }

    public FunctionBody getFunctionBody() {
        return functionBody;
    }

    public Scope getScope() {
        if (scope == null)
            scope = new Scope(functionName);
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    private String functionName;
    private FunctionHeader functionHeader;
    private FunctionBody functionBody;

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public void setFunctionHeader(FunctionHeader functionHeader) {
        this.functionHeader = functionHeader;
    }

    public void setFunctionBody(FunctionBody functionBody) {
        this.functionBody = functionBody;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        functionHeader.accept(visitor);
        functionBody.accept(visitor);
        scopesStack.pop();
    }

    @Override
    public String toString() {
        return "Function{" +
                "functionName='" + functionName + '\'' +
                "} " + super.toString();
    }
}
