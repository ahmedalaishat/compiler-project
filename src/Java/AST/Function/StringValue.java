package Java.AST.Function;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class StringValue extends Node {
    private Type type;
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public enum Type {STRING, IDENTIFIER, CHAR}
}
