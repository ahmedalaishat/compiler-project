package Java.AST.Node;

import Java.SymbolTable.type.IMergedTable;

public interface ITable extends INode {
    IMergedTable getType();
    String getTableAlias();
}
