package Java.AST.Node;

import Java.AST.Visitor.AcceptVisit;

public interface INode extends AcceptVisit {
    public void setLine(int line);

    public void setCol(int col);
}
