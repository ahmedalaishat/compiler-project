package Java.AST.Node;

public abstract class Node implements INode {

    private int line;
    private int col;

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public int getCol() {
        return col;
    }

    @Override
    public String toString() {
        return "{" +
                "line=" + line +
                ", col=" + col +
                '}';
    }
    //
//    @Override
//    public String toString() {
//        return getClass().getName() + " " + getCol() + "," + getLine();
//    }
}
