package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class ColumnExpr extends Expr {
    private ArrayList<String> tableNames=new ArrayList<>();
    private String columnName;

    public ArrayList<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(ArrayList<String> tableNames) {
        this.tableNames = tableNames;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getName() {
        return tableNames + "." + columnName;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        StringBuilder builder=new StringBuilder();
        if (tableNames.size()>0){
            for (String s : tableNames) {
                builder.append(s).append("$");
            }
        }
        builder.append(columnName);
        return builder.toString();
    }

    @Override
    public String getPredicate() {
        StringBuilder builder=new StringBuilder();
        if (tableNames.size()>0){
            for (String s : tableNames) {
                builder.append(s).append("$");
            }
        }
        builder.append(columnName);
        return builder.toString();
    }
}
