package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class OrderingTerm extends Node {
    Expr expr;
    String collectionName;
    boolean desc;

    public void setDesc(boolean desc) {
        this.desc = desc;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (expr != null)
            expr.accept(visitor);
    }

    public Expr getExpr() {
        return expr;
    }

    @Override
    public String toString() {
        return "OrderingTerm{" +
                "collectionName='" + collectionName + '\'' +
                ", desc=" + desc +
                "} " + super.toString();
    }
}
