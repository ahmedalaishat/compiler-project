package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.AggregationFunction;

public class CreateAggFunStmt extends Statement {
    private String functionName;
    private String jarPath;
    private String className;
    private String methodName;
    private String returnType;
    private String arrayType;

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public void setJarPath(String jarPath) {
        this.jarPath = jarPath;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public void setArrayType(String arrayType) {
        this.arrayType = arrayType;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public AggregationFunction getAggregationFunction(){
        return new AggregationFunction(functionName,
                jarPath.substring(1,jarPath.length()-1),

                className.substring(1,className.length()-1),
                methodName.substring(1,methodName.length()-1),
                returnType, arrayType);
    }

    @Override
    public String toString() {
        return "CreateAggFunStmt{" +
                "functionName='" + functionName + '\'' +
                ", jarPath='" + jarPath + '\'' +
                ", className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", returnType='" + returnType + '\'' +
                ", array=" + arrayType +
                "} " + super.toString();
    }
}
