/*-
 * #%L
 * JSQLParser library
 * %%
 * Copyright (C) 2004 - 2019 JSQLParser
 * %%
 * Dual licensed under GNU LGPL 2.1 or Apache License 2.0
 * #L%
 */
package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.SqlStmt.table.ListTable;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.type.IMergedTable;

import java.util.ArrayList;

public class JoinClause extends Node {
    public enum JoinOperator {INNER_JOIN, LEFT_JOIN, CROSS}
    private ArrayList<JoinOperator> joinOperators;
    private ArrayList<Expr> joinConstraints = new ArrayList<>();
    private ListTable listTable;

    public ArrayList<JoinOperator> getJoinOperators() {
        return joinOperators;
    }

    public void setJoinOperators(ArrayList<JoinOperator> joinOperators) {
        this.joinOperators = joinOperators;
    }

    public void setJoinConstraints(ArrayList<Expr> joinConstraints) {
        this.joinConstraints = joinConstraints;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr : joinConstraints
        ) {
            expr.accept(visitor);
        }
        if (listTable!=null)
            listTable.accept(visitor);
    }

    @Override
    public String toString() {
        return "JoinClause{" +
                "} " + super.toString();
    }
    public IMergedTable getMergedTable(){
        if (listTable.getiTables().size()==1)
            return listTable.getiTables().get(0).getType();
        return listTable.getType(joinOperators,joinConstraints);
    }

    public ArrayList<Expr> getJoinConstraints() {
        return joinConstraints;
    }

    public ListTable getListTable() {
        return listTable;
    }

    public void setListTable(ListTable listTable) {
        this.listTable = listTable;
    }
}
