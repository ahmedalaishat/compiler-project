package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

public class UnaryOperationExpr extends Expr {
    private String unaryOperator;
    private Expr expr;

    public void setUnaryOperator(String unaryOperator) {
        this.unaryOperator = unaryOperator;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        expr.accept(visitor);
    }

    @Override
    public String toString() {
        return "UnaryOperationExpr{" +
                "unaryOperator='" + unaryOperator + '\'' +
                "} " + super.toString();
    }
}
