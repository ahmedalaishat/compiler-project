package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

public class ExistExpr extends Expr {
    private prefix prefix;
    private SelectStmt selectStmt;

    public void setPrefix(ExistExpr.prefix prefix) {
        this.prefix = prefix;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        selectStmt.accept(visitor);
    }

    @Override
    public String toString() {
        return "ExistExpr{} " + super.toString();
    }

    public enum prefix {Exist, NotExist}
}
