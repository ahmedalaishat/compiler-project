package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class Where extends Node {
    private Expr expr;
    private SubExpr subExpr;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (expr != null)
            expr.accept(visitor);
        if (subExpr != null)
            subExpr.accept(visitor);
    }

    @Override
    public String toString() {
        return expr!=null?expr.toString():super.toString();
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setSubExpr(SubExpr subExpr) {
        this.subExpr = subExpr;
    }

    public Expr getExpr() {
        return expr;
    }

    public SubExpr getSubExpr() {
        return subExpr;
    }

//    public String check(String tableName) {
//        if (expr instanceof InExpr && ((InExpr) expr).getSelectStmt() != null) {
//            if (((InExpr) expr).getLeft() instanceof ColumnExpr) {
//                if(((InExpr) expr).getSelectStmt().getFlatType()
//                        .canIn(Main.symbolTable.getDeclaredType(tableName).isColumnTypeExist(
//                                ((ColumnExpr) ((InExpr) expr).getLeft()).getName()))){
//                    return null;
//                }
//                else return ((ColumnExpr) ((InExpr) expr).getLeft()).getColumnName();
//            } else return null;
//        }
//        else
//            return null;
//    }
}
