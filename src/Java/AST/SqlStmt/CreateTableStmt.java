package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.type.TableType;
import Java.SymbolTable.type.Type;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CreateTableStmt extends Statement {
    private String tableName;
    private String typeName;
    private String path;
    private ArrayList<ColumnDef> columnDefs = new ArrayList<>();

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setColumnDefs(ArrayList<ColumnDef> columnDefs) {
        this.columnDefs = columnDefs;
    }


    public TableType getType() throws Exception {
        TableType tableType = new TableType();
        tableType.setTypeName(tableName);
        LinkedHashMap<String, Type> map = new LinkedHashMap<>();
        for (int i = 0; i < columnDefs.size(); i++) {
            Type columnType = Type.createPrimitiveFrom(columnDefs.get(i).getTypeNames().get(0).getName());
            if (columnType == null) {
                columnType = Main.symbolTable.getDeclaredType(columnDefs.get(i).getTypeNames().get(0).getName());
                if (columnType == null)
                    throw new Exception(columnDefs.get(i).getColumnName() + " not exists.");
            }
            map.put(columnDefs.get(i).getColumnName(), columnType);
        }
        tableType.setColumns(map);
        tableType.setType(typeName);
        tableType.setPath(path);
        return tableType;
    }


    public int getCount(String name) {
        int count = 0;
        for (ColumnDef columnDef : columnDefs) {
            if (name.equals(columnDef.getColumnName()))
                count++;
        }
        return count;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (ColumnDef columnDef : columnDefs
        ) {
            columnDef.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "CreateTableStmt{" +
                "tableName='" + tableName + '\'' +
                ", typeName='" + typeName + '\'' +
                ", path='" + path + '\'' +
                ", columnDefs=" + columnDefs +
                "} " + super.toString();
    }

    public String getTableName() {
        return tableName;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getPath() {
        return path;
    }

    public ArrayList<ColumnDef> getColumnDefs() {
        return columnDefs;
    }


}
