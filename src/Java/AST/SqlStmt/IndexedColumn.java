package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class IndexedColumn extends Node {
    String columnName;
    String collationName;

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setCollationName(String collationName) {
        this.collationName = collationName;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "IndexedColumn{" +
                "columnName='" + columnName + '\'' +
                ", collationName='" + collationName + '\'' +
                "} " + super.toString();
    }
}
