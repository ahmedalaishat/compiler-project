package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.type.PrimitiveType;
import Java.SymbolTable.type.TableType;
import Java.SymbolTable.type.Type;

import java.util.ArrayList;
import java.util.HashMap;

public class CreateTypeStmt extends Statement {
    private String typeName;
    private ArrayList<ColumnDef> columnDefs = new ArrayList<>();

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public void setColumnDefs(ArrayList<ColumnDef> columnDefs) {
        this.columnDefs = columnDefs;
    }

    public TableType getType() {
        TableType tableType = new TableType();
        tableType.setTypeName(typeName);
        HashMap<String, Type> map = new HashMap<>();
        for (ColumnDef columnDef : columnDefs) {
//            TODO must add tables as a column
            if (Main.symbolTable.getDeclaredType(columnDef.getColumnName()) != null) {
                map.put(columnDef.getColumnName(), Main.symbolTable.getDeclaredType(columnDef.getTypeNames().get(0).getName()));
            } else {
                PrimitiveType columnType = Type.createPrimitiveFrom(columnDef.getTypeNames().get(0).getName());
                map.put(columnDef.getColumnName(), columnType);
            }
        }
        tableType.setColumns(map);
        return tableType;
    }

    public int getCount(String name) {
        int count = 0;
        for (ColumnDef columnDef : columnDefs) {
            if (name.equals(columnDef.getColumnName()))
                count++;
        }
        return count;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (ColumnDef columnDef : columnDefs)
            columnDef.accept(visitor);
    }

    @Override
    public String toString() {
        return "CreateTypeStmt{" +
                "typeName='" + typeName + '\'' +
                ", columnDefs=" + columnDefs +
                "} " + super.toString();
    }

    public ArrayList<ColumnDef> getColumnDefs() {
        return columnDefs;
    }
}
