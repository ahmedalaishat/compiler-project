package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.AggregationFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionExpr extends Expr {
    private String functionName;
    private boolean star;
    private boolean disc;
    private ArrayList<Expr> exprs = new ArrayList<>();

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public void setDisc(boolean disc) {
        this.disc = disc;
    }

    public void setExprs(ArrayList<Expr> exprs) {
        this.exprs = exprs;
    }

    public ArrayList<Expr> getExprs() {
        return exprs;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr : exprs
        ) {
            expr.accept(visitor);
        }
    }

    @Override
    public String getPredicate() {
        return "p.aggVal";
    }

    @Override
    public String toString() {
        return functionName;
    }

    public String getFunctionName() {
        return functionName;
    }


    public List<AggregationFunction> getAgg() {
        return Main.symbolTable.getDeclaredAggregationFunction().
                stream().filter(p -> functionName.equals(p.getAggregationFunctionName())).collect(Collectors.toList());
    }
}
