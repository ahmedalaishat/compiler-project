package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class Limit extends Node {
    private Expr expr;
    private boolean offset;

    private ArrayList<Expr> exprs;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr : exprs) {
            expr.accept(visitor);
        }
    }

    public boolean isOffset() {
        return offset;
    }

    public void setOffset(boolean offset) {
        this.offset = offset;
    }

    public ArrayList<Expr> getExprs() {
        return exprs;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public Expr getExpr() {
        return expr;
    }

    public void setExprs(ArrayList<Expr> exprs) {
        this.exprs = exprs;
    }

    @Override
    public String toString() {
        return "Limit{} " + super.toString();
    }
}
