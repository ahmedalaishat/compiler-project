package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class InExpr extends Expr {
    private boolean not;
    private Expr left;

    private ArrayList<Expr> exprs = new ArrayList<>();
    private SelectStmt selectStmt;
    private String tableName;

    public void setNot(boolean not) {
        this.not = not;
    }

    public Expr getLeft() {
        return left;
    }

    public void setLeft(Expr left) {
        this.left = left;
    }

    public void setExprs(ArrayList<Expr> exprs) {
        this.exprs = exprs;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }


    public void setTableName(String tableName) {
        this.tableName = tableName;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr :
                exprs) {
            expr.accept(visitor);
        }
        if (selectStmt != null)
            selectStmt.accept(visitor);
    }

    @Override
    public String getPredicate() {
        StringBuilder builder = new StringBuilder();
        if (not)
            builder.append("!");
        builder.append("(");
        for (int i = 1; i < exprs.size(); i++) {
            if (i > 1)
                builder.append(" || ");
            builder.append("p.").append(left.getPredicate());
            if (exprs.get(i).isString())
                builder.append(".equals(").append(exprs.get(i)).append(")");
            else
                builder.append("==").append(exprs.get(i));
        }
        builder.append(")");
        return builder.toString();
    }

    public boolean isNot() {
        return not;
    }

    public ArrayList<Expr> getExprs() {
        return exprs;
    }

    public SelectStmt getSelectStmt() {
        return selectStmt;
    }

    public String getTableName() {
        return tableName;
    }
}
