package Java.AST.SqlStmt;

import Java.AST.Node.IColumnDefault;
import Java.AST.Node.Node;

public abstract class Expr extends Node implements IColumnDefault {
    public boolean isString() {
        return false;
    }

    public String getPredicate() {
        return toString();
    }
}
