package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;

import java.util.ArrayList;

public class GroupBy extends Node {
    private Having having;
    private ArrayList<Expr> exprs;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr : exprs) {
            expr.accept(visitor);
        }
        if (having!=null)
            having.accept(visitor);
    }

    public Having getHaving() {
        return having;
    }

    public void setHaving(Having having) {
        this.having = having;
    }

    public void setExprs(ArrayList<Expr> exprs) {
        this.exprs = exprs;
    }

    @Override
    public String toString() {
        return "GroupBy{" +
                "having=" + having +
                "} " + super.toString();
    }

    public ArrayList<Expr> getExprs() {
        return exprs;
    }

    public ArrayList<String> getAggFunctions() {
        ArrayList<String> functions = new ArrayList<>();
        for (Expr expr :
                exprs) {
            if (expr instanceof FunctionExpr
                    && Main.symbolTable.aggFunctionExists(((FunctionExpr) expr).getFunctionName()))
                functions.add(((FunctionExpr) expr).getFunctionName());
        }
        return functions;
    }
}
