package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class Values extends Node {
    ArrayList<Expr> exprs;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (Expr expr : exprs) {
            expr.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "Values{} " + super.toString();
    }

    public void setExprs(ArrayList<Expr> exprs) {
        this.exprs = exprs;
    }

    public ArrayList<Expr> getExprs() {
        return exprs;
    }
}
