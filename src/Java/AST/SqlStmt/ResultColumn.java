package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class ResultColumn extends Node {
    private String columnAlias;
    private Expr expr;
    private String tableName;
    private boolean star;

    public void setColumnAlias(String columnAlias) {
        this.columnAlias = columnAlias;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (expr != null)
            expr.accept(visitor);
    }

    @Override
    public String toString() {
        if (expr != null)
            return expr.toString();
        return super.toString();
    }

    public boolean isFunction(){
        return expr instanceof FunctionExpr;
    }

    public String getColumnAlias() {
        return columnAlias;
    }

    public Expr getExpr() {
        return expr;
    }

    public String getTableName() {
        return tableName;
    }

    public boolean isStar() {
        return star;
    }
}
