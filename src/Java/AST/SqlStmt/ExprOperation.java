package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class ExprOperation extends Node {
    public enum ExprOper {
        PIPE2("||"), MULTIPLY("*"), DIVISION("/"), PERCENT("%"), PLUS("+"), MINUS("-"), LTLT(null), GTGT(null),
        AMP(null), PIPE(null), LT("<"), LT_EQ("<="), GT(">"), GT_EQ(">="), SEQ("=="), EQ("=="), NOT_EQ1("!="), NOT_EQ2("!="),
        K_IS(null), K_NOT(null), K_LIKE("like"), K_NOT_LIKE("note like"), K_GLOB(null), K_MATCH(null), K_REGEXP(null), K_AND("&&"), K_OR("||");
        private String oper;

        ExprOper(String oper) {
            this.oper = oper;
        }
    }

    private ExprOper exprOper;

    public ExprOperation(ExprOper exprOper) {
        this.exprOper = exprOper;
    }

    public ExprOper getExprOper() {
        return exprOper;
    }

    public void setExprOper(ExprOper exprOper) {
        this.exprOper = exprOper;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return exprOper.oper;
    }
}
