package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

public class LiteralValue extends Expr {
    private Object value;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    public Object getValue() {
        return value;
    }

    @Override
    public boolean isString() {
        return value instanceof String;
    }
}
