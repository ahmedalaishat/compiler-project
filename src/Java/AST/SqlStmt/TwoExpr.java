package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;

public class TwoExpr extends Expr {
    private Expr lExpr;
    private Expr rExpr;
    private ExprOperation operation;

    public void setlExpr(Expr lExpr) {
        this.lExpr = lExpr;
    }

    public void setrExpr(Expr rExpr) {
        this.rExpr = rExpr;
    }


    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (lExpr != null)
            lExpr.accept(visitor);
        if (rExpr != null)
            rExpr.accept(visitor);
    }

    private String getLikeStr(String left, String right, boolean like) {
        StringBuilder builder = new StringBuilder();
        if (!like) builder.append("!");
        builder.append("(").append(left);
        if (right.startsWith("\"%") && right.endsWith("%\""))
            builder.append(".contains(\"").append(right, 2, right.length() - 2).append("\"");
        else if (right.startsWith("\"%")) builder.append(".endsWith(\"").append(right.substring(2));
        else if (right.endsWith("%\""))
            builder.append(".startsWith(").append(right, 0, right.length() - 2).append("\"");
        else builder.append(".equals(").append(right);
        builder.append("))");
        return builder.toString();
    }

    @Override
    public String getPredicate() {
        if (operation.toString() == null) return null;
        String left = (lExpr instanceof ColumnExpr ? "p." : "") + lExpr.getPredicate();
        String right = (rExpr instanceof ColumnExpr ? "p." : "") + rExpr.getPredicate();
        if (operation.getExprOper().equals(ExprOperation.ExprOper.K_LIKE))
            return getLikeStr(left, right, true);
        if (operation.getExprOper().equals(ExprOperation.ExprOper.K_NOT_LIKE))
            return getLikeStr(left, right, false);
        if ((operation.toString().equals("==") || (operation.toString().equals("="))) && ((lExpr.isString() || rExpr.isString()) ||(
                rExpr instanceof ColumnExpr && ((ColumnExpr) rExpr)
                        .getColumnName().endsWith("initials"))))
            return left + ".equals(" + right + ")";
        if (operation.toString().equals("!=") && ((lExpr.isString() || rExpr.isString()) ||(
                rExpr instanceof ColumnExpr && ((ColumnExpr) rExpr)
                        .getColumnName().endsWith("initials"))))
            return "!" + left + ".equals(" + right + ")";
        return left + operation.toString() + right;
    }

    public Expr getlExpr() {
        return lExpr;
    }

    public Expr getrExpr() {
        return rExpr;
    }

    public ExprOperation getOperation() {
        return operation;
    }

    public void setOperation(ExprOperation operation) {
        this.operation = operation;
    }
}
