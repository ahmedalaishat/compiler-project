package Java.AST.SqlStmt.table;

import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.type.IMergedTable;
import Java.SymbolTable.type.TableType;

public class BasicTable extends Table {
    private String indexName;
    private String tableName;

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public IMergedTable getType() {
        TableType tableType = Main.symbolTable.getDeclaredType(tableName);
        if (tableType!=null)
            tableType.setAlias(super.getTableAlias());
        if (tableType!=null)
            tableType=tableType.clone();
        if (tableType == null)
            tableType = Main.symbolTable.getSymbolType(tableName);
        return tableType;
    }
}
