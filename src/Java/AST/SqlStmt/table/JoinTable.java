package Java.AST.SqlStmt.table;

import Java.AST.SqlStmt.JoinClause;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.type.IMergedTable;

public class JoinTable extends Table {
    private JoinClause joinClause;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (joinClause != null)
            joinClause.accept(visitor);
    }

    public JoinClause getJoinClause() {
        return joinClause;
    }

    public void setJoinClause(JoinClause joinClause) {
        this.joinClause = joinClause;
    }

    @Override
    public IMergedTable getType() {
        return joinClause.getMergedTable();
    }
}
