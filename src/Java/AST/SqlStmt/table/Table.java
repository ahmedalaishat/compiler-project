package Java.AST.SqlStmt.table;

import Java.AST.Node.ITable;
import Java.AST.Node.Node;

public abstract class Table extends Node implements ITable {
    private String tableAlias;

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    @Override
    public String getTableAlias() {
        return tableAlias;
    }
}
