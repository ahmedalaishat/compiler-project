package Java.AST.SqlStmt.table;

import Java.AST.Node.ITable;
import Java.AST.SqlStmt.Expr;
import Java.AST.SqlStmt.JoinClause;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.type.IMergedTable;
import Java.SymbolTable.type.MergedTable;

import java.util.ArrayList;

public class ListTable extends Table {
    private ArrayList<ITable> iTables = new ArrayList<>();

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (ITable tableOrSubQuery : iTables
        ) {
            tableOrSubQuery.accept(visitor);
        }
    }

    public ArrayList<ITable> getiTables() {
        return iTables;
    }

    public void setiTables(ArrayList<ITable> iTables) {
        this.iTables = iTables;
    }


    public IMergedTable getType(ArrayList<JoinClause.JoinOperator> joinOperators, ArrayList<Expr> innerJoinExprs) {
        if (iTables.size() == 1 && iTables.get(0) instanceof BasicTable)
            return iTables.get(0).getType();
        MergedTable mergedTable = new MergedTable();
        mergedTable.setLeft(iTables.get(0).getType());
        for (int i = 1; i < iTables.size() - 1; i++) {
            mergedTable.setOperator(joinOperators.get(i - 1));
            if (mergedTable.getOperator().equals(JoinClause.JoinOperator.INNER_JOIN))
                mergedTable.setInnerJoinExpr(innerJoinExprs.get(i - 1));
            mergedTable.setRight(iTables.get(i).getType());
            MergedTable temp2 = new MergedTable();
            temp2.setLeft(mergedTable);
            mergedTable = temp2;
        }
        mergedTable.setRight(iTables.get(iTables.size() - 1).getType());
        mergedTable.setOperator(joinOperators.get(joinOperators.size() - 1));
        if (mergedTable.getOperator().equals(JoinClause.JoinOperator.INNER_JOIN))
            mergedTable.setInnerJoinExpr(innerJoinExprs.get(innerJoinExprs.size() - 1));
        return mergedTable;
    }

    @Override
    public IMergedTable getType() {
        MergedTable mergedTable = new MergedTable();
        mergedTable.setLeft(iTables.get(0).getType());
        for (int i = 1; i < iTables.size() - 1; i++) {
            mergedTable.setOperator(JoinClause.JoinOperator.CROSS);
            mergedTable.setRight(iTables.get(i).getType());
            MergedTable temp2 = new MergedTable();
            temp2.setLeft(mergedTable);
            mergedTable = temp2;
        }
        mergedTable.setOperator(JoinClause.JoinOperator.CROSS);
        mergedTable.setRight(iTables.get(iTables.size() - 1).getType());
        return mergedTable;
    }
}
