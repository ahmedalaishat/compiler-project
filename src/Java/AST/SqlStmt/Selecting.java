package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;
import Java.Main;
import Java.SymbolTable.AggregationFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Selecting extends Node {
    private boolean distinct;
    private boolean all;
    private ArrayList<ResultColumn> resultColumns;

    public boolean isDistinct() {
        return distinct;
    }

    public boolean isAll() {
        return all;
    }

    public ArrayList<ResultColumn> getResultColumns() {
        return resultColumns;
    }

    public ArrayList<String> getResultColumnsNames() {
        if (resultColumns.get(0).isStar())
            return null;
        ArrayList<String> strings = new ArrayList<>();
        for (ResultColumn resultColumn : resultColumns) {
            strings.add(resultColumn.toString() +
                    (resultColumn.getTableName() != null ? "$" + resultColumn.getTableName() : ""));
        }
        return strings;
    }

    public List<AggregationFunction> getAgg() {
        List<AggregationFunction> aggregationFunctions= Main.symbolTable.getDeclaredAggregationFunction().
                stream().filter(p -> {
            if (getResultColumnsNames() == null) return false;
            return getResultColumnsNames().contains(p.getAggregationFunctionName());
        }).collect(Collectors.toList());
        for (AggregationFunction aggregationFunction : aggregationFunctions) {
            for (ResultColumn resultColumn : resultColumns) {
                if (resultColumn.toString().equals(aggregationFunction.getAggregationFunctionName()))
                    aggregationFunction.setAlias(resultColumn.getColumnAlias());
            }
        }
        return aggregationFunctions;
    }

    public List<ResultColumn> getFunctionExprs() {
        return getResultColumns().stream().filter(p -> p.getExpr() instanceof FunctionExpr).collect(Collectors.toList());
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (ResultColumn resultColumn : resultColumns) {
            resultColumn.accept(visitor);
        }
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public void setResultColumns(ArrayList<ResultColumn> resultColumns) {
        this.resultColumns = resultColumns;
    }

    @Override
    public String toString() {
        return "Selecting{" +
                "resultColumns=" + resultColumns +
                "} " + super.toString();
    }

    public ArrayList<Expr> getExprs() {
        ArrayList<Expr> exprs = new ArrayList<>();
        for (ResultColumn resultColumn : resultColumns) {
            exprs.add(resultColumn.getExpr());
        }
        return exprs;
    }
}
