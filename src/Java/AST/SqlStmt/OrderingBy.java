package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class OrderingBy extends Node {
    private ArrayList<OrderingTerm> orderingTerms;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (OrderingTerm orderingTerm : orderingTerms) {
            orderingTerm.accept(visitor);
        }
    }

    public void setOrderingTerms(ArrayList<OrderingTerm> orderingTerms) {
        this.orderingTerms = orderingTerms;
    }

    @Override
    public String toString() {
        return "OrderingBy{} " + super.toString();
    }


    public ArrayList<Expr> getExprs() {
        ArrayList<Expr> exprs = new ArrayList<>();
        for (OrderingTerm orderingTerm :
                orderingTerms) {
            exprs.add(orderingTerm.getExpr());
        }
        return exprs;
    }
}
