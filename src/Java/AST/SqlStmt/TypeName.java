package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class TypeName extends Node {
    private ArrayList<Float> floats;
    private ArrayList<String> strings;
    private String name;

    public void setStrings(ArrayList<String> strings) {
        this.strings = strings;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFloats(ArrayList<Float> floats) {
        this.floats = floats;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "TypeName{" +
                "strings=" + strings +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }

    public ArrayList<Float> getFloats() {
        return floats;
    }

    public ArrayList<String> getStrings() {
        return strings;
    }

    public String getName() {
        return name;
    }
}
