package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

import java.util.ArrayList;

public class ColumnDef extends Node {
    private String columnName;
    private ArrayList<TypeName> typeNames = new ArrayList<>();

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setTypeNames(ArrayList<TypeName> typeNames) {
        this.typeNames = typeNames;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        for (TypeName typeName : typeNames
        ) {
            typeName.accept(visitor);
        }
    }

    @Override
    public String toString() {
        return "ColumnDef{" +
                "columnName='" + columnName + '\'' +
                "} " + super.toString();
    }

    public String getColumnName() {
        return columnName;
    }

    public ArrayList<TypeName> getTypeNames() {
        return typeNames;
    }
}
