package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class Operation extends Node {
    private boolean any;
    private boolean all;
    private Oper oper;

    public boolean isAny() {
        return any;
    }

    public void setAny(boolean any) {
        this.any = any;
    }

    public boolean isAll() {
        return all;
    }

    public void setAll(boolean all) {
        this.all = all;
    }

    public Oper getOper() {
        return oper;
    }

    public void setOper(Oper oper) {
        this.oper = oper;
    }

    public enum Oper {GT, LT, GT_EQ, LT_EQ, NOT_EQ, NOT_IN, IN}

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
    }
}
