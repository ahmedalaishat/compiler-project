package Java.AST.SqlStmt;

import Java.AST.Node.ITable;
import Java.AST.Node.Node;
import Java.AST.SqlStmt.table.BasicTable;
import Java.AST.SqlStmt.table.JoinTable;
import Java.AST.SqlStmt.table.ListTable;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.type.IMergedTable;

import java.util.ArrayList;

public class From extends Node {
    private ITable table;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (table != null)
            table.accept(visitor);
    }

    @Override
    public String toString() {
        return "From{} " + super.toString();
    }


    public ArrayList<String> getTables() {
        ArrayList<String> tables = new ArrayList<>();
        ITable iTable;
        if (table instanceof JoinTable) {
            iTable = ((JoinTable) table).getJoinClause().getListTable();
        } else iTable = table;
        if (iTable instanceof ListTable)
            for (ITable tableOrSubQuerie : ((ListTable) iTable).getiTables()) {
                if (tableOrSubQuerie instanceof BasicTable) {
                    tables.add(((BasicTable) tableOrSubQuerie).getTableName());
                }
            }
        return tables;
    }


    public IMergedTable getIMergedTable() {
        return table.getType();
    }

    public IMergedTable getType() {
        return table.getType();
    }

    public ITable getTable() {
        return table;
    }

    public void setTable(ITable table) {
        this.table = table;
    }
}
