package Java.AST.SqlStmt;

import Java.AST.Node.ITable;
import Java.AST.SqlStmt.table.Table;
import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.type.TableType;

public class SubQuery extends Table implements ITable {
    private SelectStmt selectStmt;

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public TableType getType() {
        return selectStmt.getFlattedType();
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        selectStmt.accept(visitor);
    }

    @Override
    public String toString() {
        return "SubQuery{" +
                "selectStmt=" + selectStmt +
                "} " + super.toString();
    }
}
