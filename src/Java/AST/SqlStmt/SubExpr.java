package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class SubExpr extends Node {
    private String columnName;
    private Operation operation;
    private SelectStmt selectStmt;

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void setSelectStmt(SelectStmt selectStmt) {
        this.selectStmt = selectStmt;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (selectStmt != null)
            selectStmt.accept(visitor);
    }

    @Override
    public String toString() {
        return "SubExpr{" +
                "columnName='" + columnName + '\'' +
                ", operation='" + operation + '\'' +
                ", selectStmt=" + selectStmt +
                "} " + super.toString();
    }
}
