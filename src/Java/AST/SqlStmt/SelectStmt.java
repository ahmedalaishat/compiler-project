package Java.AST.SqlStmt;

import Java.AST.Visitor.ASTVisitor;
import Java.SymbolTable.AggregationFunction;
import Java.SymbolTable.Flat;
import Java.SymbolTable.type.TableType;

import java.util.ArrayList;

import static Java.Main.symbolTable;

public class SelectStmt extends Statement {
    enum SelectType {VALUES, SELECT}

    private Selecting selecting;
    private From from;
    private Where where;
    private GroupBy groupBy;
    private Values values;
    private OrderingBy orderingBy;
    private Limit limit;
    private TableType flatedType;

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        if (selecting != null)
            selecting.accept(visitor);
        if (from != null)
            from.accept(visitor);
        if (where != null)
            where.accept(visitor);
        if (groupBy != null)
            groupBy.accept(visitor);
        if (values != null)
            values.accept(visitor);
        if (orderingBy != null)
            orderingBy.accept(visitor);
        if (limit != null)
            limit.accept(visitor);
    }

    public void setSelecting(Selecting selecting) {
        this.selecting = selecting;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public void setWhere(Where where) {
        this.where = where;
    }

    public void setGroupBy(GroupBy groupBy) {
        this.groupBy = groupBy;
    }

    public void setValues(Values values) {
        this.values = values;
    }

    public void setOrderingBy(OrderingBy orderingBy) {
        this.orderingBy = orderingBy;
    }

    public void setLimit(Limit limit) {
        this.limit = limit;
    }


    public ArrayList<ColumnExpr> getColumnsUsed() {
        ArrayList<Expr> exprs = new ArrayList<>(selecting.getExprs());
        if (where != null)
            exprs.add(where.getExpr());
        if (groupBy != null)
            exprs.addAll(groupBy.getExprs());
        if (values != null)
            exprs.addAll(values.getExprs());
        if (orderingBy != null)
            exprs.addAll(orderingBy.getExprs());
        if (limit != null)
            exprs.addAll(limit.getExprs());
        return getColumnsExprs(exprs);
    }

    private ArrayList<ColumnExpr> getColumnsExprs(ArrayList<Expr> exprs) {
        ArrayList<ColumnExpr> columnExprs = new ArrayList<>();
        for (Expr expr :
                exprs) {
            if (expr instanceof ColumnExpr)
                columnExprs.add((ColumnExpr) expr);
            else if (expr instanceof InExpr) {
                ArrayList<Expr> exprs1 = new ArrayList<>(((InExpr) expr).getExprs());
                exprs1.add(((InExpr) expr).getLeft());
                columnExprs.addAll(getColumnsExprs(exprs1));
            } else if (expr instanceof TwoExpr) {
                ArrayList<Expr> exprs1 = new ArrayList<>();
                exprs1.add(((TwoExpr) expr).getlExpr());
                exprs1.add(((TwoExpr) expr).getrExpr());
                columnExprs.addAll(getColumnsExprs(exprs1));

            } else if (expr instanceof FunctionExpr) {
                columnExprs.addAll(getColumnsExprs(((FunctionExpr) expr).getExprs()));
            }
        }
        return columnExprs;
    }

    public String getTableName() {
        return from.getTables().get(0);
    }

    public TableType getFlattedType() {
        if (flatedType == null) {
            flatedType = Flat.getFlattedType(from.getType(), selecting.getResultColumnsNames());
            for (AggregationFunction a : selecting.getAgg()) {
                flatedType.getColumns().put(
                        a.getAlias()!=null ?a.getAlias():a.getAggregationFunctionName(),
                        a.getReturnType());
            }
        }
        return flatedType;
    }

    public ArrayList<String> check() {
        ArrayList<String> notExist = new ArrayList<>();
        for (ColumnExpr columnExpr : getColumnsUsed()) {
            boolean exists = false;
            for (String tableName : from.getTables()) {
                TableType tableType = symbolTable.getDeclaredType(tableName);
                if (tableType != null && tableType.isColumnTypeExist(columnExpr.toString()) != null)
                    exists = true;
            }
            if (!exists)
                notExist.add(columnExpr.toString());
        }
        return notExist;
    }
//    public boolean checkIn() {
//        if (where != null && where.getExpr().getiExpr() instanceof InExpr) {
//            ArrayList<Expr> exprs= ((InExpr) where.getExpr().getiExpr()).getExprs();
//            for (Expr expr:exprs) {
//                if (expr.getiExpr() instanceof ColumnExpr){
//
//                }
//            }
//            }
//    }


    @Override
    public String toString() {
        return "select " + selecting +
                " from=" + from +
                " where=" + where;
    }

    public Selecting getSelecting() {
        return selecting;
    }

    public From getFrom() {
        return from;
    }

    public Where getWhere() {
        return where;
    }

    public GroupBy getGroupBy() {
        return groupBy;
    }

    public Values getValues() {
        return values;
    }

    public OrderingBy getOrderingBy() {
        return orderingBy;
    }

    public Limit getLimit() {
        return limit;
    }
}
