package Java.AST.SqlStmt;

import Java.AST.Node.Node;
import Java.AST.Visitor.ASTVisitor;

public class Having extends Node {
    private Expr expr;

    public Having(Expr expr) {
        this.expr = expr;
    }

    @Override
    public void accept(ASTVisitor visitor) {
        visitor.visit(this);
        expr.accept(visitor);
    }

    public Expr getExpr() {
        return expr;
    }

    public void setExpr(Expr expr) {
        this.expr = expr;
    }

    public FunctionExpr getFunctionExpr() {
        if (expr instanceof TwoExpr) {
            if (((TwoExpr) expr).getlExpr() instanceof FunctionExpr)
                return (FunctionExpr) ((TwoExpr) expr).getlExpr();
            if (((TwoExpr) expr).getrExpr() instanceof FunctionExpr)
                return (FunctionExpr) ((TwoExpr) expr).getrExpr();
        }
        return null;
    }
}
