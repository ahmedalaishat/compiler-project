package Java.Base;

import Java.AST.Function.*;
import Java.AST.Function.JavaStmt.*;
import Java.AST.Node.ITable;
import Java.AST.SqlStmt.*;
import Java.AST.SqlStmt.ExprOperation.ExprOper;
import Java.AST.SqlStmt.table.BasicTable;
import Java.AST.SqlStmt.table.JoinTable;
import Java.AST.SqlStmt.table.ListTable;
import Java.AST.SqlStmt.table.Table;
import gen.sqlBaseVisitor;
import gen.sqlParser;

import java.util.ArrayList;
import java.util.List;
//import static Java.Main.id;
//import static Java.Main.symbolTable;

public class BaseVisitor extends sqlBaseVisitor {
//    private static Stack<Scope> scopesStack = new Stack<>();

    @Override
    public Parse visitParse(sqlParser.ParseContext ctx) {
        Parse p = new Parse();
        if (ctx.stmt_list() != null) {
            ArrayList<GlobalStatement> globalStatements = visitStmt_list(ctx.stmt_list());
            p.setGlobalStatements(globalStatements);
        }
        p.setLine(ctx.getStart().getLine());
        p.setCol(ctx.getStart().getCharPositionInLine());
        p.setLine(ctx.getStart().getLine());
        p.setCol(ctx.getStart().getCharPositionInLine());
        return p;
    }

    @Override
    public ArrayList<GlobalStatement> visitStmt_list(sqlParser.Stmt_listContext ctx) {
        ArrayList<GlobalStatement> globalStmt = new ArrayList<>();
        for (int i = 0; i < ctx.getChildCount(); i++) {
            if (ctx.getChild(i) instanceof sqlParser.FunctionContext) {
                globalStmt.add(visitFunction((sqlParser.FunctionContext) ctx.getChild(i)));
            }
            if (ctx.getChild(i) instanceof sqlParser.InstContext) {
                globalStmt.add(visitInst((sqlParser.InstContext) ctx.getChild(i)));
            }
            if (ctx.getChild(i) instanceof sqlParser.Sql_stmtContext) {
                globalStmt.add(visitSql_stmt((sqlParser.Sql_stmtContext) ctx.getChild(i)));
            }
        }
        return globalStmt;
    }

    @Override
    public Statement visitSql_stmt(sqlParser.Sql_stmtContext ctx) {
        if (ctx.select_stmt() != null)
            return visitSelect_stmt(ctx.select_stmt());
        if (ctx.create_table_stmt() != null)
            return visitCreate_table_stmt(ctx.create_table_stmt());
        if (ctx.create_type_stmt() != null)
            return visitCreate_type_stmt(ctx.create_type_stmt());
        if (ctx.create_agg_fun_stmt() != null)
            return visitCreate_agg_fun_stmt(ctx.create_agg_fun_stmt());
        return null;//unreachable
    }

    @Override
    public CreateTableStmt visitCreate_table_stmt(sqlParser.Create_table_stmtContext ctx) {
        //AHMED_MARWAN you have to iterate on columns list,
        // for each one create new symbol(name,type) => new column and insert it in previous type
        CreateTableStmt createTableStmt = new CreateTableStmt();
        createTableStmt.setTableName(visitTable_name(ctx.table_name()));
        String path = visitPath(ctx.path());
        path = path.substring(1, path.length() - 1);
        createTableStmt.setPath(path);
        String typeName = visitTable_type(ctx.table_type());
        typeName = typeName.substring(1, typeName.length() - 1);
        createTableStmt.setTypeName(typeName);
        ArrayList<ColumnDef> columnDefs = new ArrayList<>();
        for (int i = 0; i < ctx.column_def().size(); i++) {
//            String name =ctx.column_def(i).column_name().getText();
//            String type=  ctx.column_def(i).type_name(0).name().getText();
//            Symbol symbol=new Symbol();
//            if(type.equalsIgnoreCase("int"))
//            Type column=new Type();
//            column.setName();
            columnDefs.add(visitColumn_def(ctx.column_def(i)));
        }
        createTableStmt.setColumnDefs(columnDefs);
        createTableStmt.setLine(ctx.getStart().getLine());
        createTableStmt.setCol(ctx.getStart().getCharPositionInLine());
        return createTableStmt;
    }

    @Override
    public SelectStmt visitSelect_stmt(sqlParser.Select_stmtContext ctx) {
        SelectStmt select = new SelectStmt();
        if (ctx.selecting() != null)
            select.setSelecting(visitSelecting(ctx.selecting()));
        if (ctx.from() != null)
            select.setFrom(visitFrom(ctx.from()));
        if (ctx.where() != null)
            select.setWhere(visitWhere(ctx.where()));
        if (ctx.group_by() != null)
            select.setGroupBy(visitGroup_by(ctx.group_by()));
        if (ctx.values() != null)
            select.setValues(visitValues(ctx.values()));
        if (ctx.ordering_by() != null)
            select.setOrderingBy(visitOrdering_by(ctx.ordering_by()));
        if (ctx.limit() != null)
            select.setLimit(visitLimit(ctx.limit()));
        select.setLine(ctx.getStart().getLine());
        select.setCol(ctx.getStart().getCharPositionInLine());
        return select;
    }

    @Override
    public Limit visitLimit(sqlParser.LimitContext ctx) {
        Limit limit = new Limit();
        limit.setExpr(visitExpr(ctx.expr(0)));
        ArrayList<Expr> exprs = new ArrayList<>();
        for (int i = 1; i < ctx.expr().size(); i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        limit.setExprs(exprs);
        limit.setOffset(ctx.K_OFFSET() != null);
        limit.setLine(ctx.getStart().getLine());
        limit.setCol(ctx.getStart().getCharPositionInLine());
        return limit;
    }

    @Override
    public Expr visitJoin_constraint(sqlParser.Join_constraintContext ctx) {
        if (ctx.expr() != null)
            return visitExpr(ctx.expr());
        return null;
    }

    @Override
    public Having visitHaving(sqlParser.HavingContext ctx) {
        return new Having(visitExpr(ctx.expr()));
    }

    @Override
    public OrderingBy visitOrdering_by(sqlParser.Ordering_byContext ctx) {
        OrderingBy orderingBy = new OrderingBy();
        ArrayList<OrderingTerm> orderingTerms = new ArrayList<>();
        for (int i = 0; i < ctx.ordering_term().size(); i++) {
            orderingTerms.add(visitOrdering_term(ctx.ordering_term(i)));
        }
        orderingBy.setOrderingTerms(orderingTerms);
        orderingBy.setLine(ctx.getStart().getLine());
        orderingBy.setCol(ctx.getStart().getCharPositionInLine());
        return orderingBy;
    }

    @Override
    public Values visitValues(sqlParser.ValuesContext ctx) {
        Values values = new Values();
        ArrayList<Expr> exprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        values.setExprs(exprs);
        values.setLine(ctx.getStart().getLine());
        values.setCol(ctx.getStart().getCharPositionInLine());
        return values;
    }

    @Override
    public GroupBy visitGroup_by(sqlParser.Group_byContext ctx) {
        GroupBy groupBy = new GroupBy();
        ArrayList<Expr> exprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        if (ctx.having() != null)
            groupBy.setHaving(visitHaving(ctx.having()));
        groupBy.setExprs(exprs);
        groupBy.setLine(ctx.getStart().getLine());
        groupBy.setCol(ctx.getStart().getCharPositionInLine());
        return groupBy;
    }

    @Override
    public Where visitWhere(sqlParser.WhereContext ctx) {
        Where where = new Where();
        if (ctx.expr() != null)
            where.setExpr(visitExpr(ctx.expr()));
        if (ctx.sub_exp() != null)
            where.setSubExpr(visitSub_exp(ctx.sub_exp()));
        where.setLine(ctx.getStart().getLine());
        where.setCol(ctx.getStart().getCharPositionInLine());
        return where;
    }

    @Override
    public From visitFrom(sqlParser.FromContext ctx) {
        From from = new From();
        if (ctx.join_clause() != null) {
            JoinTable joinTable = new JoinTable();
            joinTable.setJoinClause(visitJoin_clause(ctx.join_clause()));
            from.setTable(joinTable);
        } else {
            ArrayList<ITable> iTables = visitTable_or_subquery_list(ctx.table_or_subquery_list());
            ListTable listTable = new ListTable();
            listTable.setiTables(iTables);
            from.setTable(listTable);
        }
        from.setLine(ctx.getStart().getLine());
        from.setCol(ctx.getStart().getCharPositionInLine());
        return from;
    }

    @Override
    public ArrayList<ITable> visitTable_or_subquery_list(sqlParser.Table_or_subquery_listContext ctx) {
        ArrayList<ITable> iTables = new ArrayList<>();
        for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
            iTables.add(visitTable_or_subquery(ctx.table_or_subquery(i)));
        }
        return iTables;
    }

    @Override
    public Selecting visitSelecting(sqlParser.SelectingContext ctx) {
        Selecting selecting = new Selecting();
        selecting.setDistinct(ctx.K_DISTINCT() != null);
        selecting.setAll(ctx.K_ALL() != null);
        ArrayList<ResultColumn> resultColumns = new ArrayList<>();
        for (int i = 0; i < ctx.result_column().size(); i++) {
            resultColumns.add(visitResult_column(ctx.result_column(i)));
        }
        selecting.setResultColumns(resultColumns);
        selecting.setLine(ctx.getStart().getLine());
        selecting.setCol(ctx.getStart().getCharPositionInLine());
        return selecting;
    }

    @Override
    public ColumnDef visitColumn_def(sqlParser.Column_defContext ctx) {
        ColumnDef columnDef = new ColumnDef();
        columnDef.setColumnName(visitColumn_name(ctx.column_name()));
        ArrayList<TypeName> typeNames = new ArrayList<>();
        for (int i = 0; i < ctx.type_name().size(); i++) {
            typeNames.add((visitType_name(ctx.type_name(i))));
        }
        columnDef.setTypeNames(typeNames);
        columnDef.setLine(ctx.getStart().getLine());
        columnDef.setCol(ctx.getStart().getCharPositionInLine());
        return columnDef;
    }

    @Override
    public TypeName visitType_name(sqlParser.Type_nameContext ctx) {
        TypeName typeName = new TypeName();
        ArrayList<Float> floats = new ArrayList<>();
        for (int i = 0; i < ctx.number().size(); i++) {
            floats.add(Float.valueOf(ctx.number(i).getText()));
        }
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < ctx.any_name().size(); i++) {
            strings.add(ctx.any_name(i).sql_identifirer().getText());
        }
        typeName.setName(visitName(ctx.name()));
        typeName.setFloats(floats);
        typeName.setStrings(strings);
        typeName.setLine(ctx.getStart().getLine());
        typeName.setCol(ctx.getStart().getCharPositionInLine());
        return typeName;
    }

    @Override
    public Expr visitExpr(sqlParser.ExprContext ctx) {
        Expr expr;
        if (ctx.literal_value() != null)
            expr = visitLiteral_value(ctx.literal_value());
        else if (ctx.column_expr() != null)
            expr = visitColumn_expr(ctx.column_expr());
        else if (ctx.unary_operator_expr() != null)
            expr = visitUnary_operator_expr(ctx.unary_operator_expr());
        else if (ctx.function_expr() != null)
            expr = visitFunction_expr(ctx.function_expr());
        else if (ctx.expr_operation() != null) {
            expr = new TwoExpr();
            ((TwoExpr) expr).setlExpr(visitExpr(ctx.expr(0)));
            ((TwoExpr) expr).setrExpr(visitExpr(ctx.expr(1)));
            ((TwoExpr) expr).setOperation(visitExpr_operation(ctx.expr_operation()));
        } else if (ctx.other_expr() != null)
            return visitOther_expr(ctx.other_expr());
        else if (ctx.K_IN() != null) {
            expr = new InExpr();
            ((InExpr) expr).setNot(ctx.K_NOT() != null);
            ((InExpr) expr).setLeft(visitExpr(ctx.expr(0)));
            if (ctx.table_name() != null)
                ((InExpr) expr).setTableName(visitTable_name(ctx.table_name()));
            else if (ctx.select_stmt() != null)
                ((InExpr) expr).setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
            else {
                ArrayList<Expr> exprs = new ArrayList<>();
                for (int i = 0; i < ctx.expr().size(); i++) {
                    Expr expr1 = visitExpr(ctx.expr(i));
//                    if (((Expr) expr1).getiExpr() instanceof ColumnExpr)
                    exprs.add(expr1);
                }
                ((InExpr) expr).setExprs(exprs);
            }
        } else {
            expr = new ExistExpr();
            ((ExistExpr) expr).setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
            if (ctx.K_EXISTS() != null && ctx.K_NOT() != null)
                ((ExistExpr) expr).setPrefix(ExistExpr.prefix.NotExist);
            else if (ctx.K_EXISTS() != null)
                ((ExistExpr) expr).setPrefix(ExistExpr.prefix.Exist);
        }
        expr.setLine(ctx.getStart().getLine());
        expr.setCol(ctx.getStart().getCharPositionInLine());
        return expr;
    }


    @Override
    public Expr visitOther_expr(sqlParser.Other_exprContext ctx) {
        return visitExpr(ctx.expr());
    }

    @Override
    public ExprOperation visitExpr_operation(sqlParser.Expr_operationContext ctx) {
        if (ctx.PIPE2() != null) return new ExprOperation(ExprOper.PIPE2);
        if (ctx.MULTIPLY() != null) return new ExprOperation(ExprOper.MULTIPLY);
        if (ctx.DIVISION() != null) return new ExprOperation(ExprOper.DIVISION);
        if (ctx.PERCENT() != null) return new ExprOperation(ExprOper.PERCENT);
        if (ctx.PLUS() != null) return new ExprOperation(ExprOper.PLUS);
        if (ctx.MINUS() != null) return new ExprOperation(ExprOper.MINUS);
        if (ctx.LTLT() != null) return new ExprOperation(ExprOper.LTLT);
        if (ctx.GTGT() != null) return new ExprOperation(ExprOper.GTGT);
        if (ctx.AMP() != null) return new ExprOperation(ExprOper.AMP);
        if (ctx.PIPE() != null) return new ExprOperation(ExprOper.PIPE);
        if (ctx.LT() != null) return new ExprOperation(ExprOper.LT);
        if (ctx.LT_EQ() != null) return new ExprOperation(ExprOper.LT_EQ);
        if (ctx.GT() != null) return new ExprOperation(ExprOper.GT);
        if (ctx.GT_EQ() != null) return new ExprOperation(ExprOper.GT_EQ);
        if (ctx.K_IS() != null) return new ExprOperation(ExprOper.K_IS);
        if (ctx.K_LIKE() != null && ctx.K_NOT() != null) return new ExprOperation(ExprOper.K_LIKE);
        if (ctx.K_LIKE() != null) return new ExprOperation(ExprOper.K_LIKE);
        if (ctx.K_NOT() != null) return new ExprOperation(ExprOper.K_NOT);
        if (ctx.K_GLOB() != null) return new ExprOperation(ExprOper.K_GLOB);
        if (ctx.K_MATCH() != null) return new ExprOperation(ExprOper.K_MATCH);
        if (ctx.K_REGEXP() != null) return new ExprOperation(ExprOper.K_REGEXP);
        if (ctx.K_AND() != null) return new ExprOperation(ExprOper.K_AND);
        if (ctx.K_OR() != null) return new ExprOperation(ExprOper.K_OR);
        if (ctx.SEQ() != null) return new ExprOperation(ExprOper.SEQ);
        if (ctx.EQ() != null) return new ExprOperation(ExprOper.EQ);
        if (ctx.NOT_EQ1() != null) return new ExprOperation(ExprOper.NOT_EQ1);
        if (ctx.NOT_EQ2() != null) return new ExprOperation(ExprOper.NOT_EQ2);
        return null;
    }

    @Override
    public ColumnExpr visitColumn_expr(sqlParser.Column_exprContext ctx) {
        ColumnExpr columnExpr = new ColumnExpr();
        columnExpr.setColumnName(visitColumn_name(ctx.column_name()));
        if (ctx.table_name() != null&&ctx.table_name().size()>0) {
            ArrayList<String> strings=new ArrayList<>();
            for (int i = 0; i < ctx.table_name().size(); i++) {
                strings.add(visitTable_name(ctx.table_name(i)));
                columnExpr.setTableNames(strings);
            }
        }
        columnExpr.setLine(ctx.getStart().getLine());
        columnExpr.setCol(ctx.getStart().getCharPositionInLine());
        return columnExpr;
    }

    @Override
    public UnaryOperationExpr visitUnary_operator_expr(sqlParser.Unary_operator_exprContext ctx) {
        UnaryOperationExpr unaryOperationExpr = new UnaryOperationExpr();
        unaryOperationExpr.setExpr(visitExpr(ctx.expr()));
        unaryOperationExpr.setUnaryOperator(ctx.unary_operator().getText());
        unaryOperationExpr.setLine(ctx.getStart().getLine());
        unaryOperationExpr.setCol(ctx.getStart().getCharPositionInLine());
        return unaryOperationExpr;
    }

    @Override
    public FunctionExpr visitFunction_expr(sqlParser.Function_exprContext ctx) {
        FunctionExpr functionExpr = new FunctionExpr();
        functionExpr.setFunctionName(visitFunction_name(ctx.function_name()));
        functionExpr.setDisc(ctx.K_DISTINCT() != null);
        ArrayList<Expr> exprs = new ArrayList<>();
        for (int i = 0; i < ctx.expr().size(); i++) {
            exprs.add(visitExpr(ctx.expr(i)));
        }
        functionExpr.setExprs(exprs);
        functionExpr.setStar(ctx.getText().contains("*"));
        functionExpr.setLine(ctx.getStart().getLine());
        functionExpr.setCol(ctx.getStart().getCharPositionInLine());
        return functionExpr;
    }

    @Override
    public OrderingTerm visitOrdering_term(sqlParser.Ordering_termContext ctx) {
        OrderingTerm orderingTerm = new OrderingTerm();
        orderingTerm.setExpr(visitExpr(ctx.expr()));
        if (ctx.collation_name() != null)
            orderingTerm.setCollectionName(visitCollation_name(ctx.collation_name()));
        orderingTerm.setDesc(ctx.K_DESC() != null);
        orderingTerm.setLine(ctx.getStart().getLine());
        orderingTerm.setCol(ctx.getStart().getCharPositionInLine());
        return orderingTerm;
    }

    @Override
    public ResultColumn visitResult_column(sqlParser.Result_columnContext ctx) {
        ResultColumn resultColumn = new ResultColumn();
        if (ctx.table_name() != null)
            resultColumn.setTableName(visitTable_name(ctx.table_name()));
        if (ctx.expr() != null) {
            resultColumn.setExpr(visitExpr(ctx.expr()));
        }
        resultColumn.setStar(ctx.getText().equals("*"));
        if (ctx.column_alias() != null)
            resultColumn.setColumnAlias(visitColumn_alias(ctx.column_alias()));
        resultColumn.setLine(ctx.getStart().getLine());
        resultColumn.setCol(ctx.getStart().getCharPositionInLine());
        return resultColumn;
    }

    @Override
    public ITable visitTable_or_subquery(sqlParser.Table_or_subqueryContext ctx) {
        if (ctx.table() != null)
            return visitTable(ctx.table());
        if (ctx.sub_query() != null)
            return visitSub_query(ctx.sub_query());
        return null;//unreachable
    }

    @Override
    public Table visitTable(sqlParser.TableContext ctx) {
        if (ctx.basic_table() != null)
            return visitBasic_table(ctx.basic_table());
        if (ctx.table_or_subquery_list() != null) {
            ArrayList<ITable> iTables = visitTable_or_subquery_list(ctx.table_or_subquery_list());
            ListTable listTable = new ListTable();
            listTable.setiTables(iTables);
            if (ctx.table_alias() != null)
                listTable.setTableAlias(visitTable_alias(ctx.table_alias()));
            listTable.setLine(ctx.getStart().getLine());
            listTable.setCol(ctx.getStart().getCharPositionInLine());
            return listTable;
        }
        if (ctx.join_clause() != null) {
            JoinTable joinTable = new JoinTable();
            joinTable.setJoinClause(visitJoin_clause(ctx.join_clause()));
            if (ctx.table_alias() != null)
                joinTable.setTableAlias(visitTable_alias(ctx.table_alias()));
            joinTable.setLine(ctx.getStart().getLine());
            joinTable.setCol(ctx.getStart().getCharPositionInLine());
            return joinTable;
        }
        return null;//unreachable
    }

    @Override
    public BasicTable visitBasic_table(sqlParser.Basic_tableContext ctx) {
        BasicTable basicTable = new BasicTable();
        if (ctx.table_name() != null)
            basicTable.setTableName(visitTable_name(ctx.table_name()));
        if (ctx.table_alias() != null)
            basicTable.setTableAlias(visitTable_alias(ctx.table_alias()));
        if (ctx.index_name() != null)
            basicTable.setIndexName(visitIndex_name(ctx.index_name()));
        return basicTable;
    }

    @Override
    public SubQuery visitSub_query(sqlParser.Sub_queryContext ctx) {
        SubQuery subQuery = new SubQuery();
        subQuery.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        if (ctx.table_alias() != null)
            subQuery.setTableAlias(visitTable_alias(ctx.table_alias()));
        subQuery.setLine(ctx.getStart().getLine());
        subQuery.setCol(ctx.getStart().getCharPositionInLine());
        return subQuery;
    }

    @Override
    public JoinClause visitJoin_clause(sqlParser.Join_clauseContext ctx) {
        JoinClause joinClause = new JoinClause();
        ArrayList<ITable> iTables = new ArrayList<>();
        for (int i = 0; i < ctx.table_or_subquery().size(); i++) {
            iTables.add(visitTable_or_subquery(ctx.table_or_subquery(i)));
        }
        ListTable listTable = new ListTable();
        listTable.setiTables(iTables);
        joinClause.setListTable(listTable);
        ArrayList<JoinClause.JoinOperator> joinOperators = new ArrayList<>();
        for (int i = 0; i < ctx.join_operator().size(); i++)
            joinOperators.add(visitJoin_operator(ctx.join_operator(i)));
        ArrayList<Expr> joinConstraint = new ArrayList<>();
        for (int i = 0; i < ctx.join_constraint().size(); i++) {
            Expr expr = visitJoin_constraint(ctx.join_constraint(i));
            if (expr != null)
                joinConstraint.add(expr);
        }
        joinClause.setJoinOperators(joinOperators);
        joinClause.setJoinConstraints(joinConstraint);
        joinClause.setLine(ctx.getStart().getLine());
        joinClause.setCol(ctx.getStart().getCharPositionInLine());
        return joinClause;
    }

    @Override
    public JoinClause.JoinOperator visitJoin_operator(sqlParser.Join_operatorContext ctx) {
        if (ctx.K_LEFT() != null) return JoinClause.JoinOperator.LEFT_JOIN;
        if (ctx.K_INNER() != null || ctx.K_JOIN() != null) return JoinClause.JoinOperator.INNER_JOIN;
        return JoinClause.JoinOperator.CROSS;
    }

    @Override
    public LiteralValue visitLiteral_value(sqlParser.Literal_valueContext ctx) {
        LiteralValue literalValue = new LiteralValue();
        literalValue.setLine(ctx.getStart().getLine());
        literalValue.setCol(ctx.getStart().getCharPositionInLine());
        if (ctx.NUMERIC_LITERAL() != null)
            literalValue.setValue(Float.valueOf(ctx.NUMERIC_LITERAL().getText()));
        if (ctx.number() != null)
            literalValue.setValue(Double.valueOf(ctx.number().getText()));
        if (ctx.STRING_LITERAL() != null)
            literalValue.setValue(ctx.STRING_LITERAL().getText());
        if (ctx.J_STRING() != null)
            literalValue.setValue(ctx.J_STRING().getText());
        if (ctx.BLOB_LITERAL() != null)
            literalValue.setValue(ctx.BLOB_LITERAL().getText());
        if (ctx.K_NULL() != null)
            literalValue.setValue(ctx.K_NULL().getText());
        if (ctx.K_CURRENT_DATE() != null)
            literalValue.setValue(ctx.K_CURRENT_DATE().getText());
        if (ctx.K_CURRENT_TIME() != null)
            literalValue.setValue(ctx.K_CURRENT_TIME().getText());
        if (ctx.K_CURRENT_TIMESTAMP() != null)
            literalValue.setValue(ctx.K_CURRENT_TIMESTAMP().getText());
        return literalValue;
    }

    @Override
    public String visitUnary_operator(sqlParser.Unary_operatorContext ctx) {
        if (ctx.PLUS() != null)
            return ctx.PLUS().getText();
        else
            return ctx.getText();
    }

    @Override
    public String visitColumn_alias(sqlParser.Column_aliasContext ctx) {
        if (ctx.sql_identifirer() != null)
            return visitSql_identifirer(ctx.sql_identifirer());
        if (ctx.STRING_LITERAL() != null)
            return ctx.STRING_LITERAL().getText();
        return null;//////////// unreachable
    }

    @Override
    public String visitFunction_name(sqlParser.Function_nameContext ctx) {
        return ctx.J_IDENTIFIER().getSymbol().getText();
    }

    @Override
    public String visitName(sqlParser.NameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitDatabase_name(sqlParser.Database_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitTable_name(sqlParser.Table_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitJar_path(sqlParser.Jar_pathContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitClass_name(sqlParser.Class_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitMethod_name(sqlParser.Method_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitReturn_type(sqlParser.Return_typeContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitPath(sqlParser.PathContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitTable_type(sqlParser.Table_typeContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitColumn_name(sqlParser.Column_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitCollation_name(sqlParser.Collation_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitIndex_name(sqlParser.Index_nameContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitTable_alias(sqlParser.Table_aliasContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public String visitAny_name(sqlParser.Any_nameContext ctx) {
        if (ctx.sql_identifirer() != null)
            return visitSql_identifirer(ctx.sql_identifirer());
        if (ctx.any_name() != null)
            return visitAny_name(ctx.any_name());
        if (ctx.STRING_LITERAL() != null)
            return ctx.STRING_LITERAL().getText();
        return null;//////////// unreachable
    }

    @Override
    public String visitSql_identifirer(sqlParser.Sql_identifirerContext ctx) {
        if (ctx.J_STRING() != null)
            return ctx.J_STRING().getText();
        if (ctx.J_IDENTIFIER() != null)
            return ctx.J_IDENTIFIER().getText();
        if (ctx.STRING_LITERAL2() != null)
            return ctx.STRING_LITERAL2().getText();
        return null;//////////// unreachable
    }

    @Override
    public SubExpr visitSub_exp(sqlParser.Sub_expContext ctx) {
        SubExpr subExpr = new SubExpr();
        subExpr.setColumnName(visitColumn_name(ctx.column_name()));
        if (ctx.operation() != null)
            subExpr.setOperation(visitOperation(ctx.operation()));
        if (ctx.select_stmt() != null)
            subExpr.setSelectStmt(visitSelect_stmt(ctx.select_stmt()));
        subExpr.setLine(ctx.getStart().getLine());
        subExpr.setCol(ctx.getStart().getCharPositionInLine());
        return subExpr;
    }

    @Override
    public Operation visitOperation(sqlParser.OperationContext ctx) {
        Operation operation = new Operation();
        if (ctx.GT() != null)
            operation.setOper(Operation.Oper.GT);
        if (ctx.LT() != null)
            operation.setOper(Operation.Oper.LT);
        if (ctx.GT_EQ() != null)
            operation.setOper(Operation.Oper.GT_EQ);
        if (ctx.LT_EQ() != null)
            operation.setOper(Operation.Oper.LT);
        if (ctx.NOT_EQ1() != null)
            operation.setOper(Operation.Oper.NOT_EQ);
        if (ctx.NOT_EQ2() != null)
            operation.setOper(Operation.Oper.NOT_EQ);
        operation.setAll(ctx.K_ALL() != null);
        operation.setAll(ctx.K_ANY() != null);
        return operation;
    }


    @Override
    public BooleanValue visitBoolean_value(sqlParser.Boolean_valueContext ctx) {
        BooleanValue booleanValue = new BooleanValue();
        if (ctx.boolean_value() != null && ctx.boolean_value().size() == 1) {
            return visitBoolean_value(ctx.boolean_value(0));
        } else if (ctx.boolean_value() != null && ctx.boolean_value().size() == 2) {
            booleanValue.setType(BooleanValue.Type.COMPARE_BOOL);
            VariableValue variableValue = new VariableValue();
            variableValue.setType(VariableValue.VariableType.BOOLEAN);
            variableValue.setValue(visitBoolean_value(ctx.boolean_value(0)));
            booleanValue.setLeft(variableValue);
            variableValue = new VariableValue();
            variableValue.setType(VariableValue.VariableType.BOOLEAN);
            variableValue.setValue(visitBoolean_value(ctx.boolean_value(1)));
            booleanValue.setRight(variableValue);
            booleanValue.setOperation(ctx.getChild(1).getText());
        } else if (ctx.JK_FALSE() != null) {
            booleanValue.setType(BooleanValue.Type.FALSE);
            booleanValue.setCon(ctx.JK_FALSE().getSymbol().getText());
        } else if (ctx.JK_TRUE() != null) {
            booleanValue.setType(BooleanValue.Type.TRUE);
            booleanValue.setCon(ctx.JK_TRUE().getSymbol().getText());
        } else if (ctx.J_IDENTIFIER() != null) {
            booleanValue.setType(BooleanValue.Type.IDENTIFIER);
            booleanValue.setCon(ctx.J_IDENTIFIER().getText());
        } else if (ctx.getText().charAt(0) == '!') {
            booleanValue.setType(BooleanValue.Type.NOT);
            booleanValue.setCon(visitBoolean_value(ctx.boolean_value(0)));
        } else if (ctx.number_value() != null && ctx.number_value().size() > 0) {
            booleanValue.setType(BooleanValue.Type.COMPARE_NUM);
            booleanValue.setLeft(visitNumber_value(ctx.number_value(0)));
            booleanValue.setRight(visitNumber_value(ctx.number_value(1)));
        } else if (ctx.string_value() != null && ctx.string_value().size() > 0) {
            booleanValue.setType(BooleanValue.Type.COMPARE_STR);
            booleanValue.setLeft(visitString_value(ctx.string_value(0)));
            booleanValue.setRight(visitString_value(ctx.string_value(1)));
        }
        booleanValue.setLine(ctx.getStart().getLine());
        booleanValue.setCol(ctx.getStart().getCharPositionInLine());
        return booleanValue;
    }

    @Override
    public StringValue visitString_value(sqlParser.String_valueContext ctx) {
        StringValue stringValue = new StringValue();
        if (ctx.J_IDENTIFIER() != null) {
            stringValue.setType(StringValue.Type.IDENTIFIER);
            stringValue.setValue(ctx.J_IDENTIFIER().getText());
        }
        if (ctx.J_CHAR() != null) {
            stringValue.setType(StringValue.Type.CHAR);
            stringValue.setValue(ctx.J_CHAR().getText());
        } else {
            stringValue.setType(StringValue.Type.STRING);
            stringValue.setValue(ctx.J_STRING().getText());
        }
        return stringValue;
    }

    @Override
    public VariableValue visitVariable_value(sqlParser.Variable_valueContext ctx) {
        VariableValue variableValue = new VariableValue();
        if (ctx.JK_NULL() != null) {
            variableValue.setType(VariableValue.VariableType.NULL);
            variableValue.setValue(ctx.JK_NULL().getSymbol().getText());
        } else if (ctx.J_IDENTIFIER() != null) {
            variableValue.setType(VariableValue.VariableType.IDENTIFIER);
            variableValue.setValue(ctx.J_IDENTIFIER().getSymbol().getText());
        } else if (ctx.boolean_value() != null) {
            variableValue.setType(VariableValue.VariableType.BOOLEAN);
            variableValue.setValue(visitBoolean_value(ctx.boolean_value()));
        } else if (ctx.string_value() != null) {
            variableValue.setType(VariableValue.VariableType.STRING);
            variableValue.setValue(visitString_value(ctx.string_value()));
        } else if (ctx.number_value() != null) {
            variableValue.setType(VariableValue.VariableType.NUMBER);
            variableValue.setValue(visitNumber_value(ctx.number_value()));
        } else if (ctx.increment() != null) {
            variableValue.setType(VariableValue.VariableType.INCREMENT);
            variableValue.setValue(visitIncrement(ctx.increment()));
        } else if (ctx.decrement() != null) {
            variableValue.setType(VariableValue.VariableType.DECREMENT);
            variableValue.setValue(visitDecrement(ctx.decrement()));
        } else if (ctx.call_function() != null) {
            variableValue.setType(VariableValue.VariableType.CALL_FUN);
            variableValue.setValue(visitCall_function(ctx.call_function()));
        } else if (ctx.select_stmt() != null) {
            variableValue.setType(VariableValue.VariableType.SELECT);
            variableValue.setValue(visitSelect_stmt(ctx.select_stmt()));
        } else if (ctx.enhanced_if_stmt() != null) {
            variableValue.setType(VariableValue.VariableType.ENHANCED_IF);
            variableValue.setValue(visitEnhanced_if_stmt(ctx.enhanced_if_stmt()));
        }
        variableValue.setLine(ctx.getStart().getLine());
        variableValue.setCol(ctx.getStart().getCharPositionInLine());
        return variableValue;
    }

    @Override
    public ArrayList<VariableValue> visitValues_list(sqlParser.Values_listContext ctx) {
        return get(ctx.variable_value());
    }

    @Override
    public ArrayList<VariableValue> visitList_args(sqlParser.List_argsContext ctx) {
        return get(ctx.variable_value());
    }

    private ArrayList<VariableValue> get(List<sqlParser.Variable_valueContext> variable_value) {
        ArrayList<VariableValue> variableValues = new ArrayList<>();
        for (int i = 0; i < variable_value.size(); i++) {
            variableValues.add(visitVariable_value(variable_value.get(i)));
        }
        return variableValues;
    }

    @Override
    public Function visitFunction(sqlParser.FunctionContext ctx) {
        Function function = new Function();
        if (ctx.function_name() != null)
            function.setFunctionName(visitFunction_name(ctx.function_name()));
        if (ctx.function_header() != null)
            function.setFunctionHeader(visitFunction_header(ctx.function_header()));
        if (ctx.function_body() != null)
            function.setFunctionBody(visitFunction_body(ctx.function_body()));
        function.setLine(ctx.getStart().getLine());
        function.setCol(ctx.getStart().getCharPositionInLine());
        return function;
    }

    @Override
    public FunctionHeader visitFunction_header(sqlParser.Function_headerContext ctx) {
        FunctionHeader functionHeader = new FunctionHeader();
        if (ctx.list_params() != null) {
            ArrayList<Param> params = visitList_params(ctx.list_params());
            functionHeader.setParams(params);
        }
        functionHeader.setLine(ctx.getStart().getLine());
        functionHeader.setCol(ctx.getStart().getCharPositionInLine());
        return functionHeader;
    }

    @Override
    public ArrayList<Param> visitList_params(sqlParser.List_paramsContext ctx) {
        ArrayList<Param> params = new ArrayList<>();
        for (int i = 0; i < ctx.param().size(); i++) {
            params.add(visitParam(ctx.param(i)));
        }
        return params;
    }

    @Override
    public Param visitParam(sqlParser.ParamContext ctx) {
        Param param = new Param();
        param.setIdentifier(ctx.J_IDENTIFIER().getSymbol().getText());
        param.setLine(ctx.getStart().getLine());
        param.setCol(ctx.getStart().getCharPositionInLine());
        return param;
    }

    @Override
    public FunctionBody visitFunction_body(sqlParser.Function_bodyContext ctx) {
        FunctionBody functionBody = new FunctionBody();
        functionBody.setInstructions(visitList_inst(ctx.list_inst()));
        functionBody.setLine(ctx.getStart().getLine());
        functionBody.setCol(ctx.getStart().getCharPositionInLine());
        return functionBody;
    }

    @Override
    public ArrayList<Instruction> visitList_inst(sqlParser.List_instContext ctx) {
        ArrayList<Instruction> instructions = new ArrayList<>();
        for (int i = 0; i < ctx.inst().size(); i++) {
            instructions.add(visitInst(ctx.inst(i)));
        }
        return instructions;
    }

    @Override
    public Instruction visitInst(sqlParser.InstContext ctx) {
        if (ctx.break_inst() != null)
            return visitBreak_inst(ctx.break_inst());
        if (ctx.return_inst() != null)
            return visitReturn_inst(ctx.return_inst());
        if (ctx.call_function() != null)
            return visitCall_function((ctx.call_function()));
        if (ctx.var_assign() != null)
            return visitVar_assign(ctx.var_assign());
        if (ctx.var_declaration() != null)
            return visitVar_declaration(ctx.var_declaration());
        if (ctx.java_statement() != null)
            return visitJava_statement((ctx.java_statement()));
        if (ctx.increment() != null)
            return visitIncrement((ctx.increment()));
        if (ctx.decrement() != null)
            return visitDecrement((ctx.decrement()));
        return null;
    }

    @Override
    public ReturnIns visitReturn_inst(sqlParser.Return_instContext ctx) {
        ReturnIns returnIns = new ReturnIns();
        if (ctx.variable_value() != null)
            returnIns.setReturnsObject(visitVariable_value(ctx.variable_value()));
        returnIns.setLine(ctx.getStart().getLine());
        returnIns.setCol(ctx.getStart().getCharPositionInLine());
        return returnIns;
    }

    @Override
    public BreakIns visitBreak_inst(sqlParser.Break_instContext ctx) {
        BreakIns breakIns = new BreakIns();
        breakIns.setLine(ctx.getStart().getLine());
        breakIns.setCol(ctx.getStart().getCharPositionInLine());
        return breakIns;
    }

    @Override
    public EnhancedIfStmt visitEnhanced_if_stmt(sqlParser.Enhanced_if_stmtContext ctx) {
        EnhancedIfStmt enhancedIfStmt = new EnhancedIfStmt();
        if (ctx.children.size() == 3 && ctx.enhanced_if_stmt().enhanced_if_stmt() != null)
            return visitEnhanced_if_stmt(ctx.enhanced_if_stmt());
        enhancedIfStmt.setCon(visitBoolean_value(ctx.boolean_value()));
        enhancedIfStmt.setT(visitVariable_value(ctx.variable_value(0)));
        enhancedIfStmt.setF(visitVariable_value(ctx.variable_value(1)));
        enhancedIfStmt.setLine(ctx.getStart().getLine());
        enhancedIfStmt.setCol(ctx.getStart().getCharPositionInLine());
        return enhancedIfStmt;
    }

    @Override
    public Array visitArray(sqlParser.ArrayContext ctx) {
        Array array = new Array();
        if (ctx.values_list() != null) {
            array.setVariableValues(visitValues_list(ctx.values_list()));
        }
        array.setLine(ctx.getStart().getLine());
        array.setCol(ctx.getStart().getCharPositionInLine());
        return array;
    }

    @Override
    public CallFunctionIns visitCall_function(sqlParser.Call_functionContext ctx) {
        CallFunctionIns callFunctionIns = new CallFunctionIns();
        callFunctionIns.setFunctionName(ctx.function_name().getText());
        if (ctx.list_args() != null)
            callFunctionIns.setArguments(visitList_args(ctx.list_args()));
        callFunctionIns.setLine(ctx.getStart().getLine());
        callFunctionIns.setCol(ctx.getStart().getCharPositionInLine());
        return callFunctionIns;
    }

    @Override
    public JavaStmt visitJava_statement(sqlParser.Java_statementContext ctx) {
        if (ctx.do_while_stmt() != null)
            return visitDo_while_stmt(ctx.do_while_stmt());
        if (ctx.while_stmt() != null)
            return visitWhile_stmt(ctx.while_stmt());
        if (ctx.for_stmt() != null)
            return visitFor_stmt(ctx.for_stmt());
        if (ctx.foreach_stmt() != null)
            return visitForeach_stmt(ctx.foreach_stmt());
        if (ctx.if_stmt() != null)
            return visitIf_stmt(ctx.if_stmt());
        if (ctx.print_stmt() != null)
            return visitPrint_stmt(ctx.print_stmt());
        if (ctx.switch_stmt() != null)
            return visitSwitch_stmt(ctx.switch_stmt());
        return null;
    }

    @Override
    public PrintStmt visitPrint_stmt(sqlParser.Print_stmtContext ctx) {
        PrintStmt printStmt = new PrintStmt();
        printStmt.setVal(visitVariable_value(ctx.variable_value()));
        printStmt.setLine(ctx.getStart().getLine());
        printStmt.setCol(ctx.getStart().getCharPositionInLine());
        return printStmt;
    }

    @Override
    public IfStmt visitIf_stmt(sqlParser.If_stmtContext ctx) {
        IfStmt ifStmt = new IfStmt();
        ifStmt.setBooleanValue(visitBoolean_value(ctx.boolean_value()));
        ifStmt.setInstructions(visitList_inst(ctx.list_inst()));
        if (ctx.else_stmt() != null)
            ifStmt.setElseStmt(visitElse_stmt(ctx.else_stmt()));
        ifStmt.setLine(ctx.getStart().getLine());
        ifStmt.setCol(ctx.getStart().getCharPositionInLine());
        return ifStmt;
    }

    @Override
    public ElseStmt visitElse_stmt(sqlParser.Else_stmtContext ctx) {
        ElseStmt elseStmt = new ElseStmt();
        elseStmt.setInstructions(visitList_inst(ctx.list_inst()));
        elseStmt.setLine(ctx.getStart().getLine());
        elseStmt.setCol(ctx.getStart().getCharPositionInLine());
        return elseStmt;
    }

    @Override
    public DoWhileStmt visitDo_while_stmt(sqlParser.Do_while_stmtContext ctx) {
        DoWhileStmt doWhileStmt = new DoWhileStmt();
        doWhileStmt.setBooleanValue(visitBoolean_value(ctx.boolean_value()));
        doWhileStmt.setInstructions(visitList_inst(ctx.list_inst()));
        doWhileStmt.setLine(ctx.getStart().getLine());
        doWhileStmt.setCol(ctx.getStart().getCharPositionInLine());
        return doWhileStmt;
    }

    @Override
    public WhileStmt visitWhile_stmt(sqlParser.While_stmtContext ctx) {
        WhileStmt whileStmt = new WhileStmt();
        whileStmt.setBooleanValue(visitBoolean_value(ctx.boolean_value()));
        whileStmt.setInstructions(visitList_inst(ctx.list_inst()));
        whileStmt.setLine(ctx.getStart().getLine());
        whileStmt.setCol(ctx.getStart().getCharPositionInLine());
        return whileStmt;
    }

    @Override
    public SwitchStmt visitSwitch_stmt(sqlParser.Switch_stmtContext ctx) {
        SwitchStmt switchStmt = new SwitchStmt();
        switchStmt.setIdentifierName(ctx.J_IDENTIFIER().getText());
        switchStmt.setCasees(visitList_cases(ctx.list_cases()));
        switchStmt.setLine(ctx.getStart().getLine());
        switchStmt.setCol(ctx.getStart().getCharPositionInLine());
        return switchStmt;
    }

    @Override
    public ArrayList<Casee> visitList_cases(sqlParser.List_casesContext ctx) {
        ArrayList<Casee> casees = new ArrayList<>();
        for (int i = 0; i < ctx.case_block().size(); i++) {
            casees.add(visitCase_block(ctx.case_block(i)));
        }
        casees.add(visitDefault_block(ctx.default_block()));
        return casees;
    }

    @Override
    public CaseBlock visitCase_block(sqlParser.Case_blockContext ctx) {
        CaseBlock caseBlock = new CaseBlock();
        if (ctx.J_CHAR() != null)
            caseBlock.setValue(ctx.J_CHAR().getText().charAt(0));
        if (ctx.J_STRING() != null)
            caseBlock.setValue(ctx.J_STRING());
        if (ctx.number() != null)
            caseBlock.setValue(Integer.valueOf(ctx.number().getText()));
        caseBlock.setCaseBody(visitCase_body(ctx.case_body()));
        caseBlock.setLine(ctx.getStart().getLine());
        caseBlock.setCol(ctx.getStart().getCharPositionInLine());
        return caseBlock;
    }

    @Override
    public DefaultBlock visitDefault_block(sqlParser.Default_blockContext ctx) {
        DefaultBlock defaultBlock = new DefaultBlock();
        defaultBlock.setCaseBody(visitCase_body(ctx.case_body()));
        defaultBlock.setLine(ctx.getStart().getLine());
        defaultBlock.setCol(ctx.getStart().getCharPositionInLine());
        return defaultBlock;
    }

    @Override
    public CaseBody visitCase_body(sqlParser.Case_bodyContext ctx) {
        CaseBody caseBody = new CaseBody();
        caseBody.setInstructions(visitList_inst(ctx.list_inst()));
        if (ctx.break_inst() != null)
            caseBody.setBreakIns(visitBreak_inst(ctx.break_inst()));
        caseBody.setLine(ctx.getStart().getLine());
        caseBody.setCol(ctx.getStart().getCharPositionInLine());
        return caseBody;
    }

    @Override
    public ForEachStmt visitForeach_stmt(sqlParser.Foreach_stmtContext ctx) {
        ForEachStmt forEachStmt = new ForEachStmt();
        forEachStmt.setVarName(ctx.J_IDENTIFIER(0).getText());
        forEachStmt.setArrayName(ctx.J_IDENTIFIER(1).getText());
        forEachStmt.setInstructions(visitList_inst(ctx.list_inst()));
        forEachStmt.setLine(ctx.getStart().getLine());
        forEachStmt.setCol(ctx.getStart().getCharPositionInLine());
        return forEachStmt;
    }

    @Override
    public ForStmt visitFor_stmt(sqlParser.For_stmtContext ctx) {
        ForStmt forStmt = new ForStmt();
        forStmt.setForUpdate(visitFor_update(ctx.for_update()));
        forStmt.setForInitialization(visitFor_initialization(ctx.for_initialization()));
        forStmt.setInstructions(visitList_inst(ctx.list_inst()));
        forStmt.setBooleanValue(visitBoolean_value(ctx.boolean_value()));
        forStmt.setLine(ctx.getStart().getLine());
        forStmt.setCol(ctx.getStart().getCharPositionInLine());
        return forStmt;
    }

    @Override
    public ForInitialization visitFor_initialization(sqlParser.For_initializationContext ctx) {
        ForInitialization forInitialization = new ForInitialization();
        if (ctx.var_assign() != null)
            forInitialization.setVarAssign(visitVar_assign(ctx.var_assign()));
        if (ctx.var_declaration() != null)
            forInitialization.setVarDeclaration(visitVar_declaration(ctx.var_declaration()));
        forInitialization.setLine(ctx.getStart().getLine());
        forInitialization.setCol(ctx.getStart().getCharPositionInLine());
        return forInitialization;
    }

    @Override
    public ForUpdate visitFor_update(sqlParser.For_updateContext ctx) {
        ForUpdate forUpdate = new ForUpdate();
        if (ctx.J_IDENTIFIER() != null) {
            ArrayList<String> identifiers = new ArrayList<>();
            for (int i = 0; i < ctx.J_IDENTIFIER().size(); i++) {
                identifiers.add(ctx.J_IDENTIFIER(i).getText());
            }
            forUpdate.setIdentifiers(identifiers);
        }
        if (ctx.decrement() != null)
            forUpdate.setDecrement(visitDecrement(ctx.decrement()));
        if (ctx.increment() != null)
            forUpdate.setIncrement(visitIncrement(ctx.increment()));
        forUpdate.setLine(ctx.getStart().getLine());
        forUpdate.setCol(ctx.getStart().getCharPositionInLine());
        return forUpdate;
    }

    @Override
    public NumberValue visitNumber_value(sqlParser.Number_valueContext ctx) {
        NumberValue numberValue = new NumberValue();
        if (ctx.J_IDENTIFIER() != null) {
            numberValue.setType(NumberValue.Type.IDENTIFIER);
            numberValue.setValue(ctx.J_IDENTIFIER().getText());
        } else if (ctx.number() != null) {
            numberValue.setType(NumberValue.Type.NUMBER);
            numberValue.setValue(ctx.number().getText());
        } else {
            numberValue.setType(NumberValue.Type.TWO);
            numberValue.setValue(ctx.getText());
        }
        return numberValue;
    }

    @Override
    public String visitType_nam(sqlParser.Type_namContext ctx) {
        return visitAny_name(ctx.any_name());
    }

    @Override
    public CreateTypeStmt visitCreate_type_stmt(sqlParser.Create_type_stmtContext ctx) {
        CreateTypeStmt createTypeStmt = new CreateTypeStmt();
        createTypeStmt.setTypeName(visitType_nam(ctx.type_nam()));
        ArrayList<ColumnDef> columnDefs = new ArrayList<>();
        for (int i = 0; i < ctx.column_def().size(); i++) {
            columnDefs.add(visitColumn_def(ctx.column_def(i)));
        }
        createTypeStmt.setColumnDefs(columnDefs);
        createTypeStmt.setLine(ctx.getStart().getLine());
        createTypeStmt.setCol(ctx.getStart().getCharPositionInLine());
        return createTypeStmt;
    }

    @Override
    public CreateAggFunStmt visitCreate_agg_fun_stmt(sqlParser.Create_agg_fun_stmtContext ctx) {
        CreateAggFunStmt createAggFunStmt = new CreateAggFunStmt();
        createAggFunStmt.setFunctionName(visitFunction_name(ctx.function_name()));
        createAggFunStmt.setJarPath(visitJar_path(ctx.jar_path()));
        createAggFunStmt.setClassName(visitClass_name(ctx.class_name()));
        createAggFunStmt.setMethodName(visitMethod_name(ctx.method_name()));
        createAggFunStmt.setReturnType(visitReturn_type(ctx.return_type()));
        createAggFunStmt.setArrayType(visitArray(ctx.array()).getVariableValues().get(0).toString());
        createAggFunStmt.setLine(ctx.getStart().getLine());
        createAggFunStmt.setCol(ctx.getStart().getCharPositionInLine());
        return createAggFunStmt;
    }

    @Override
    public VarDeclaration visitVar_declaration(sqlParser.Var_declarationContext ctx) {
        VarDeclaration varDeclaration = new VarDeclaration();
        varDeclaration.setIdentifierName(ctx.J_IDENTIFIER().getText());
        if (ctx.variable_value() != null)
            varDeclaration.setVariableValue(visitVariable_value(ctx.variable_value()));
        varDeclaration.setLine(ctx.getStart().getLine());
        varDeclaration.setCol(ctx.getStart().getCharPositionInLine());
        return varDeclaration;
    }

    @Override
    public VarAssign visitVar_assign(sqlParser.Var_assignContext ctx) {
        VarAssign varAssign = new VarAssign();
        varAssign.setIdentifierName(ctx.J_IDENTIFIER().getText());
        varAssign.setVariableValue(visitVariable_value(ctx.variable_value()));
        varAssign.setLine(ctx.getStart().getLine());
        varAssign.setCol(ctx.getStart().getCharPositionInLine());
        return varAssign;
    }

    @Override
    public Increment visitIncrement(sqlParser.IncrementContext ctx) {
        Increment increment = new Increment();
        increment.setVariable(ctx.J_IDENTIFIER().getText());
        increment.setType(ctx.getChild(0) == ctx.J_IDENTIFIER() ? Decrement.Type.after : Decrement.Type.before);
        increment.setCol(ctx.getStart().getCharPositionInLine());
        return increment;
    }

    @Override
    public Decrement visitDecrement(sqlParser.DecrementContext ctx) {
        Decrement decrement = new Decrement();
        decrement.setVariable(ctx.J_IDENTIFIER().getText());
        decrement.setType(ctx.getChild(0) == ctx.J_IDENTIFIER() ? Decrement.Type.after : Decrement.Type.before);
        decrement.setCol(ctx.getStart().getCharPositionInLine());
        return decrement;
    }

    @Override
    public Object visitNumber(sqlParser.NumberContext ctx) {
        return super.visitNumber(ctx);
    }
//    private String getOperation(sqlParser.Boolean_valueContext ctx) {
//        if (ctx.EQ() != null)
//            return ctx.EQ().getSymbol().getText();
//        if (ctx.GT() != null)
//            return ctx.GT().getSymbol().getText();
//        if (ctx.LT() != null)
//            return ctx.LT().getSymbol().getText();
//        if (ctx.GT_EQ() != null)
//            return ctx.GT_EQ().getSymbol().getText();
//        if (ctx.NOT_EQ1() != null)
//            return ctx.NOT_EQ1().getSymbol().getText();
//        if (ctx.PIPE2() != null)
//            return ctx.PIPE2().getSymbol().getText();
//        if (ctx.LT_EQ() != null)
//            return ctx.LT_EQ().getSymbol().getText();
//        if (ctx.AMP2() != null)
//            return ctx.AMP2().getSymbol().getText();
//        return null;
//    }
}
