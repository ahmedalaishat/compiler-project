package Java.SymbolTable;

import Java.SymbolTable.type.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Flat {

    public static TableType getFlattedType(IMergedTable iMergedTable, ArrayList<String> columnsNames) {
        Map<String, Type> columns;
        if (iMergedTable instanceof TableType) {
            columns = getColumns((TableType) iMergedTable, "");
            columns = columns.entrySet().stream()
                    .filter(p -> columnsNames == null || columnsNames.contains(p.getKey()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            return new TableType(((TableType) iMergedTable).getTypeName(), columns);
        } else {
            ArrayList<TableType> types = ((MergedTable) iMergedTable).getTypesList();
            ArrayList<TableType> flattedTypes = new ArrayList<>();
            for (TableType tableType : types) {
                columns = getColumns(tableType, "");
                flattedTypes.add(new TableType(tableType.getTypeName(), tableType.getAlias(), columns));
            }
            TableType resultType = TableType.merge(flattedTypes, iMergedTable.getName());
            columns = resultType.getColumns().entrySet().stream()
                    .filter(p -> columnsNames == null || columnsNames.contains(p.getKey()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            resultType.setColumns(columns);
            return resultType;
        }
    }

    private static Map<String, Type> getColumns(TableType type, String prefix) {
        if (type == null)
            return null;
        Map<String, Type> map = new HashMap<>();
        for (Map.Entry<String, Type> column : type.getColumns().entrySet()) {
            if (column.getValue() instanceof PrimitiveType)
                map.put(prefix + column.getKey(), column.getValue());
            else if (column.getValue() instanceof TableType)
                map.putAll(getColumns((TableType) column.getValue(), prefix + column.getKey() + "$"));
        }
        return map;
    }
//    private static TableType getFlattedType(TableType tableType, ArrayList<String> columnsNames) {
//        String tableName = tableType.getTypeName();
//        Map<String, Type> columns;
//        //    select * from a
//        if (columnsNames == null)
//            columns = getTypes(tableType, tableName);
//            // select some column
//        else {
//            columns = new HashMap<>();
//            for (Map.Entry<String, Type> column : tableType.getColumns().entrySet())
//                if (columnsNames.contains(tableName + "$" + column.getKey()) || columnsNames.contains(column.getKey()))
//                    columns.put(column.getKey(), column.getValue());
//        }
//        return new TableType(tableName, columns);
//    }
//        if (iMergedTable instanceof TableType)
//            return getFlattedType(((TableType) iMergedTable), null);
//        else if (iMergedTable instanceof MergedTable)
//            return TableType.merge(getFlattedType(((MergedTable) iMergedTable).getLeft()),
//                    getFlattedType(((MergedTable) iMergedTable).getRight()), iMergedTable.getName());
//        return null;
//    public static TableType getFlattedType(IMergedTable iMergedTable) {
////        String tableName = iMergedTable.getName();
////        Map<String, Type> columns;
//        if (iMergedTable instanceof TableType)
//            return getFlattedType(((TableType) iMergedTable), null);
//        else if (iMergedTable instanceof MergedTable)
//            return TableType.merge(getFlattedType(((MergedTable) iMergedTable).getLeft()),
//                    getFlattedType(((MergedTable) iMergedTable).getRight()),iMergedTable.getName());
//        return null;
//    }
////  for semantic check (no longer used)
//    public boolean canIn(String type) {
//        if (columns.size() != 1)
//            return false;
//        Map.Entry<String, Type> entry = columns.entrySet().iterator().next();
//        return ((TableType) entry.getValue()).getTypeName().equals(type);
//    }
//    public FLATType(String tableName) {
//        this.tableName = tableName;
//        columns = getTypes(Main.symbolTable.getDeclaredType(tableName), "");
//        System.out.println("");
//    }
//    public FLATType(String tableName, ArrayList<String> columnsNames) {
//        this.tableName = tableName;
//        columns = new HashMap<>();
//        FLATType temp = new FLATType(tableName);
//        if (temp.columns != null)
//            for (Map.Entry<String, Type> column : temp.getColumns().entrySet()) {
//                if (columnsNames.contains(column.getKey()))
//                    columns.put(column.getKey(), column.getValue());
//            }
//        System.out.println("");
//    }
}
