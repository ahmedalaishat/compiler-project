package Java.SymbolTable.type;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TableType extends Type implements IMergedTable, Cloneable {
    private String typeName;
    private String alias;
    private boolean isSymbol;
    private Map<String, Type> columns = new HashMap<>();
    private String type;
    private String path;


    public TableType clone() {
        try {
            return (TableType) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static TableType merge(TableType table1, TableType table2, String mergedName) {
        TableType merged = new TableType();
        merged.typeName = mergedName;
        for (Map.Entry<String, Type> entry : table1.columns.entrySet()) {
            merged.columns.put(/*table1.tableAlias !=null ?table1.tableAlias :*/table1.getName() + "$" + entry.getKey(), entry.getValue());
        }
        for (Map.Entry<String, Type> entry : table2.columns.entrySet()) {
            merged.columns.put(/*table2.tableAlias !=null ?table2.tableAlias :*/table2.getName() + "$" + entry.getKey(), entry.getValue());
        }
        return merged;
    }

    public static TableType merge(ArrayList<TableType> tableTypes, String mergedName) {
        TableType merged = new TableType();
        merged.typeName = mergedName;
//        TODO tableType.size=1
        for (TableType tableType : tableTypes) {
            for (Map.Entry<String, Type> entry : tableType.columns.entrySet()) {
                merged.columns.put((tableType.getAlias() != null ? tableType.getAlias() :
                        tableType.getTypeName())
                        + "$" + entry.getKey(), entry.getValue());
            }
        }
        return merged;
    }

    public TableType(String typeName) {
        this.typeName = typeName;
    }

    public TableType() {
    }

    public TableType(String typeName, String alias, Map<String, Type> columns) {
        this.typeName = typeName;
        this.columns = columns;
        this.alias = alias;
    }

    public TableType(String typeName, Map<String, Type> columns) {
        this.typeName = typeName;
        this.columns = columns;
        this.alias = alias;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Map<String, Type> getColumns() {
        return columns;
    }

    public void setColumns(Map<String, Type> columns) {
        this.columns = columns;
    }

    public String isColumnTypeExist(String name) {
        return columns.get(name) != null ? name : null;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableType type = (TableType) o;
        if (!Objects.equals(typeName, type.typeName))
            return false;
        if (columns.size() != ((TableType) o).columns.size())
            return false;
        for (Map.Entry<String, Type> entry : columns.entrySet()) {
            if (!type.getColumns().get(entry.getKey()).equals(entry.getValue()))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(typeName, columns);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String generateClassContent() {
        return generateVars() +
                ((type != null && type.equals("csv")) ? generateLoadCsv() : "") +
                ((type != null && type.equals("json")) ? generateLoadJson() : "") +
                ((type != null) ? generateGetter() : "") +
                generateToString();
    }

    private String generateVars() {
        StringBuilder builder = new StringBuilder();
        if (path != null) {
            builder.append("\n\tprivate static FilterableSequence<")
                    .append(typeName)
                    .append("> data;");
        }
        for (Map.Entry<String, Type> column : columns.entrySet()) {
            builder.append("\n\tpublic ");
            if (column.getValue() instanceof TableType) {
                builder.append(((TableType) column.getValue()).getTypeName())
                        .append(" ");
                if (((TableType) column.getValue()).getAlias() == null)
                    builder.append(((TableType) column.getValue()).getTypeName());
                else
                    builder.append(((TableType) column.getValue()).getAlias());
                builder.append(";");
            } else if (column.getValue() instanceof NumberType) {
                builder.append("double ")
                        .append(column.getKey())
                        .append(";");
            } else if (column.getValue() instanceof StringType) {
                builder.append("String ")
                        .append(column.getKey())
                        .append(";");
            } else if (column.getValue() instanceof BooleanType) {
                builder.append("boolean ")
                        .append(column.getKey())
                        .append(";");
            }
        }
        return builder.toString();
    }


    private String generateLoadCsv() {
        StringBuilder builder = new StringBuilder()
                .append("\n\n\tpublic static void load()  throws IOException {")
                .append("\n\t\tFile myObj = new File(\"")
                .append(path)
                .append("\");")
                .append("\n\t\tScanner myReader = new Scanner(myObj);")
                .append("\n\t\tArrayList<").append(typeName)
                .append("> list=new ArrayList<>();")
                .append("\n\t\twhile (myReader.hasNextLine()) {")
                .append("\n\t\t\tString line = myReader.nextLine();")
                .append("\n\t\t\tString[] row = line.split(\",\");");
        builder.append("\n\t\t\t")
                .append(typeName)
                .append(" element = new ")
                .append(typeName)
                .append("();");
        int count = 0;
        for (Map.Entry<String, Type> column : columns.entrySet()) {
            if (column.getValue() instanceof PrimitiveType)
                builder.append("\n\t\t\telement.")
                        .append(column.getKey());
            if (column.getValue() instanceof NumberType) {
                builder.append("= Double.parseDouble(row[")
                        .append(count)
                        .append("]);");
            } else if (column.getValue() instanceof StringType) {
                builder.append("= row[")
                        .append(count)
                        .append("];");
            } else if (column.getValue() instanceof BooleanType) {
                builder.append("= Double.parseBoolean(row[")
                        .append(count)
                        .append("]);");
            }
            count++;
        }
        builder.append("\n\t\t\tlist.add(element);")
                .append("\n\t\t}")
                .append("data=new FilterableSequence<>(list);")
                .append("\n\t\tmyReader.close();")
                .append("\n\t}");
        return builder.toString();
    }

    private String generateLoadJson() {
        return "\n" +
                "\n" +
                "\tpublic static void load()  throws IOException {\n" +
                "\t\tGson gson = new Gson();\n" +
                "\t\tInputStreamReader in = new InputStreamReader(new FileInputStream(\"" + path + "\"));\n" +
                "\t\tJsonReader jsonReader = new JsonReader(in);\n" +
                "\t\tJsonObject fromJson  = gson.fromJson(jsonReader, JsonObject.class);\n" +
                "\t\tJsonElement jsonElement= fromJson.get(\"" + typeName + "\");\n" +
                "\t\tArrayList<" + typeName + "> d = gson.fromJson(jsonElement,new TypeToken<ArrayList<" + typeName + ">>() {}.getType());\n" +
                "\t\tdata=new FilterableSequence<>(d);\n" +
                "\t\tin.close();\n" +
                "\t\tjsonReader.close();\n" +
                "\t}";
    }

    private String generateGetter() {
        return "\n" +
                "\n" +
                "\tpublic static FilterableSequence<" + typeName + "> getData()  throws IOException {\n" +
                "\t\tif (data==null)\n" +
                "\t\t\tload();\n" +
                "\t\treturn data;\n" +
                "\t}";
    }

    public String generateToString() {
        StringBuilder builder = new StringBuilder();
        builder.append("\n" +
                "\n" +
                "\t@Override\n" +
                "\tpublic String toString() {\n" +
                "\t\treturn \"").append(typeName).append("{\" +\n");
        for (Map.Entry<String, Type> entry : columns.entrySet()) {
            String name;
            if (entry.getValue() instanceof TableType && ((TableType) entry.getValue()).getAlias() != null)
                name = ((TableType) entry.getValue()).getAlias();
            else
                name = entry.getKey();
            builder.append("\t\t\t\t\"").append(name).append("='\" + ").append(name).append(" + '\\'' +\n");
        }
        builder.append("\t\t\t\t'}';\n" +
                "\t}");
        return builder.toString();
    }

    @Override
    public String getName() {
        return getTypeName();
    }

    public boolean isSymbol() {
        return isSymbol;
    }

    public void setSymbol(boolean symbol) {
        isSymbol = symbol;
    }
}
