package Java.SymbolTable.type;

public abstract class Type {
    public final static String NUMBER_CONST = "number";
    public final static String STRING_CONST = "string";
    public final static String BOOLEAN_CONST = "boolean";

    public static PrimitiveType createPrimitiveFrom(String type) {
        if (type == null) return null;
        if (type.toLowerCase().equals(NUMBER_CONST)) return new NumberType();
        if (type.toLowerCase().equals(STRING_CONST)) return new StringType();
        if (type.toLowerCase().equals(BOOLEAN_CONST)) return new BooleanType();
        return null; //mustn't reached
    }
}
