package Java.SymbolTable.type;

import Java.AST.SqlStmt.Expr;
import Java.AST.SqlStmt.JoinClause;
import Java.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MergedTable implements IMergedTable {

    private IMergedTable left;
    private IMergedTable right;
    private JoinClause.JoinOperator operator;
    private Expr innerJoinExpr;
    private String alias;

    public Expr getInnerJoinExpr() {
        return innerJoinExpr;
    }

    public void setInnerJoinExpr(Expr innerJoinExpr) {
        this.innerJoinExpr = innerJoinExpr;
    }

    public ArrayList<MergedTable> getMergedTables() {
        ArrayList<MergedTable> mergedTables = new ArrayList<>();
        if (left instanceof TableType && right instanceof TableType) {
            mergedTables.add(this);
        } else if (left instanceof MergedTable && right instanceof TableType) {
            mergedTables = ((MergedTable) left).getMergedTables();
            mergedTables.add(this);
        }
        return mergedTables;
    }

    public TableType getMergedType() {
        return new TableType(getName(),getAlias(),getType(this));
    }
    public ArrayList<TableType> getTypesList() {
        return getTypeList(this);
    }


    private Map<String , Type> getType(IMergedTable iMergedTable) {
        Map<String,Type> types=new LinkedHashMap<>();
        if (iMergedTable instanceof TableType) {
            types.put(iMergedTable.getName(),(TableType) iMergedTable);
            return types;
        } else if (iMergedTable instanceof MergedTable){
            types.putAll(getType(((MergedTable) iMergedTable).getLeft()));
            types.putAll(getType(((MergedTable) iMergedTable).getRight()));
        }
        return types;
    }


    private ArrayList<TableType> getTypeList(IMergedTable iMergedTable) {
        ArrayList<TableType> types=new ArrayList<>();
        if (iMergedTable instanceof TableType) {
            types.add((TableType) iMergedTable);
            return types;
        } else if (iMergedTable instanceof MergedTable){
            types.addAll(getTypeList(((MergedTable) iMergedTable).getLeft()));
            types.addAll(getTypeList(((MergedTable) iMergedTable).getRight()));
        }
        return types;
    }

    private static Map<String, Type> getTypes(TableType type) {
        if (type == null)
            return null;
        Map<String, Type> map = new HashMap<>();
        for (Map.Entry<String, Type> column : type.getColumns().entrySet()) {
            map.put( /*(!tableName.equals("") ? "." : "") +*/ column.getKey(), column.getValue());
        }
        return map;
    }
//    public TableType getMergedType() {
//        return getType(this);
//    }
//
//
//    private TableType getType(IMergedTable iMergedTable) {
//        String tableName = iMergedTable.getName();
//        Map<String, Type> columns;
//        if (iMergedTable instanceof TableType) {
//            columns = getTypes((TableType) iMergedTable);
//            return new TableType(tableName, columns);
//        } else if (iMergedTable instanceof MergedTable)
//            return TableType.merge(getType(((MergedTable) iMergedTable).getLeft()),
//                    getType(((MergedTable) iMergedTable).getRight()), iMergedTable.getName());
//        return null;
//    }
//
//    private static Map<String, Type> getTypes(TableType type) {
//        if (type == null)
//            return null;
//        Map<String, Type> map = new HashMap<>();
//        for (Map.Entry<String, Type> column : type.getColumns().entrySet()) {
//            map.put( /*(!tableName.equals("") ? "." : "") +*/ column.getKey(), column.getValue());
//        }
//        return map;
//    }

    public String getPredicate() {
        if (innerJoinExpr != null)
            return innerJoinExpr.getPredicate();
        return null;
    }

    public IMergedTable getLeft() {
        return left;
    }

    public void setLeft(IMergedTable left) {
        this.left = left;
    }

    public IMergedTable getRight() {
        return right;
    }

    public void setRight(IMergedTable right) {
        this.right = right;
    }

    public JoinClause.JoinOperator getOperator() {
        return operator;
    }

    public void setOperator(JoinClause.JoinOperator operator) {
        this.operator = operator;
    }

    @Override
    public String getName() {
        return Util.capitalize(left.getName()) + "Merge" + Util.capitalize(right.getName());
    }

    @Override
    public String getAlias() {
        return alias;
    }
}
