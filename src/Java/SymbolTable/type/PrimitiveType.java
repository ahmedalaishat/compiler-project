package Java.SymbolTable.type;

public abstract class PrimitiveType extends Type {
    private String alias;
    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        return true;
    }
}
