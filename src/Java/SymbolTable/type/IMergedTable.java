package Java.SymbolTable.type;

public interface IMergedTable {
    String getName();
    String getAlias();
}
