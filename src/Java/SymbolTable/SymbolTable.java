package Java.SymbolTable;

import Java.SymbolTable.type.TableType;

import java.util.ArrayList;
import java.util.Map;


public class SymbolTable {


    private ArrayList<Scope> scopes = new ArrayList<Scope>();

    private ArrayList<TableType> declaredTypes = new ArrayList<TableType>();
    private ArrayList<AggregationFunction> declaredAggregationFunction = new ArrayList<AggregationFunction>();


    public ArrayList<Scope> getScopes() {
        return scopes;
    }

    public Scope getScope(String id) {
        for (Scope scope : scopes) {
            if (scope.getId().equals(id))
                return scope;
        }
        return null;
    }

    public ArrayList<AggregationFunction> getDeclaredAggregationFunction() {
        return declaredAggregationFunction;
    }

    public void setScopes(ArrayList<Scope> scopes) {
        this.scopes = scopes;
    }

    public ArrayList<TableType> getDeclaredTypes() {
        return declaredTypes;
    }

    public void setDeclaredTypes(ArrayList<TableType> declaredTypes) {
        this.declaredTypes = declaredTypes;
    }


    public void addScope(Scope scope) {
        this.scopes.add(scope);
    }

    public void addType(TableType tableType) {
        this.declaredTypes.add(tableType);
    }

    public TableType getDeclaredType(String name) {
        for (TableType tableType : declaredTypes) {
            if (tableType.getTypeName().equals(name))
                return tableType;
        }
        return null;
    }

    public String getAggJarPath(String funName){
        for (AggregationFunction aggregationFunction : declaredAggregationFunction) {
            if (aggregationFunction.getMethodName().equalsIgnoreCase(funName))
                return aggregationFunction.getJarPath();
        }
        return null;
    }

    public String getAggClassName(String funName){
        for (AggregationFunction aggregationFunction : declaredAggregationFunction) {
            if (aggregationFunction.getAggregationFunctionName().equalsIgnoreCase(funName))
                return aggregationFunction.getClassName();
        }
        return null;
    }

    public boolean aggFunctionExists(String funName) {
        for (AggregationFunction aggregationFunction : declaredAggregationFunction) {
            if (aggregationFunction.getAggregationFunctionName().equals(funName))
                return true;
        }
        return false;
    }

    public TableType getSymbolType(String symbolName) {
        for (Scope scope : scopes) {
            for (Map.Entry<String,Symbol> entry : scope.getSymbolMap().entrySet()) {
                if (entry.getKey().equals(symbolName)&&entry.getValue().getType()instanceof TableType){
                    TableType tableType=((TableType) entry.getValue().getType()).clone();
                    tableType.setAlias(symbolName);
                    return tableType;
                }
            }
        }
        return null;
    }
}
