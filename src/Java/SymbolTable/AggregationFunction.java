package Java.SymbolTable;

import Java.SymbolTable.type.*;

public class AggregationFunction extends Type {
    private String AggregationFunctionName;
    private String JarPath;
    private String alias;
    private String ClassName;
    private String MethodName;
    private String returnType;
    private String arrayType;


    public AggregationFunction() {
    }

    public AggregationFunction(String aggregationFunctionName, String jarPath, String alias,
                               String className, String methodName, String returnType, String arrayType) {
        AggregationFunctionName = aggregationFunctionName;
        JarPath = jarPath;
        this.alias = alias;
        ClassName = className;
        MethodName = methodName;
        this.returnType = returnType;
        this.arrayType = arrayType;
    }

    public AggregationFunction(String aggregationFunctionName, String jarPath, String className, String methodName, String returnType, String arrayType) {
        AggregationFunctionName = aggregationFunctionName;
        JarPath = jarPath;
        ClassName = className;
        MethodName = methodName;
        this.returnType = returnType;
        this.arrayType = arrayType;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAggregationFunctionName() {
        return AggregationFunctionName;
    }

    public void setAggregationFunctionName(String aggregationFunctionName) {
        AggregationFunctionName = aggregationFunctionName;
    }

    public String getJarPath() {
        return JarPath;
    }

    public void setJarPath(String jarPath) {
        JarPath = jarPath;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getMethodName() {
        return MethodName;
    }

    public void setMethodName(String methodName) {
        MethodName = methodName;
    }

    public PrimitiveType getReturnType() {
        if (returnType.equalsIgnoreCase("boolean"))
            return new BooleanType();
        if (returnType.equalsIgnoreCase("string"))
            return new StringType();
        if (returnType.equalsIgnoreCase("number"))
            return new NumberType();
        return null;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getArrayType() {
        return arrayType;
    }

    public void setArrayType(String arrayType) {
        this.arrayType = arrayType;
    }

    public PrimitiveType getType() {
//        return type must be string or number or boolean
        if (returnType.equals(NUMBER_CONST)) return new NumberType();
        if (returnType.equals(BOOLEAN_CONST)) return new BooleanType();
        if (returnType.equals(STRING_CONST)) return new StringType();
        return null;
    }

    public String getAlias() {
        return alias;
    }
}