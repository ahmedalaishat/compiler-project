/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 by Bart Kiers
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * Project      : sqlite-parser; an ANTLR4 grammar for SQLite
 *                https://github.com/bkiers/sqlite-parser
 * Developed by : Bart Kiers, bart@big-o.nl
 */
grammar sql;

parse
 : stmt_list|( error)* EOF
 ;

error
 : UNEXPECTED_CHAR
   {
     throw new RuntimeException("UNEXPECTED_CHAR=" + $UNEXPECTED_CHAR.text);
   }
 ;


stmt_list
 :(function|inst|sql_stmt)*
 ;

sql_stmt
 :
 (create_table_stmt
 | select_stmt
 | create_type_stmt
 |create_agg_fun_stmt
 ) ';'
 ;

create_table_stmt
 : K_CREATE  K_TABLE ( (K_IF|JK_IF) K_NOT K_EXISTS )?
   table_name
   ( '(' column_def ( ',' column_def )* ')' )
   K_TYPE '=' table_type
   'path' '=' path
 ;

create_type_stmt
 : K_CREATE  K_TYPE
   type_nam
   ( '(' column_def ( ',' column_def )* ')' )
 ;

create_agg_fun_stmt
 : K_CREATE  K_AGGREGATION_FUNCTION
   function_name
   ( '(' jar_path ',' class_name ',' method_name ',' return_type ',' array ')' )
 ;

select_stmt
 : ((selecting from where? group_by?)|values) ordering_by? limit?
 ;

 limit:
 K_LIMIT expr ( ( K_OFFSET | ',' ) expr )?
 ;

 ordering_by:
   K_ORDER K_BY ordering_term ( ',' ordering_term )*
   ;

 values:
 K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
 ;

 group_by:
 K_GROUP K_BY expr ( ',' expr )* having?
 ;

 having:
 ( K_HAVING expr );

 where:
 K_WHERE (expr|sub_exp)
 ;

 from:
 K_FROM  join_clause | table_or_subquery_list /////AHMED_MARWAN DELETE ? AT THR END
 ;

 selecting:
 K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
 ;

column_def
 : column_name ( type_name )*
 ;

type_name
 : name ( '(' number (any_name)? ')' | '(' number (any_name)? ',' number (any_name)? ')' )?
 ;

/*
    SQLite understands the following binary operators, in order from highest to
    lowest precedence:
    ||
    *    /    %
    +    -
    <<   >>   &    |
    <    <=   >    >=
    =    ==   !=   <>   IS   IS NOT   IN   LIKE   GLOB   MATCH   REGEXP
    AND
    OR
*/
expr
 : literal_value
 | column_expr
 | unary_operator_expr
 | expr K_NOT? K_IN ( '(' ( select_stmt | expr ( ',' expr )* ) ')' | table_name )
 | expr expr_operation expr
 | ( ( K_NOT )? K_EXISTS )? '(' select_stmt ')'
 | function_expr
 | other_expr
 ;

 other_expr:
 '(' expr ')';

 expr_operation:
 PIPE2| MULTIPLY | DIVISION | PERCENT | PLUS | MINUS | LTLT | GTGT |
  AMP | PIPE | LT | LT_EQ | GT | GT_EQ | SEQ | EQ | NOT_EQ1 | NOT_EQ2 |
  K_IS | K_NOT | (K_NOT? K_LIKE) | K_GLOB | K_MATCH | K_REGEXP |K_AND|K_OR;

 column_expr
 : ( table_name '.' )* column_name
 ;

 unary_operator_expr
 : unary_operator expr
 ;

 function_expr
 : function_name '(' ( K_DISTINCT? expr ( ',' expr )* | '*' )? ')'
 ;

ordering_term
 : expr ( K_COLLATE collation_name )? ( K_ASC | K_DESC )?
 ;

result_column
 : '*'
 | table_name '.' '*'
 | expr ( K_AS? column_alias )?
 ;

/////////AHMED_MARWAN table sub add
table_or_subquery
 : table| sub_query
 ;

 table_or_subquery_list:
 table_or_subquery ( ',' table_or_subquery )*
 ;

table :
 basic_table
 | '(' table_or_subquery_list')' ( K_AS? table_alias )?
 | '(' join_clause ')' ( K_AS? table_alias )?

 ;

 basic_table:
 table_name ( K_AS? table_alias )? ( K_INDEXED K_BY index_name | K_NOT K_INDEXED )?
 ;

 sub_query:
 '(' select_stmt ')' ( K_AS? table_alias )?
 ;

join_clause
 : table_or_subquery ( join_operator table_or_subquery join_constraint )*
 ;

join_operator
 : ','
 | ( K_LEFT K_OUTER? | K_INNER )? K_JOIN
 ;


join_constraint
 : ( K_ON expr)?
 ;

//signed_number
// : ( ( '+' | '-' )? NUMERIC_LITERAL | '*' )
// ;

literal_value
 :number
 | NUMERIC_LITERAL
 | STRING_LITERAL
 | J_STRING
 | BLOB_LITERAL
 | K_NULL
 | K_CURRENT_TIME
 | K_CURRENT_DATE
 | K_CURRENT_TIMESTAMP
 ;

unary_operator
 : '-'
 | '+'
 | '~'
 | K_NOT
 ;

column_alias
 : sql_identifirer
 | STRING_LITERAL
 ;

function_name
 : J_IDENTIFIER
 ;

name
 : any_name
 ;

database_name
 : any_name
 ;

table_name
 : any_name
 ;

column_name
 : any_name
 ;

collation_name
 : any_name
 ;

index_name
 : any_name
 ;

table_alias
 : any_name
 ;

 jar_path
 : any_name
 ;

 class_name
 : any_name
 ;

 method_name
 : any_name
 ;

 return_type
 : any_name
 ;

 path
 :  any_name
 ;

 table_type
 : any_name
 ;

 type_nam
 : any_name
 ;

any_name
 : sql_identifirer
 | STRING_LITERAL
 | '(' any_name ')'
 ;

sql_identifirer
 : J_STRING
 | STRING_LITERAL2
// | '[' ~']'* ']'
 | J_IDENTIFIER
 ;

sub_exp
 :column_name operation '(' select_stmt ')'
 ;

operation
 : ((GT | LT |GT_EQ |LT_EQ |NOT_EQ1 |NOT_EQ2) (K_ANY|K_ALL)? )|(K_NOT? K_IN);



/////AHMED_MARWAN basic component in java
/////conditions
boolean_value
 :(number_value) (GT |LT |EQ|GT_EQ |LT_EQ |NOT_EQ1|AMP2|PIPE2) (number_value)
 |(string_value) (EQ|NOT_EQ1) (string_value)
 | J_IDENTIFIER
 | JK_TRUE
 | JK_FALSE
 |'!' boolean_value
 | boolean_value (EQ|NOT_EQ1|AMP2|PIPE2) boolean_value
 | '(' boolean_value ')'
 ;

/////variable_value(number,string int,boolean,attribute within variable)
variable_value
 : J_IDENTIFIER
 | boolean_value
 | string_value
 | number_value
 | increment
 | decrement
 | JK_NULL
 | call_function
 | select_stmt
 | enhanced_if_stmt
 ;

 string_value:
 J_IDENTIFIER|J_STRING|J_CHAR;

 number_value
 :number_value ('+'|'-'|'*'|'/') number_value
 |number
 |J_IDENTIFIER
 ;

values_list
 : variable_value (',' variable_value)*
 ;

list_args
 : variable_value (',' variable_value)*
 ;

/////AHMED_MARWAN java_parser

////function
function
 : function_name function_header function_body
 ;

function_header
 : '(' list_params? ')'
 ;

list_params
 : param (',' param)*
 ;

param
//TODO add var decleration
 :( NK_VAR J_IDENTIFIER ('=' variable_value)?)
 ;

function_body
 : '{' list_inst '}'
 ;

list_inst
 : inst*
 ;

/////AHMED_MARWAN instuctions(defenetion variable or array, java blocks, call functions, return, break)
inst
 :increment';'|decrement';'|break_inst|return_inst|call_function ';'|var_declaration ';'|var_assign ';' |java_statement
 ;

return_inst
 : JK_RETURN variable_value? ';'
 ;

break_inst
 : JK_BREAK ';'
 ;

//store (select result, json object, any variable vlaue) in variable
var_declaration
 : NK_VAR J_IDENTIFIER ('=' variable_value)?
 ;
var_assign
 : J_IDENTIFIER '=' variable_value
 ;

 enhanced_if_stmt
 : ((boolean_value) '?' (variable_value) ':' (variable_value))|('(' enhanced_if_stmt')')
 ;

array
 : '[' values_list? ']'
  ;

/////call function
call_function
 : function_name '(' list_args? ')'
 ;

/////AHMED_MARWAN java blocks(if, for, foreach, while, do while, switch)
java_statement
 : print_stmt|while_stmt|do_while_stmt|switch_stmt|if_stmt|for_stmt|foreach_stmt
 ;

////print statment
 print_stmt:
 JK_PRINT '(' variable_value ')'';'
 ;

//////if statment
if_stmt
 : JK_IF '(' boolean_value ')' (('{' list_inst '}')|inst) else_stmt?
 ;

else_stmt
: JK_ELSE (('{' list_inst '}')|inst)
;

//////do while statment
do_while_stmt
 : JK_DO '{' list_inst '}' JK_WHILE '(' boolean_value ')'';'
 ;

//////while statment
while_stmt
 : JK_WHILE '(' boolean_value ')' (('{' list_inst '}') |inst)
 ;

//////switch statment
switch_stmt
 : JK_SWITCH'(' J_IDENTIFIER ')' '{' list_cases '}'
 ;

list_cases
 : case_block* default_block?
 ;

case_block
 : JK_CASE (number|J_CHAR|J_STRING) ':' (case_body|'{'case_body '}')
 ;

default_block
 : JK_DEFAULT ':'  (case_body|'{'case_body '}')
 ;

case_body
 : list_inst break_inst?
 ;

////for_statment
foreach_stmt
 : JK_FOR '('NK_VAR J_IDENTIFIER ':' J_IDENTIFIER ')' ('{' list_inst '}' |inst)
 ;

for_stmt
 : JK_FOR '(' for_initialization? ';' boolean_value? ';' for_update? ')' ('{' list_inst '}' |inst)
 ;

for_initialization
 : var_declaration|var_assign
 ;

for_update
 : increment|decrement|(J_IDENTIFIER '=' J_IDENTIFIER ('+'|'-') J_UNSIGN_NUMBER)|(J_IDENTIFIER ('+='|'-=') J_UNSIGN_NUMBER)
 ;

 increment
 : (J_IDENTIFIER '++')|('++' J_IDENTIFIER )
 ;

 decrement
 : (J_IDENTIFIER '--')|('--' J_IDENTIFIER )
 ;

number
   : J_SIGN_NUMBER |J_UNSIGN_NUMBER
   ;

PIPE2 : '||';
AMP2 : '&&';
AMP : '&';
PIPE : '|';
LT : '<';
LT_EQ : '<=';
GT : '>';
GT_EQ : '>=';
EQ : '==';
NOT_EQ1 : '!=';
NOT_EQ2 : '<>';
PLUS : '+';
MINUS : '-';


SEQ : '=';
GTGT : '>>';
LTLT : '<<';
PERCENT : '%';
DIVISION : '/';
MULTIPLY : '*';

/////AHMED_MARWAN NEW KEYWORDS
NK_VAR : V A R;
NK_FUNCTION: 'function';

/////AHMED_MARWAN JAVA KEYWORDS
JK_BREAK: 'break';
JK_CASE : 'case';
JK_DEFAULT : 'default';
JK_DO : 'do';
JK_ELSE : 'else';
JK_FALSE: 'false';
JK_FOR : 'for';
JK_IF : 'if';
JK_NULL: 'null';
JK_PRINT : 'print';
JK_RETURN:'return';
JK_SWITCH: 'switch';
JK_TRUE :'true';
JK_WHILE : 'while';

// http://www.sqlite.org/lang_keywords.html
K_ABORT : A B O R T;
K_ACTION : A C T I O N;
K_ADD : A D D;
K_AFTER : A F T E R;
K_ALL : A L L;
K_ANY :A N Y;
K_ALTER : A L T E R;
K_ANALYZE : A N A L Y Z E;
K_AND : A N D;
K_AS : A S;
K_ASC : A S C;
K_ATTACH : A T T A C H;
K_AUTOINCREMENT : A U T O I N C R E M E N T;
K_BEFORE : B E F O R E;
K_BEGIN : B E G I N;
K_BETWEEN : B E T W E E N;
K_BY : B Y;
K_CASCADE : C A S C A D E;
K_CASE : C A S E;
K_CAST : C A S T;
K_CHECK : C H E C K;
K_COLLATE : C O L L A T E;
K_COLUMN : C O L U M N;
K_COMMIT : C O M M I T;
K_CONFLICT : C O N F L I C T;
K_CONSTRAINT : C O N S T R A I N T;
K_CREATE : C R E A T E;
K_CROSS : C R O S S;
K_CURRENT_DATE : C U R R E N T '_' D A T E;
K_CURRENT_TIME : C U R R E N T '_' T I M E;
K_CURRENT_TIMESTAMP : C U R R E N T '_' T I M E S T A M P;
K_DATABASE : D A T A B A S E;
K_DEFAULT : D E F A U L T;
K_DEFERRABLE : D E F E R R A B L E;
K_DEFERRED : D E F E R R E D;
K_DELETE : D E L E T E;
K_DESC : D E S C;
K_DETACH : D E T A C H;
K_DISTINCT : D I S T I N C T;
K_DROP : D R O P;
K_EACH : E A C H;
K_ELSE : E L S E;
K_END : E N D;
K_ENABLE : E N A B L E;
K_ESCAPE : E S C A P E;
K_EXCEPT : E X C E P T;
K_EXCLUSIVE : E X C L U S I V E;
K_EXISTS : E X I S T S;
K_EXPLAIN : E X P L A I N;
K_FAIL : F A I L;
K_FOR : F O R;
K_FOREIGN : F O R E I G N;
K_FROM : F R O M;
K_FULL : F U L L;
K_GLOB : G L O B;
K_GROUP : G R O U P;
K_HAVING : H A V I N G;
K_IF : I F;
K_IGNORE : I G N O R E;
K_IMMEDIATE : I M M E D I A T E;
K_IN : I N;
K_INDEX : I N D E X;
K_INDEXED : I N D E X E D;
K_INITIALLY : I N I T I A L L Y;
K_INNER : I N N E R;
K_INSERT : I N S E R T;
K_INSTEAD : I N S T E A D;
K_INTERSECT : I N T E R S E C T;
K_INTO : I N T O;
K_IS : I S;
K_ISNULL : I S N U L L;
K_JOIN : J O I N;
K_KEY : K E Y;
K_LEFT : L E F T;
K_LIKE : L I K E;
K_LIMIT : L I M I T;
K_MATCH : M A T C H;
K_NATURAL : N A T U R A L;
K_NEXTVAL : N E X T V A L;
K_NO : N O;
K_NOT : N O T;
K_NOTNULL : N O T N U L L;
K_NULL : N U L L;
K_OF : O F;
K_OFFSET : O F F S E T;
K_ON : O N;
K_ONLY : O N L Y;
K_OR : O R;
K_ORDER : O R D E R;
K_OUTER : O U T E R;
K_PLAN : P L A N;
K_PRAGMA : P R A G M A;
K_PRIMARY : P R I M A R Y;
K_QUERY : Q U E R Y;
K_RAISE : R A I S E;
K_RECURSIVE : R E C U R S I V E;
K_REFERENCES : R E F E R E N C E S;
K_REGEXP : R E G E X P;
K_REINDEX : R E I N D E X;
K_RELEASE : R E L E A S E;
K_RENAME : R E N A M E;
K_REPLACE : R E P L A C E;
K_RESTRICT : R E S T R I C T;
K_RIGHT : R I G H T;
K_ROLLBACK : R O L L B A C K;
K_ROW : R O W;
K_SAVEPOINT : S A V E P O I N T;
K_SELECT : S E L E C T;
K_SET : S E T;
K_TABLE : T A B L E;
K_TYPE : T Y P E;
K_TEMP : T E M P;
K_TEMPORARY : T E M P O R A R Y;
K_THEN : T H E N;
K_TO : T O;
K_TRANSACTION : T R A N S A C T I O N;
K_TRIGGER : T R I G G E R;
K_UNION : U N I O N;
K_UNIQUE : U N I Q U E;
K_UPDATE : U P D A T E;
K_USING : U S I N G;
K_VACUUM : V A C U U M;
K_VALUES : V A L U E S;
K_VIEW : V I E W;
K_VIRTUAL : V I R T U A L;
K_WHEN : W H E N;
K_WHERE : W H E R E;
K_WITH : W I T H;
K_WITHOUT : W I T H O U T;
K_AGGREGATION_FUNCTION: A G G R E G A T I O N '_' F U N C T I O N;

J_IDENTIFIER
 :[a-zA-Z_] [a-zA-Z_0-9]*
 | [a-zA-Z_] [a-zA-Z_0-9]*
 ;

J_UNSIGN_NUMBER
   : DIGIT+ ( '.' DIGIT* )?
   ;
J_SIGN_NUMBER
   : ('+'|'-') DIGIT+ ( '.' DIGIT* )?
   ;
J_STRING
 : '"' ( ~'"' )* '"'
 ;

J_CHAR
 : '\'' [a-zA-Z_0-9] '\''
 ;

STRING_LITERAL
 : '\'' ( ~'\'' | '\'\'' )* '\''
 ;

STRING_LITERAL2
 : '`' (~'`' | '``')* '`'
 ;

NUMERIC_LITERAL
 : DIGIT+ ( '.' DIGIT* )? ( E [-+]? DIGIT+ )?
 | '.' DIGIT+ ( E [-+]? DIGIT+ )?
 ;

BLOB_LITERAL
 : X STRING_LITERAL
 ;

SINGLE_LINE_COMMENT
 : '//' ~[\r\n]* -> channel(HIDDEN)
 ;

MULTILINE_COMMENT
 : '/*' .*? ( '*/' | EOF ) -> channel(HIDDEN)
 ;

SPACES
 : [ \u000B\t\r\n] -> channel(HIDDEN)
 ;

WS
 : [ \t\n\r] + -> skip
 ;

UNEXPECTED_CHAR
 : .
 ;

fragment DIGIT : [0-9];
fragment A : [aA];
fragment B : [bB];
fragment C : [cC];
fragment D : [dD];
fragment E : [eE];
fragment F : [fF];
fragment G : [gG];
fragment H : [hH];
fragment I : [iI];
fragment J : [jJ];
fragment K : [kK];
fragment L : [lL];
fragment M : [mM];
fragment N : [nN];
fragment O : [oO];
fragment P : [pP];
fragment Q : [qQ];
fragment R : [rR];
fragment S : [sS];
fragment T : [tT];
fragment U : [uU];
fragment V : [vV];
fragment W : [wW];
fragment X : [xX];
fragment Y : [yY];
fragment Z : [zZ];
//
//PATH
// : ( ~'"' )+
// ;



/////AHMED_MARWAN remove BIND_PARAMETER from exp rule
//BIND_PARAMETER
// : '?' DIGIT*
// | [:@$] identifirer
// ;

/////AHMED_MARWAN remove IDENTIFIER & add identifier
//IDENTIFIER
// : '"' (~'"' | '""')* '"'
// | '`' (~'`' | '``')* '`'
//// | '[' ~']'* ']'
// | [a-zA-Z_] [a-zA-Z_0-9]*
// ;



//SCOL : ';';
//DOT : '.';
//OPEN_PAR : '(';
//CLOSE_PAR : ')';
//COMMA : ',';
//STAR : '*';
//PLUS : '+';
//MINUS : '-';
//TILDE : '~';
//MOD : '%';
//DIV : '/';
//LT2 : '<<';
//GT2 : '>>';



//factored_select_stmt
// : select_core
//   ( K_ORDER K_BY ordering_term ( ',' ordering_term )* )?
//   ( K_LIMIT expr ( ( K_OFFSET | ',' ) expr )? )?
// ;

//select_core
// : K_SELECT ( K_DISTINCT | K_ALL )? result_column ( ',' result_column )*
//  K_FROM ( table_or_subquery ( ',' table_or_subquery )* | join_clause )
//   ( K_WHERE (expr|sub_exp) )?
//   ( K_GROUP K_BY expr ( ',' expr )* ( K_HAVING expr )? )?
// | K_VALUES '(' expr ( ',' expr )* ')' ( ',' '(' expr ( ',' expr )* ')' )*
// ;